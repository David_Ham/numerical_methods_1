from matplotlib import rc
import pylab
from mpl_toolkits.axes_grid.axislines import Subplot
import numpy
import curve_fitting

def plot_linear():
    # x values to fit to .
    x = numpy.array ([1 ,2])
    # y values to fit to .
    y = numpy.array ([2 ,6])
    # solve for gradient and y intercept .
    c = curve_fitting.fit_linear (x , y )
    # create the line we have fit .
    X = numpy.linspace ( -1 ,3)
    Y = c [0]* X + c [1]
    # plot x and y axes :
    pylab.plot (X ,0* X , 'k ')
    pylab.plot (0* Y ,Y , 'k ')
    # plot the line.
    pylab.plot (X ,Y )
    # plot the original data .
    pylab.plot (x ,y , 'o ')
    pylab.axis ([ min ( X ) , max ( X) , min ( Y ) , max ( Y )])

def plot_linear_lst():
    x=numpy.array([ 3.57338565,  6.38581114,  4.87629479,  4.99438835,  9.00727138,
        2.80418475,  1.17134854,  2.99452961,  8.68396369,  2.84576936,
        2.27723779,  5.90504115,  0.07425835,  3.53685262,  8.81387161,
        9.07912777,  3.22052799,  2.80778042,  2.57690814,  1.41029419])
    y=numpy.array([  7.48416523,   9.27614392,   5.480598  ,   9.19124261,
        10.23815245,   2.18287206,  -0.27524785,   2.83567109,
        13.50456217,   4.81113061,   2.97506506,   9.63029136,
         2.70422832,   4.60216719,  13.30351221,  14.4472751 ,
         3.56250035,   6.55430054,   2.1476935 ,   2.98456271])

    cp=numpy.poly1d(curve_fitting.leastsq_polynomial(x,y,1))

    X = numpy.linspace ( 0 ,10)
    Y = cp(X)
    pylab.plot(X,Y)
    pylab.plot(x,y,'*')

def plot_quad_lst():
    x=numpy.array([ 7.12504696,  4.40289263,  5.35177407,  0.06732029,  7.9521156 ,
        0.45407183,  2.85899579,  8.28326999,  5.08574814,  3.89070369,
        2.09674416,  8.39522803,  9.72582814,  2.35577064,  1.86490175,
        6.2494143 ,  0.9579772 ,  8.64140737,  0.56293784,  8.89217612])

    y=numpy.array([  92.11236757,   39.75529423,   32.58132245,    9.14450561,
        123.76553868,   37.522072  ,  -13.28905534,  176.12091399,
         28.19069388,   63.16913309,  -19.82158357,  159.23668036,
        216.75200383,   46.19263315,   16.62383446,   48.25687978,
          9.54050564,  172.76534287,  -25.28074433,  186.84229368])

    cp=numpy.poly1d(curve_fitting.leastsq_polynomial(x,y,2))

    X = numpy.linspace ( 0 ,10)
    Y = cp(X)
    
    # Print the misfit.
    for i in range(len(x)):
        pylab.plot([x[i],x[i]],[y[i],cp(x[i])],'r')

    pylab.plot(X,Y,'b')
    pylab.plot(x,y,'*g')
    


def generate_plots():
    rc('font',**{'family':'serif',
                 'serif':['Palatino'],
                 'size':10})
    rc('text', usetex=True)

    pylab.figure(figsize=(6,4.5))
    plot_linear()
    pylab.savefig("linear_fit.pdf")

    pylab.figure(figsize=(6,4.5))
    plot_linear_lst()
    pylab.savefig("linear_lst.pdf")

    pylab.figure(figsize=(6,4.5))
    plot_quad_lst()
    pylab.savefig("quad_lst.pdf")


if __name__=="__main__":
    generate_plots()
