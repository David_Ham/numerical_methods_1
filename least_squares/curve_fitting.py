from numpy import *
from pylab import *
from random_data import random_data

def fit_linear(x,y):
    '''Construct and solve a linear system for the line passing through two
    points in the plane. x is The two x values, y is the two y values.

    The result is a vector c where c[0]x+c[1]=y is the line. 
    '''

    if(x.shape!=(2,)): raise ValueError("x must be a 2-vector")
    if(y.shape!=(2,)): raise ValueError("y must be a 2-vector")

    A=zeros((2,2))

    A[:,0]=x
    A[:,1]=1.

    c=linalg.solve(A,y)

    return c

def fit_polynomial(x,y, n):
    '''Construct and solve a linear system for the degree n polynomial
    passing through n+1 points in the plane.
    x is the n+1 x values, y is the n+1 y values.

    The result is a vector c where c[0]x^n+c[1]x^(n-1)+...+c[n]=y is the line. 
    '''

    if(x.shape!=(n+1,)): raise ValueError("x must be a n+1-vector")
    if(y.shape!=(n+1,)): raise ValueError("y must be a n+1-vector")

    A=zeros((n+1,n+1))

    for i in range(n+1):
        A[:,i]=x**(n-i)
        
    c=linalg.solve(A,y)

    return c

def leastsq_polynomial(x,y, n):
    '''Construct and solve the least squares system for the degree n polynomial
    passing through a set of points in the plane.
    x is set of x values, y set of corresponding y values.

    The result is a vector c where c[0]x^n+c[1]x^(n-1)+...+c[n]=y is the line. 
    '''

    if(x.shape!=y.shape): raise ValueError("y and x must be the same size.")
    if(len(x)<n+1): raise ValueError("not enough points")

    nx=len(x)
    
    A=zeros((nx,n+1))

    for i in range(n+1):
        A[:,i]=x**(n-i)
    
    c=linalg.solve(dot(A.T,A),dot(A.T,y))

    return c

def test_lstsq(n,eps,misfit=False):

    figure()

    # 20 random  points.
    x,y=random_data(20,n,eps)

    cp=poly1d(leastsq_polynomial(x,y,n))

    # Points to plot curve at.
    px=linspace(0,10)
    py=cp(px)

    plot(x,y,'*')
    plot(px,py)

    if misfit:
        for i in range(len(x)):
            plot([x[i],x[i]],[y[i],cp(x[i])],'r')

def test_linear():
    # x values to fit to.
    x=array([1,2])
    # y values to fit to.
    y=array([2,6])
    
    # solve for gradient and y intercept.
    c=fit_linear(x,y)
    
    # plot the original data.
    plot(x,y,'o')
    
    # plot the line we have fit.
    X=linspace(-1,3)
    Y=c[0]*X+c[1]
    
    # plot x and y axes:
    plot(X,0*X,'k')
    plot(0*Y,Y,'k')
    
    plot(X,Y)
    axis([min(X),max(X),min(Y),max(Y)])

    show()
