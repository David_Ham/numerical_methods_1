from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FixedLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

rc('font',**{'family':'serif',
             'serif':['Palatino'],
             'size':10})
rc('text', usetex=True)


fig = plt.figure(figsize=(6,4))
ax = Axes3D(fig)
X = np.arange(-10, 10, 0.25)
xlen = len(X)
Y = np.arange(-10, 10, 0.25)
ylen = len(Y)
X, Y = np.meshgrid(X, Y)

Z = 0.5*(X+Y)

surf = ax.plot_surface(X, Y, Z, rstride=1, 
        linewidth=0, antialiased=False,color='LightBlue',shade=False)
ax.view_init(elev=24,azim=-64)

ax.plot3D([0,2.],[0,0],[0,1],'m', linewidth=2)
ax.plot3D([0,0],[0,4],[0,2],'m', linewidth=2)

A=np.array([[1.,0.,0.5],[0.,2.,1.]]).T
b=np.array([2.,2.,8.])

x=np.linalg.lstsq(A,b)[0]

Ax=np.dot(A,x)

#print np.dot(A.T,b-Ax)
assert(abs(np.max(np.dot(A.T,b-Ax)))<1.e-14)

ax.plot3D([b[0],Ax[0]],[b[1],Ax[1]],[b[2],Ax[2]],'r',linewidth=2)
ax.plot3D([0.,Ax[0]],[0.,Ax[1]],[0.,Ax[2]],'b',linewidth=2)

# Plot original vector last so it goes on top.
ax.plot3D([0,2],[0,2],[0,8],'k', linewidth=2)

plt.savefig("least_squares_2d.pdf")
