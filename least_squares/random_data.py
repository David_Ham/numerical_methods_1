import numpy
from numpy import random

def random_data(n,p,eps):
    '''random_data(n,p,eps)

    Produce a random data set composed of n points randomly spread about
    a polynomial of degree p. The data will differ from the "true" curve by
    at most epsilon times the maximum valule.'''

    x=random.random(n)*10

    # Random polynomial coefficients.
    poly=numpy.poly1d(random.random(p+1)*3)

    y=poly(x)

    e=eps*max(abs(y))

    y+=random.random(n)*2*e-e
    
    return x,y
    
