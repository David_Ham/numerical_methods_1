from numpy import *
from numpy import random

def fit_polynomial(x,y, n):
    '''Construct and solve a linear system for the degree n polynomial
    passing through n+1 points in the plane.
    x is the n+1 x values, y is the n+1 y values.

    The result is a vector c where c[0]x^n+c[1]x^(n-1)+...+c[n]=y is the line. 
    '''

    if(x.shape!=(n+1,)): raise ValueError("x must be a n+1-vector")
    if(y.shape!=(n+1,)): raise ValueError("y must be a n+1-vector")

    A=zeros((n+1,n+1))

    for i in range(n+1):
        A[:,i]=x**(n-i)
        
    return linalg.solve(A,y)


def random_data(n,p,eps):
    '''random_data(n,p,eps)

    Produce a random data set composed of n points randomly spread about
    a polynomial of degree p. The data will differ from the "true" curve by
    at most epsilon times the maximum value.'''

    x=random.random(n)*10

    # Random polynomial coefficients.
    poly=poly1d(random.random(p+1)*3)

    y=poly(x)

    e=eps*max(abs(y))

    y+=random.random(n)*2*e-e
    
    return x,y
