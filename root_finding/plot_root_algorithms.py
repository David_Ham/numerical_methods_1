from matplotlib import rc
import pylab
from mpl_toolkits.axes_grid.axislines import Subplot
import numpy

def plot_bolzano():

    f=lambda x: numpy.sin(x)

    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    l=-numpy.pi/4
    r=numpy.pi/2
    dx=l-r

    # X axis
    dx=r-l
    pylab.plot([l-.1*dx,r+.1*dx],[0.,0.],'k')
    
    # Function
    x=numpy.linspace(l,r,201)
    pylab.plot(x,f(x),'b')

    # Vertical lines.
    pylab.plot([l,l],[0,f(l)],'g')
    right=pylab.plot([r,r],[0,f(r)],'g')

    pylab.text(l,0.05,"$x_0$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(r,-.05,"$x_1$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(0,-0.05,"$x'$",horizontalalignment='center',
               verticalalignment='center')


    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    pylab.margins(0.01)

def plot_bisection():

    f=lambda x: numpy.sin(x)

    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    l=-numpy.pi/4
    r=numpy.pi/2
    dx=l-r

    # X axis
    dx=r-l
    pylab.plot([l-.1*dx,r+.1*dx],[0.,0.],'k')
    
    # Function
    x=numpy.linspace(l,r,201)
    pylab.plot(x,f(x),'b')

    # Vertical lines.
    pylab.plot([l,l],[0,f(l)],'g')
    right=pylab.plot([r,r],[0,f(r)],'g')

    pylab.text(l,0.05,"$x_0$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(r,-.05,"$x_1$",horizontalalignment='center',
               verticalalignment='center')

    for i in range(4):
        x_c=.5*(l+r)
        if f(x_c)*f(l)>0:
            l=x_c
        else:
            r=x_c
        
        pylab.plot([x_c,x_c],[0,f(x_c)],'g--')
        pylab.text(x_c,
                   numpy.sign(f(x_c))*-0.05,
                   "$x_"+`i+2`+"$",
                   horizontalalignment='center',
                   verticalalignment='center')
    

    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    pylab.margins(0.01)

def plot_newton():

    f=lambda x: numpy.sin(x)
    dfdx=lambda x: numpy.cos(x)

    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    l=-numpy.pi/4
    r=numpy.pi/4
    dx=l-r

    # X axis
    dx=r-l
    pylab.plot([l-.1*dx,r+.1*dx],[0.,0.],'k')
    
    # Function
    x=numpy.linspace(l,r,201)
    pylab.plot(x,f(x),'b')

    # Vertical lines.

    x_c=l
    for i in range(3):
        x_n=x_c-f(x_c)/dfdx(x_c)
        
        pylab.plot([x_c,x_c],[0,f(x_c)],'g')
        pylab.plot([x_n,x_c],[0,f(x_c)],'g--')
        pylab.text(x_c,
                   numpy.sign(f(x_c))*-0.06,
                   "$x_"+`i`+"$",
                   horizontalalignment='center',
                   verticalalignment='center')
        x_c=x_n

    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    pylab.margins(0.01)

def generate_plots():
    rc('font',**{'family':'serif',
                 'serif':['Palatino'],
                 'size':10})
    rc('text', usetex=True)

    f=lambda x: x**4+1
    df=lambda x: 4*x**3

    pylab.figure(figsize=(6,4))
    plot_bolzano()
    pylab.savefig("bolzano.pdf")

    pylab.figure(figsize=(6,4))
    plot_bisection()
    pylab.savefig("bisection.pdf")

    pylab.figure(figsize=(6,4))
    plot_newton()
    pylab.savefig("newton.pdf")

if __name__=="__main__":
    generate_plots()
