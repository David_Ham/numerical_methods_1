import numpy

def q_convergence(s):
    
    s_l=numpy.array(s)
    
    return numpy.log(abs((s[2:-1]-s[-1])/(s[1:-2]-s[-1])))\
        /numpy.log(abs((s[1:-2]-s[-1])/(s[:-3]-s[-1])))
