import sys
import root_finding
from numpy import *

f=lambda x:x**3-1
df=lambda x:3*x**2

def generic_scheme(scheme, x_0=None, x_1=None, l=None, r=None, epsilon=None,
                  N=100, record=False):
    if scheme=="bisection":
        if record:
            return root_finding.bisection(f, l, r, epsilon, record)
        else:
            # Allow for checking of default argument value.
            return root_finding.bisection(f, l, r, epsilon)

    elif scheme=="Newton":
        if record:
            return root_finding.newton(f, df, x_0, epsilon, N, record)
        else:
            # Allow for checking of default argument value.
            return root_finding.newton(f, df, x_0, epsilon, N)

    elif scheme=="secant":
        if record:
            return root_finding.secant(f, x_0, x_1, epsilon, N, record)
        else:
            # Allow for checking of default argument value.
            return root_finding.secant(f, x_0, x_1, epsilon, N)

def die(message):
    print "FAIL: "+message
    print "Tests failed"
    sys.exit(1)

all_schemes=["bisection","Newton","secant"]
expected_rate={
    "bisection": 1,
    "Newton": 2,
    "secant": (1+sqrt(5))/2}
try:
    schemes=[sys.argv[1]]
    
    if schemes[0] not in all_schemes:
        print "Unknown root_finding method "+schemes[0]
        sys.exit(1)

except IndexError:
    print "No method specified on the command line. Testing all 3\n"
    schemes=all_schemes

for scheme in schemes:

    print "Testing root finding using "+scheme
    try:
        s=generic_scheme(scheme,0.1,0.2,0.5,2,-1,record=True)
        die(scheme +" fails to trap negative epsilon values")
    
    except ValueError:
        print scheme +" successfully traps negative epsilon values"    

    if scheme=="bisection":
        try:
            s=generic_scheme(scheme,0.1,0.2,0.5,0.6,1.e-12,record=False)
            die("bisection fails to correctly trap intervals not containing a root")
        except ValueError:
            print scheme+" interval not containing a root correctly trapped"
    else:
        try:
            s=generic_scheme(scheme,0.1,0.2,0.5,2,1.e-12,N=1,record=False)
            die(scheme+ " fails to handle maximum iteration count correctly")
        except ArithmeticError:
            print scheme+" handles maximum iteration count correctly" 

    try:
        s=generic_scheme(scheme,0.1,0.2,0.5,2,1.e-12,record=False)
        assert(isscalar(s) and isreal(s))
        print scheme +\
            " result with default record value (False) has correct type."
    except TypeError:
        die(scheme +" does not seem to have a default record value of true")
    except AssertionError:
        die(scheme +" result with default record value (False) is not a scalar real value.")

    try:
        s=array(generic_scheme(scheme,0.1,0.2,0.5,2.,1.e-12,record=True))
        s0=s[0][0]
        assert(isscalar(s0) and isreal(s0))
        s1=s[0][1]
        assert(isscalar(s1) and isreal(s1))
        
        print scheme +" result with record=True has correct type."
    except AssertionError:
        die(scheme +" result with record=True is not a sequence of pairs of floats.")

    # Just need the first column of s.
    s=s[:,0]
    rate=log(abs((s[-2]-1)/(s[-3]-1)))\
        /log(abs((s[-3]-1)/(s[-4]-1)))
    
    if (rate-expected_rate[scheme])/rate < 0.02:
        print scheme +" converges at order "+\
            `rate`+", close to the expected rate of "\
            +`expected_rate[scheme]`+"\n"
    else:
        die(scheme +" fails to converge at the expected rate")
    

print "All tests passed"
