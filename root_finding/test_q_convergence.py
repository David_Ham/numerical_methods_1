from numpy import array

S1=array([0.24978724162242166,
 0.12485840701911723,
 0.06243440755401777,
 0.031217683784672382,
 0.01562177182080231,
 0.007810089461995666,
 0.003907838636813136,
 0.001953676054603064,
 0.0009765268679874607,
 0.0004881583916238785,
 0.0])

S2=array([0.24978724162242166,
 0.062336986323277305,
 0.003884608577646516,
 1.5091683601477576e-05,
 2.27919109843289e-10,
 5.198441240592778e-20,
 2.700918405664104e-39,
 7.297813478515325e-78,
 5.3256446677126324e-155,
 2.833596596781695e-309])


def test_convergence(convergence):
    """Check that the convergence function supplied produces the right
    result"""

    n=array(convergence(S1))
    print "Testing first order convergence, results should be close to 1.0:"
    print n

    n=array(convergence(S2))
    print "Testing first order convergence, results should be close to 2.0:"
    print n
    

    # if numpy.max(abs(c1-n))<1.e-5:
    #     print "convergence function returns correct result for first order convergence test."
    # else:
    #     print "convergence function returns incorrect result for first order convergence test:"
    #     print "function result: ",str(n)
    #     print "correct answer:  ",str(c1)
    #     print "difference:      ",str(abs(n-c1))
    #     print ""

    # n=array(convergence(h,S2))

    # if numpy.max(abs(c2-n))<1.e-5:
    #     print "convergence function returns correct result for second order convergence test."
    # else:
    #     print "convergence function returns incorrect result for second order convergence test:"
    #     print "function result: ",str(n)
    #     print "correct answer:  ",str(c2)
    #     print "difference:      ",str(abs(n-c2))
    #     print ""

if __name__=="__main__":
    import convergence
    test_convergence(convergence.q_convergence)
