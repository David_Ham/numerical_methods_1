
def bisection(f, x_0, x_1, epsilon, record=False):

    f_l=f(x_0)
    l=x_0
    r=x_1

    if f_l*f(x_1)>0:
        raise ValueError("f(x_0) and f(x_1) have the same sign!")

    if epsilon<0:
        raise ValueError("epsilon must be positive!")

    x_c=0.5*(l+r)
    f_c=f(x_c)
    if record:
        seq = [(x_c,f_c)]

    while abs(f_c)>epsilon:
        
        if f_c*f_l>0:
            l=x_c
            f_l=f_c
        else:
            r=x_c

        x_c=0.5*(l+r)
        f_c=f(x_c)
        if record:
            seq.append((x_c,f_c))
        
    if record:
        return seq
    else:
        return x_c

def newton(f, dfdx, x_0, epsilon, N=100, record=False):

    f_x=f(x_0)
    x=x_0

    if epsilon<0:
        raise ValueError("epsilon must be positive!")

    if record:
        seq = [(x,f_x)]
        
    i=0
    while abs(f_x)>epsilon:
        
        i+=1
        if (i>N):
            raise ArithmeticError("Maximum iteration count exceeded")        

        x=x-float(f_x)/dfdx(x)
        f_x=f(x)
        if record:
            seq.append((x,f_x))
        
    if record:
        return seq
    else:
        return x

def secant(f, x_0, x_1, epsilon, N=100, record=False):
    
    f_old=f(x_0)
    f_x=f(x_1)
    x_old=x_0
    x=x_1

    if epsilon<0:
        raise ValueError("epsilon must be positive!")

    if record:
        seq = [(x,f_x)]
        
    i=0
    while abs(f_x)>epsilon:
        
        i+=1
        if (i>N):
            raise ArithmeticError("Maximum iteration count exceeded")        

        dfdx=(f_x-f_old)/float(x-x_old)

        x_old=x
        f_old=f_x
        x=x-float(f_x)/dfdx
        f_x=f(x)
        if record:
            seq.append((x,f_x))
        
    if record:
        return seq
    else:
        return x









        
