from root_finding import newton
import pylab 
import numpy
from numpy import *
from time import sleep

def cr_wait():
    """Wait for the user to hit return"""
    import sys

    sys.stdout.write("Hit return")
    sys.stdout.flush()
    null=sys.stdin.readline()
    
def zoomaxis(newaxis):
    """Zoom in 10 steps from the current axis to the newaxis given."""

    oldaxis=array(pylab.axis())
    
    daxis=array(newaxis)-oldaxis

    for dx in linspace(0.,1.,10):
        pylab.axis(oldaxis+dx*daxis)
        pylab.draw()

def zoomline(x,y,line=None):
    """zoomline(line, p0,p1)
    Produce a line extending gradually from p0 to p1. If present, line
    should be an existing pylab line which will be redrawn."""
    
    if not line:
        lline=pylab.plot([x[0],x[0]],[y[0],y[0]],'g')[0]
    else:
        lline=line

    dx=x[1]-x[0]
    dy=y[1]-y[0]
    for s in linspace(0.,1.,10):
        lline.set_data([x[0],x[0]+s*dx],[y[0],y[0]+s*dy])
        lline.set(visible=True)
        pylab.draw()
        sleep(0.05)

    return lline

def vanishline(line):

    for s in linspace(0.,1.,10):
        line.set(alpha=1-s)
        pylab.draw()
        sleep(0.05)
    line.set(alpha=1,visible=False)

    return line

def visualise_newton(f, dfdx, x0, epsilon=1.0E-7, N=10):
    """visualise_newton(f, dfdx, x0,epsilon=1.0E-7, N=10)
    progressively visualise Newton's method one step at a time.

    x0 is the starting point, epsilon is the convergence tolerance
    and N is the maximum number of steps to take."""

    # Use Newton's method to generate a list of root iterates and function
    # values. We discard the final solution.
    its=newton(f, dfdx, x0, epsilon, N, record=True)

    # Old values are the initial guess.
    xold,yold=its[0]
    # New values are the result of the first Newton solve.
    x,y=its[1]

    # New figure
    pylab.clf()
    a=pylab.gca()
    a.set_autoscale_on(False)

    # Calculate a sensible size for the axis window.
    xmax=max(x,xold)
    xmin=min(x,xold)
    dx=abs(x-xold)
    ymax=max(y,yold)
    ymin=min(y,yold)
    dy=abs(y-yold)

    # X axis.
    xaxis=pylab.plot([xmin-dx,xmax+dx],[0.,0.],'k')[0]
    # Function values.
    xfunc=linspace(xmin-dx, xmax+dx, 100)
    yfunc=array([f(xi) for xi in xfunc])
    func=pylab.plot(xfunc,yfunc,'b')[0]

    # Plot a green circle on the initial guess.
    xf=pylab.plot([x0],[0.],'go')[0]

    newaxis=[xmin-dx,xmax+dx,ymin-dy,ymax+dy]
    pylab.axis(newaxis)

    pylab.draw()

    # Wait for the user to hit return.
    cr_wait()

    # Draw the line from the initial guess to the function value at that
    # point.
    vertical=zoomline([xold, xold],[0.,yold])
    sleep(0.5)

    # Draw the tangent line from the function value to the new iterate.
    tangent=zoomline([xold, x],[yold,0])
    pylab.draw()
    sleep(0.5)

    # Move the green dot to the new iterate.
    xf.set_data([x],[0.])
    pylab.draw()
    
    # Wait for the user to hit return.
    cr_wait()
    
    # Copy the new values to the old ones for the next iteration.
    xold=x
    yold=y
    
    for x,y in its[2:]:
        # Make lines vanish before axis zooming.
        vertical.set(visible=False)        
        tangent.set(visible=False)
        pylab.draw()
        
        # Calculate a sensible size for the axis window.
        xmax=max(x,xold)
        xmin=min(x,xold)
        dx=abs(x-xold)
        ymax=max(y,yold)
        ymin=min(y,yold)
        dy=abs(y-yold)
        newaxis=[xmin-dx,xmax+dx,ymin-dy,ymax+dy]

        # Zoom to the new axis.
        zoomaxis(newaxis)

        # Reset the plot to fit the new window.
        xfunc=linspace(xmin-dx, xmax+dx, 100)
        yfunc=array([f(xi) for xi in xfunc])

        xaxis.set_data([xmin-dx,xmax+dx],[0.,0.])
        func.set_data(xfunc,yfunc)
        pylab.draw()
        sleep(0.5)

        # Draw the line from the current iterate to the function value.
        zoomline([xold, xold],[0.,yold],vertical)
        pylab.draw()
        sleep(0.5)
        
        # Draw the tangent line from the function value to the new iterate.
        zoomline([xold, x],[yold,0],tangent)
        pylab.draw()
        sleep(0.5)

        # Move the green dot to the new iterate.
        xf.set_data([x],[0.])
        pylab.draw()

        # Wait for the user to hit return.
        cr_wait()

        # Copy the new values to the old ones for the next iteration.
        xold=x
        yold=y


def visualise_bisection(f, x0, x1, epsilon=1.0E-7):
    """visualise_bisection(f, x0, x1, epsilon=1.0E-7)
    progressively visualise the bisection method one step at a time.

    [x0,x1] is the starting interval, epsilon is the convergence tolerance."""

    # Old values are the initial guess.
    xold,yold=x0, f(x0)
    # New values are the result of the first Newton solve.
    x,y=x1, f(x1)

    # New figure
    pylab.clf()
    a=pylab.gca()
    a.set_autoscale_on(False)

    # Calculate a sensible size for the axis window.
    xmax=max(x,xold)
    xmin=min(x,xold)
    dx=abs(x-xold)
    ymax=max(y,yold)
    ymin=min(y,yold)
    dy=abs(y-yold)

    # X axis.
    xaxis=pylab.plot([xmin-dx,xmax+dx],[0.,0.],'k')[0]
    # Function values.
    xfunc=linspace(xmin-dx, xmax+dx, 100)
    yfunc=array([f(xi) for xi in xfunc])
    func=pylab.plot(xfunc,yfunc,'b')[0]

    newaxis=[xmin-dx,xmax+dx,ymin-dy,ymax+dy]
    pylab.axis(newaxis)

    pylab.draw()

    # Initial interval
    left=zoomline([xold, xold],[0.,yold])
    right=zoomline([x, x],[0.,y])

    # Copy the new values to the old ones for the next iteration.
    l=xold
    r=x
    fl=f(l)
    fr=f(r)
    c=.5*(l+r)
    fc=f(c)
    centre=zoomline([c,c],[0,0])

    while abs(fc)>epsilon:
        centre=zoomline([c,c],[0,fc],centre)
        if fl*fc>0:
            tmp=vanishline(left)
            left=centre
            centre=tmp
            fl=fc
            l=c
        else:
            tmp=vanishline(right)
            right=centre
            centre=tmp
            fr=fc
            r=c
        c=.5*(l+r)
        fc=f(c)

        # Calculate a sensible size for the axis window.
        xmax=max(l,r)
        xmin=min(l,r)
        dx=abs(l-r)
        ymax=max(fl,fr)
        ymin=min(fl,fr)
        dy=abs(fl-fr)
        newaxis=[xmin-dx,xmax+dx,ymin-dy,ymax+dy]

        # Zoom to the new axis.
        zoomaxis(newaxis)

        # Reset the plot to fit the new window.
        xfunc=linspace(xmin-dx, xmax+dx, 100)
        yfunc=array([f(xi) for xi in xfunc])

        xaxis.set_data([xmin-dx,xmax+dx],[0.,0.])
        func.set_data(xfunc,yfunc)
        pylab.draw()
        sleep(0.5)
        
    # put in the final line.
    centre=zoomline([c,c],[0,0])

if __name__=="__main__":
    f=lambda x: numpy.sin(x)-sqrt(2)/2
    dfdx=lambda x:numpy.cos(x)
    x0=0.0

    pylab.ion()
    visualise_newton(f, dfdx, x0, epsilon=1.0E-7, N=10)
    
    
