\documentclass[a4paper,12pt]{article}

\usepackage{listings}

\usepackage{ese_exam}
%\usepackage[answers]{ese_exam}

%\usepackage{amsmath,amssymb,mathabx}
\usepackage{amsmath,amssymb,soul}
\usepackage[noend]{algorithmic}
\usepackage{graphicx}
\usepackage{color}
\renewcommand{\to}{\textbf{ to }}

\usepackage[T1]{fontenc}
\usepackage[sc]{mathpazo}


%\changenotsign
\renewcommand{\O}{\mathcal{O}}

\newcommand{\real}[1]{\mathrm{Re}(#1)}
\newcommand{\imag}[1]{\mathrm{Im}(#1)}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\dx}{\,\d x}
\newcommand{\dy}{\,\d y}
\newcommand{\dt}{\,\d t}
\newcommand{\ddx}[2][x]{\frac{\d#2}{\d#1}}
\newcommand{\ddxx}[2][x]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ddt}[2][t]{\frac{\d#2}{\d#1}}
\newcommand{\ddtt}[2][t]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ppx}[2][x]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppy}[2][y]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppz}[2][z]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppt}[2][\theta]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppr}[2][r]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppxx}[2][x]{\frac{\partial^2#2}{\partial#1^2}}
\newcommand{\mat}[1]{\mathrm{#1}}
\DeclareMathOperator{\len}{len}

\newcommand{\A}{\mat A}
\newcommand{\B}{\mat B}
\newcommand{\AT}{\mat A^{\mat T}}
\renewcommand{\L}{\mat L}
\newcommand{\U}{\mat U}
\renewcommand{\P}{\mat P}

\newcommand{\Integer}{\mathbb{Z}}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\Rational}{\mathbb{Q}}
\newcommand{\Complex}{\mathbb{C}}
\newcommand{\Natural}{\mathbb{N}}

\renewcommand{\O}{\mathcal{O}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\x}{\vec{x}}

\newcommand{\eps}{\epsilon_{\mathrm{machine}}}

\DeclareMathOperator{\shape}{shape}


\newcommand{\amarks}[1]{\mbox{}\hfill\emph{(#1 marks)}}

\begin{document}
\begin{integratedsection}{C}{6}{Numerical Methods 1}{3.09}{David Ham}
  \begin{enumerate}
  \item
    \begin{enumerate}
    \item Consider the number $5.25$
      \begin{enumerate}
      \item 
        Write the number in the form:
        \begin{equation}
          s1.m\times 2^{e-b}
        \end{equation}
        Use a floating point format with 3 exponent bits and 4 mantissa bits, and a bias
        of 3. All the numbers must be written in binary.
        \answer{
          5.25 is $101.01_2$ or $1.0101_2\times 2^2$. We need $e-3=1$ so $e=5=101_2$. The
          sign is positive so in the prescribed form it's
          \begin{equation*}
            01.0101_2\times 2^{101_2-11_2}
          \end{equation*}
          
          \emph{1 mark for sign, 2 each for exponent and mantissa and 1 for
            correctly applying the bias.}
        }
        \marks{6}
      \item Convert this number into the bit pattern which would actually be
        stored, according to the layout provided on the formula sheet.
        \answer{
          01010101
          
          If the candidate makes a mistake on the first part but the bit pattern
          is consistent with their answer, they get the marks.
        }
        \marks{2}
      \end{enumerate}
    \item
      \begin{enumerate}
      \item
        The Bisection method is given by the algorithm:
        \begin{algorithmic}
          \STATE $f_l\gets f(x_l)$
          \REPEAT
          \STATE $x_c\gets \frac{x_l+x_r}{2}$
          \STATE $f_c\gets f(x_c)$
          \IF{$f_cf_l>0$}
          \STATE $x_l\gets x_c$
          \STATE $f_l\gets f_c$
          \ELSE
          \STATE $x_r\gets x_c$
          \ENDIF
          \UNTIL{$|f(x_c)|<\epsilon$}
        \end{algorithmic}
        Produce a sketch of one iteration of this algorithm for the
        function $f(x)=x^2-1$ with a starting interval $x_l=0.5, x_r=2$. Show all
        the relevant points and lines and indicate which end of the interval
        will be removed.
        \marks{3}
        \answer{
          
          \input{bisection_pdf.tex}
        }

      \item
        The Newton-Raphson iteration is given by the algorithm:
        \begin{algorithmic}
          \REPEAT
          \STATE $f_x\gets f(x)$
          \STATE $x\gets x - \frac{f_x}{f'(x)}$
          \UNTIL{$(|f_x|<\epsilon)$ or maximum iterations exceeded. }  
        \end{algorithmic}

       Produce a sketch of one iteration of this algorithm for the
        function $f(x)=x^2-1$ with a starting position $x_0=0.5$. Show all
        the relevant points and lines.
        \marks{3}
        \answer{

          \input{newton_pdf.tex}
        }
      \item What is the key advantage of Newton-Raphson iteration over the bisection method?
        \marks{1}
        \answer{
          Much more rapid convergence (Q-order 2 in most cases rather than 1
          for the bisection method).
        }
      \item What additional information is required to use Newton-Raphson iteration
        rather than the bisection method?
        \marks{1}
        \answer{
          The function derivative.
        }
      \item What advantage does the bisection method have over
        Newton-Raphson iteration?
        \marks{1}
        \answer{
          Guaranteed convergence as long as the root lies in the initial interval.
        }
      \end{enumerate}

    \item Suppose that $\A$ is a rectangular $n\times m$ matrix with $n>m$, and
      that the columns of $\A$ are all orthogonal to each other, and each column
      has a modulus of 1 ($|\A_{:,i}|=\sqrt{A_{:,i}\cdot \A_{:,i}}=1$ for $0\leq
      i \leq m-1$).
      \begin{enumerate}
      \item prove that $\A^{\mathrm T}\A=\mat I$ where $\mat I$ is the $m\times m$
        identity matrix.
        \marks{4}
        \answer{Let $\B=\A^{\mathrm T}\A$. Then:
          \begin{equation*}
            \B[i,j]=\A[:,i]\cdot\A[:,j]
          \end{equation*}
          Where $i\neq j$, $\B[i,j]=0$ since different columns of $\A$ are
          orthogonal to each other. However, where $i=j$, $\B[i,j]=1$ since
          each column of $\A$ has modulus 1. We can conclude that $\B$ has 1
          on the diagonal and $0$ elsewhere and is equal to the identity matrix.
        }
      \item prove therefore that the least squares solution to the
        overdetermined matrix equation:
        \begin{equation*}
          \A\x=\vec b
        \end{equation*}
        is given by $\x=\A^{\mathrm T} \vec b$
        \marks{4}
        \answer{The least squares problem may be solved by multiplying by the
          transpose of $\A$:
          \begin{equation*}
            \A^{\mathrm T}\A\x=\A^{\mathrm T}\vec b
          \end{equation*}
          Now using the previous result, we have:
          \begin{gather*}
            \mat I \x=\A^{\mathrm T}\vec b\\
            \x=\A^{\mathrm T}\vec b
          \end{gather*}
        }      
      \end{enumerate}
    \end{enumerate}
    \pagebreak
    \enlargethispage{\baselineskip}
  \item 
    \begin{enumerate}
    \item Convert the numbers in the following problems into 4 bit two's
      compliment signed binary integers before performing the calculation and
      converting back into base 10. 
      \begin{enumerate}
      \item $-5+3$
      \marks{4}
        \answer{
          $5$ is $0101_2$ so taking its binary compliment and adding $1$ gives:
          $1011_2$ for $-5$.
          \begin{align*}
            1011_2&\\
            +0011_2&\\
            =1110_2
          \end{align*}
          Taking the binary compliment of the answer and adding $1$ gives
          $0010_2=2$ so the answer is equal to $-2$.
          \emph{2 marks for conversions, 2 for the sum}
        }

      \item $-1 \times 4$
        \marks{4}
        \answer{
          $1$ is $0001_2$ so taking its binary compliment and adding $1$
          gives:
          $1111_2$ for $-1$.
          \begin{align*}
            1111_2&\\
            \times0100_2&\\
            =\textrm{\st{11}}1100_2&\\
            =1100_2
          \end{align*}
          Taking the binary compliment of the answer and adding $1$ gives
          $0100_2=4$ so the answer is equal to $-4$ 
          \emph{2 marks for conversions, 2 for the product}
        }
      \end{enumerate}

    \item For each of the following Python functions, write a mathematical
      expression using only matrices and vectors which performs the same
      operation. In each case, state whether each input and output is a matrix
      or vector.
      \begin{enumerate}
      \item     
        \begin{lstlisting}
def function_1(a,b,c):
    import numpy

    d=numpy.zeros(numpy.size(b))

    for j in range(len(d)):
        d[j]=numpy.dot(a[j,:],c)-b[j]

    return d
        \end{lstlisting}
        \marks{4}
        \answer{By counting indices, we can see that ~a~ is a matrix, and
          ~b~ and ~d~ are vectors. For the dot product to be defined, ~c~
          must also be a vector. 

          The function calculates:
          \begin{equation*}
            \vec d=\A\vec c - \vec b
          \end{equation*}
          \emph{2 marks for types, 2 for the operation.}
        }

      \item 
        \begin{lstlisting}
def function_2(a):
    import numpy
    
    c=a.shape[1]
    b=numpy.zeros((c,c))

    for i in range(c):
        for j in range(c):
            b[i,j]=numpy.dot(a[:,i],a[:,j])

    return b         
        \end{lstlisting}
        \marks{4}
        \answer{
          By counting indices, we can see that ~a~ and ~b~ must both be
          matrices. 

          The function calculates:
          \begin{equation*}
            \B=\AT\A
          \end{equation*}
          \emph{2 marks for types, 2 for the operation.}
        }
      \end{enumerate}
      
    \item A central difference approximation to the third derivative of a
      function $f(x)$ is given by:
      \begin{equation*}
        f'''(x)=\frac{-f(x-2h)+2f(x-h)-2f(x+h)+2f(x+2h)}{2h^3}+\O(h^p)
      \end{equation*}
      Where $h$ is some small positive step size, and $p$ is the order of
      convergence. Using Taylor series for the function $f$, or otherwise, prove
      that the order of convergence, $p$, is equal to $2$.
      
      You may use without proof any of the properties of $\O$.
      \marks{9}
      \answer{
        First write Taylor series centred at $x$ for each of the function evaluations:
        \begin{gather*}
          f(x-2h)=f(x)-2hf'(x)+\frac{4h^2}{2}f''(x)-\frac{8h^3}{6}f^{(3)}(x)+\frac{16h^4}{24}f^{(4)}(x)+\O(h^5)\\
          f(x+2h)=f(x)+2hf'(x)+\frac{4h^2}{2}f''(x)+\frac{8h^3}{6}f^{(3)}(x)+\frac{16h^4}{24}f^{(4)}(x)+\O(h^5)\\
          f(x-h)=f(x)-hf'(x)+\frac{h^2}{2}f''(x)-\frac{h^3}{6}f^{(3)}(x)+\frac{h^4}{24}f^{(4)}(x)+\O(h^5)\\  
          f(x+h)=f(x)+hf'(x)+\frac{h^2}{2}f''(x)+\frac{h^3}{6}f^{(3)}(x)+\frac{h^4}{24}f^{(4)}(x)+\O(h^5)\\
        \end{gather*}
        \emph{3 marks for Taylor series.}

        Now, write $-f(x-2h)+2f(x-h)-2f(x+h)+f(x+2h)$ by taking the weighted sum of
        the power series:
        \begin{multline*}
          -f(x-2h)+2f(x-h)-2f(x+h)+f(x+2h)=\\
          -f(x)+2hf'(x)-\frac{4h^2}{2}f''(x)+\frac{8h^3}{6}f^{(3)}(x)-\frac{16h^4}{24}f^{(4)}(x)\\
          +2f(x)-2hf'(x)+\frac{2h^2}{2}f''(x)-\frac{2h^3}{6}f^{(3)}(x)+\frac{2h^4}{24}f^{(4)}(x)\\
          -2f(x)-2hf'(x)-\frac{2h^2}{2}f''(x)-\frac{2h^3}{6}f^{(3)}(x)-\frac{2h^4}{24}f^{(4)}(x)\\
          +f(x)+2hf'(x)+\frac{4h^2}{2}f''(x)+\frac{8h^3}{6}f^{(3)}(x)+\frac{16h^4}{24}f^{(4)}(x)\\
          +\O(h^5)
        \end{multline*}
        Next gather like terms:
        \begin{multline*}
          -f(x-2h)+2f(x-h)-2f(x+h)+f(x+2h)=\\
          (-1+2-2+1)f(x)\\
          +(2-2-2+2)hf'(x)\\
          +(-2+1-1+2)h^2f''(x)\\
          +(4-1-1+4)\frac{h^3}{3}f^{(3)}(x)\\
          +(-2+1-1+2)\frac{h^4}{3}f^{(4)}(x)+\O(h^5)
        \end{multline*}
        Cancelling all the zero sums gives:
        \begin{multline*}
          -f(x-2h)+2f(x-h)-2f(x+h)+f(x+2h)=6\frac{h^3}{3}f^{(3)}(x)+\O(h^5)
        \end{multline*}
        \emph{4 marks for the manipulations.}

        Finally dividing through by $2h^3$ as in the original formula gives:
        \begin{equation*}
          \frac{-f(x-2h)+2f(x-h)-2f(x+h)+2f(x+2h)}{2h^3}=f^{(3)}(x)+\O(h^2)  
        \end{equation*}
        So $p=2$.

        \emph{2 marks for the final division.}
      }
    \end{enumerate}
  \end{enumerate}
\end{integratedsection}
\end{document}
