\documentclass[a4paper,12pt]{article}

\usepackage[margin=2.5cm]{geometry}
%\usepackage{mathpazo}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[usenames,dvipsnames]{color}
\usepackage{float}
\usepackage{colortbl}
\usepackage{amsthm}
\usepackage{units}
\usepackage{mathabx}
\usepackage{hyperref}
\usepackage[noend]{algorithmic}
\changenotsign

\renewcommand{\today}{15 March 2012}

\definecolor{DarkBlue}{rgb}{0.00,0.00,0.55}
\hypersetup{
    linkcolor   = DarkBlue,
    anchorcolor = DarkBlue,
    citecolor   = DarkBlue,
    filecolor   = DarkBlue,
    pagecolor   = DarkBlue,
    urlcolor    = DarkBlue,
    colorlinks  = true,
    pdftitle    = {Numerical Methods 1},
}


\definecolor{paleblue}{rgb}{0.9,0.95,1.0}
\definecolor{palegrey}{rgb}{0.9,0.9,0.9}

\newcommand{\real}[1]{\mathrm{Re}(#1)}
\newcommand{\imag}[1]{\mathrm{Im}(#1)}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\dx}{\,\d x}
\newcommand{\dy}{\,\d y}
\newcommand{\dt}{\,\d t}
\newcommand{\ddx}[2][x]{\frac{\d#2}{\d#1}}
\newcommand{\ddxx}[2][x]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ddt}[2][t]{\frac{\d#2}{\d#1}}
\newcommand{\ddtt}[2][t]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ppx}[2][x]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppy}[2][y]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppz}[2][z]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppt}[2][\theta]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppr}[2][r]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppxx}[2][x]{\frac{\partial^2#2}{\partial#1^2}}
\newcommand{\mat}[1]{\mathrm{#1}}
\DeclareMathOperator{\len}{len}

\newcommand{\A}{\mat A}
\newcommand{\D}{\mat D}
\newcommand{\AT}{\mat A^{\mat T}}
\renewcommand{\L}{\mat L}
\newcommand{\U}{\mat U}
\renewcommand{\P}{\mat P}

\newcommand{\Integer}{\mathbb{Z}}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\Rational}{\mathbb{Q}}
\newcommand{\Complex}{\mathbb{C}}
\newcommand{\Natural}{\mathbb{N}}

\renewcommand{\O}{\mathcal{O}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\x}{\vec{x}}
\renewcommand{\b}{\vec{b}}

\newcommand{\eps}{\epsilon_{\mathrm{machine}}}

\DeclareMathOperator{\shape}{shape}

\newcommand{\intpi}{\int_{-\pi}^{\pi}}


\newcommand{\T}{{\color{green}T}}
\newcommand{\F}{{\color{red}F}}

\setlength{\parskip}{2ex}
\setlength{\parindent}{0ex}

\floatstyle{boxed}
\newfloat{biography}{tp}{lob}

\usepackage{listings}

\lstloadlanguages{Python}

\lstset{basicstyle=\ttfamily,framerule=0.5ex, backgroundcolor=\color{paleblue},gobble=2}

\lstnewenvironment{pythonprogram}{\lstset{language=Python,frame=l,rulecolor=\color{blue}}}{}
\lstnewenvironment{pythonsnippet}{\lstset{language=Python}}{}

\lstMakeShortInline[language=Python]{~}

\newcommand{\note}[1]{{\Large\color{red}#1}}

\renewcommand{\marks}[1]{

\mbox{}\hfill\emph{(#1 marks)}}


\begin{document}
\title{3.09 Numerical Methods 1\\
  Programming exercise}

\maketitle

\section*{Instructions to candidates}

\subsection*{Answering questions}

Each question consists of instructions for a piece of Python code which you
are to write. In each case, the question will specify both the
\emph{algorithm}\ to be employed and the \emph{interface}\ you are to write.
Marks will be awarded for all of the following:
\begin{enumerate}
\item Correct implementation of the algorithm.
\item Correct implementation of the specified interface.
\item Quality of commenting. The comments should be such that another NM1 student would understand the code without difficulty.
\item Logical structure of code: the sequence of statements in the
  code must make logical sense.

\end{enumerate}

\subsection*{Time limit}

You may work on the problems until 1200 if you wish, however the programming
exercise should take much less time than this.

\subsection*{Submitting your answer}

You must submit your answer by uploading a .tar or .zip archive to ESESIS. This
archive must contain the file containing your answers. I suggest you
create a new folder in your home directory for this exercise. From your home
directory, you can then use a command like:
\begin{verbatim}
 tar cfvz answers.tgz dir_name
\end{verbatim}
This creates the archive \verb+answers.tgz+ for you to upload. Of course
you should use the name of your directory, not \verb+dir_name+. To double check the
archive before you upload it, type:
\begin{verbatim}
 tar tfvz answers.tgz
\end{verbatim}
This will list all the files in your archive for you to check.

\subsection*{Allowed materials}

This is an open book exercise. You may use the course text, lecture notes,
your own notes and any other written material which you choose.

\subsection*{Network access}

You may use the web to, for example, access Python documentation. However,
you may \textbf{not}\ use any communication protocol including, but not
limited to, email, any chat program, posting on internet fora, or social
network sites. In essence you may use the web as a reference but you may not
use it to communicate with anyone during the exercise. 

You must ensure that any communication programmes are shut down for the
duration of the exercise.

Violation of this rule is cheating and may constitute an examination offence
under college rules with very serious consequences.

\pagebreak

The Jacobi method is an \emph{iterative}\ algorithm for solving matrix systems:
\begin{equation}
  \A\x=\b
\end{equation}
where $\A$ is \emph{diagonally dominant}. A matrix is diagonally dominant if
the diagonal entry on each row is larger in magnitude than the sum of all
the other entries on that row. That is, for any $i$:
\begin{equation}
|A_{i,i}|>\sum_{i\neq j}|A_{i,j}|  
\end{equation}
The Jacobi method operates by splitting the matrix $\A$ into a matrix
$\mat{D}$ consisting of the diagonal of $\A$ and matrix $\mat R$ consisting
of the rest of $\A$. That is:
\begin{gather}
  \mat D_{i,j}=
  \begin{cases}
    \A_{i,i} & \textrm{ where } i=j\\
    0 & \textrm{ where } i\neq j
  \end{cases}\label{eq:D}\\
  \mat R_{i,j}=
  \begin{cases}
    0 & \textrm{ where } i=j\\
    \A_{i,j} & \textrm{ where } i\neq j
  \end{cases}\label{eq:R}
\end{gather}

The Jacobi method makes use of $\mat D^{-1}$, the inverse of $\mat D$. The
inverse of a diagonal matrix is very easy to calculate, it is simply given
by taking the reciprocal of each of the diagonal entries:
\begin{equation}
  \mat D^{-1} =
  \begin{bmatrix}
    \D_{0,0} & 0 & \cdots & 0 \\
    0 & \D_{1,1} & & \vdots \\
    \vdots & & \ddots & 0 \\
    0 & \ldots & 0 & \D_{n,n}
  \end{bmatrix}^{-1}
  =
  \begin{bmatrix}
    \frac{1}{\D_{0,0}} & 0 & \cdots & 0 \\
    0 & \frac{1}{\D_{1,1}} & & \vdots \\
    \vdots & & \ddots & 0 \\
    0 & \ldots & 0 & \frac{1}{\D_{n,n}}    
  \end{bmatrix}
\end{equation}

As an iterative method, the Jacobi method starts from an initial guess for
$\vec x$ and calculates a series of improved values of $\vec x$. We will use
an initial guess of the zero vector $\vec{0}$. 

As the Jacobi method defines an iteration, it is also necessary to calculate
when the iteration should terminate. For this, we will use the residual:
\begin{equation}
  \vec r = \b-\A\x
\end{equation}
When we start, with $\x=\vec 0$, the residual will be equal to $\b$ (since
$\A\vec 0 = \vec 0$). We will stop the iteration when the residual has
shrunk by a factor of $\epsilon$ where $\epsilon$ is a small positive
parameter supplied by the user. 

The algorithm for the Jacobi method is given by:
\begin{algorithmic}
  \STATE $\x \gets \vec 0$
  \STATE $r_0 \gets \sqrt{\b\cdot\b}$
  \REPEAT
  \STATE $\x \gets \D^{-1}\left(\b-\vec R\x\right)$
  \STATE $\vec r \gets \b - \A\x$
  \UNTIL $\sqrt{\vec r \cdot \vec r}/r_0<\epsilon$ or maximum iterations exceeded.
\end{algorithmic}
\pagebreak

Copy the module ~/numerical-methods-1/random_jacobi.py~ to your working
directory. It contains the routines ~random_dominant~, ~random_nondominant~,
~random_diag~ and ~random_vec~, which you may find useful in testing your
answers to the following as you go along.

\begin{enumerate}
\item Create a module ~jacobi~ containing a function
  ~diagonally_dominant(A)~ which returns ~True~ if the array ~A~ is square
  and diagonally dominant, and ~False~ otherwise.

\marks{20}
\item Add a function ~split_matrix(A)~ to ~jacobi~ which returns a pair of
  arrays ~D, R~ where ~D~ contains is a the diagonal part of ~A~ and
  ~R~ is the remaining part, as given by equations \ref{eq:D}\ and
  \ref{eq:R}, above.
\marks{20}
\item Add a function ~inverse_diag(D)~ to ~jacobi~ which returns the inverse
  of the diagonal matrix ~D~.
\marks{20}
\item Add a function ~solve(A, b, epsilon=1.e-6, N=100)~ to ~jacobi~ which
  returns the solution to the matrix system $\A\x=\b$ computed using the
  Jacobi method. ~epsilon~ should be used as the convergence criterion. Your
  routine should raise ~ValueError~ if $A$ is not diagonally dominant, and
  ~ArithmeticError~ if the maximum number of iterations $N$ is exceeded. You
  may find it useful to call the functions from the previous questions in
  composing your answer.
\marks{40}
\end{enumerate}


\end{document}

