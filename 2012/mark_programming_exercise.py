#/usr/bin/env/python
import numpy
import random_jacobi
import jacobi

def diagonally_dominant(A):

    return A.shape[0]==A.shape[1] and \
        all(2*numpy.diag(abs(A))- abs(A).sum(1)>0)

def split_matrix(A):
    
    D= numpy.eye(len(A))*A
    R= A-D

    return D,R

def inverse_diag(D):

    return numpy.diag(1./numpy.diag(D))

def solve(A, b, epsilon=1.e-6, N=100):

    if not diagonally_dominant(A):
        raise ValueError("A is not a square diagonally dominant matrix")
    
    x=numpy.zeros(len(A))
    r_0=numpy.sqrt(numpy.dot(b,b))
    n=0
    r=b

    D,R = split_matrix(A)
    Dinv= inverse_diag(D)

    while  numpy.sqrt(numpy.dot(r,r))/r_0>epsilon:
        n+=1
        if n>N:
            raise ArithmeticError("Maximum iterations exceeded")
        x=numpy.dot(Dinv, b-numpy.dot(R,x))

        r=b-numpy.dot(A,x)

    return x
    

for i in range(3,8):
    a=random_jacobi.random_dominant(i)

    if jacobi.diagonally_dominant(a) is True:
        print "PASS: size "+`i`+" diagonally dominant."
    else:
        print "FAIL: size "+`i`+" diagonally dominant."

for i in range(3,8):
    a=random_jacobi.random_nondominant(i)
    if (jacobi.diagonally_dominant(a) is False):
        print "PASS: size "+`i`+" not diagonally dominant."
    else:
        print "FAIL: size "+`i`+" not diagonally dominant."

for i in range(3,8):
    a=random_jacobi.random_dominant(i)
    d,r=jacobi.split_matrix(a)
    d_0,r_0=split_matrix(a)
    if (abs(d-d_0)>1.e-12).any():
        print "FAIL: size "+`i`+" split matrix diagonal."
    else:
        print "PASS: size "+`i`+" split matrix diagonal."
    if (abs(r-r_0)>1.e-12).any():
        print "FAIL: size "+`i`+" split matrix off-diagonal."
    else:
        print "PASS: size "+`i`+" split matrix off-diagonal."
        

for i in range(3,8):
    d=random_jacobi.random_diag(i)
    
    if (abs(jacobi.inverse_diag(d)-inverse_diag(d))>1.e-12).any():
        print "FAIL: size "+`i`+" inverse_diag."
    else:
        print "PASS: size "+`i`+" inverse_diag."

for i in range(3,8):
    a=random_jacobi.random_dominant(i)
    b=random_jacobi.random_vec(i)
    A=a.copy()
    x=jacobi.solve(a,b)
    r=b-numpy.dot(A,x)
    if numpy.sqrt(numpy.dot(r,r))/numpy.sqrt(numpy.dot(b,b))>1.e-6:
        print "FAIL: size "+`i`+" solve."
    else:
        print "PASS: size "+`i`+" solve."

for i in range(3,8):
    a=random_jacobi.random_dominant(i)
    b=random_jacobi.random_vec(i)
    try:
        x=jacobi.solve(a,b,epsilon=1.e-12,N=1)
        print "FAIL: size "+`i`+" iteration count."
    except ArithmeticError:
        print "PASS: size "+`i`+" iteration count."

a=numpy.array([[1,2]])
b=numpy.array([2])

try:
    x=jacobi.solve(a,b)
    print "FAIL: nonsquare error."
except ValueError:
    print "PASS: nonsquare error."

a=random_jacobi.random_nondominant(4)
b=random_jacobi.random_vec(4)
try:
    x=jacobi.solve(a,b)
    print "FAIL: nondominant error."
except ValueError:
    print "PASS: nondominant error."
