import numpy

def random_dominant(size):
    """random_dominant(size):
    Return a random size bt size matrix which is guaranteed to be diagonally
    dominant."""

    M = numpy.random.randint(1,10,(size,size)).astype(numpy.double)

    M+= numpy.diag(M.sum(1))

    return M

def random_nondominant(size):
    """random_dominant(size):
    Return a random size bt size matrix which is guaranteed not to be
    diagonally dominant."""

    M = numpy.random.randint(-10,10,(size,size)).astype(numpy.double)
    

    M+= numpy.diag(M.sum(1))

    return M

def random_diag(size):
    """random_diag(size):
    Return a random size by size diagonal matrix with diagonal entries
    between 1 and 10."""

    return numpy.diag(random_vec(size))

def random_vec(size):
    """random_vec(size)
    Return a random size vector with entries between 1 and 10."""

    return numpy.random.randint(1,10,size).astype(numpy.double)
