\documentclass[a4paper,12pt]{article}

\usepackage[margin=2.5cm]{geometry}
%\usepackage{mathpazo}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[usenames,dvipsnames]{color}
\usepackage{float}
\usepackage{colortbl}
\usepackage{amsthm}
\usepackage{units}
\usepackage{mathabx}
\usepackage{hyperref}
\usepackage[noend]{algorithmic}
\changenotsign

\renewcommand{\today}{15 March 2012}

\definecolor{DarkBlue}{rgb}{0.00,0.00,0.55}
\hypersetup{
    linkcolor   = DarkBlue,
    anchorcolor = DarkBlue,
    citecolor   = DarkBlue,
    filecolor   = DarkBlue,
    pagecolor   = DarkBlue,
    urlcolor    = DarkBlue,
    colorlinks  = true,
    pdftitle    = {Numerical Methods 1},
}


\definecolor{paleblue}{rgb}{0.9,0.95,1.0}
\definecolor{palegrey}{rgb}{0.9,0.9,0.9}

\newcommand{\real}[1]{\mathrm{Re}(#1)}
\newcommand{\imag}[1]{\mathrm{Im}(#1)}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\dx}{\,\d x}
\newcommand{\dy}{\,\d y}
\newcommand{\dt}{\,\d t}
\newcommand{\ddx}[2][x]{\frac{\d#2}{\d#1}}
\newcommand{\ddxx}[2][x]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ddt}[2][t]{\frac{\d#2}{\d#1}}
\newcommand{\ddtt}[2][t]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ppx}[2][x]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppy}[2][y]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppz}[2][z]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppt}[2][\theta]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppr}[2][r]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppxx}[2][x]{\frac{\partial^2#2}{\partial#1^2}}
\newcommand{\mat}[1]{\mathrm{#1}}
\DeclareMathOperator{\len}{len}

\newcommand{\A}{\mat A}
\newcommand{\D}{\mat D}
\newcommand{\AT}{\mat A^{\mat T}}
\renewcommand{\L}{\mat L}
\newcommand{\U}{\mat U}
\renewcommand{\P}{\mat P}

\newcommand{\Integer}{\mathbb{Z}}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\Rational}{\mathbb{Q}}
\newcommand{\Complex}{\mathbb{C}}
\newcommand{\Natural}{\mathbb{N}}

\renewcommand{\O}{\mathcal{O}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\x}{\vec{x}}
\renewcommand{\b}{\vec{b}}

\newcommand{\eps}{\epsilon_{\mathrm{machine}}}

\DeclareMathOperator{\shape}{shape}

\newcommand{\intpi}{\int_{-\pi}^{\pi}}


\newcommand{\T}{{\color{green}T}}
\newcommand{\F}{{\color{red}F}}

\setlength{\parskip}{2ex}
\setlength{\parindent}{0ex}

\floatstyle{boxed}
\newfloat{biography}{tp}{lob}

\usepackage{listings}

\lstloadlanguages{Python}

\lstset{basicstyle=\ttfamily,framerule=0.5ex, backgroundcolor=\color{paleblue},gobble=2}

\lstnewenvironment{pythonprogram}{\lstset{language=Python,frame=l,rulecolor=\color{blue}}}{}
\lstnewenvironment{pythonsnippet}{\lstset{language=Python}}{}

\lstMakeShortInline[language=Python]{~}

\newcommand{\note}[1]{{\Large\color{red}#1}}

\renewcommand{\marks}[1]{

\mbox{}\hfill\emph{(#1 marks)}}


\begin{document}
\title{3.09 Numerical Methods 1\\
  Programming exercise feedback}

\maketitle

You completed the programming exercise today, so the programming aspect of
numerical methods 1 is now over. I will have the exercises marked the marks
up on ESESIS by Friday 30 March, but I think you would probably appreciate
some more prompt feedback than this so here is a set of worked answers to
the questions against which you can compare your work. It is important to
recognise that there is more than one way to code any algorithm, so if your
program doesn't look exactly like those here, but it still gives the correct
answer, then that's just fine. 

Copy the module ~/numerical-methods-1/random_jacobi.py~ to your working
directory. It contains the routines ~random_dominant~, ~random_nondominant~,
~random_diag~ and ~random_vec~, which you may find useful in testing your
answers to the following as you go along. In case you didn't take your
question sheet with you, the documentation of the Jacobi method is on the
final page.

\begin{enumerate}
\item Create a module ~jacobi~ containing a function
  ~diagonally_dominant(A)~ which returns ~True~ if the array ~A~ is square
  and diagonally dominant, and ~False~ otherwise.

\textit{You can either complete this question by looping over all the rows
  and checking each one for diagonal dominancem or you can use array
  operations to check all the rows at once. An implementation without a loop
is:}

\begin{pythonsnippet}
  def diagonally_dominant(A):
    '''diagonally_dominant(A)
    
    return True if A is square and diagonally dominant, 
    False otherwise.'''
    
    # Check matrix for being square
    square = numpy.rank(A)==2 and A.shape[0]==A.shape[1] 

    if not square:
        return False

    # Check for diagonal dominance. Multiply the diagonal 
    # by two because the right hand side still contains 
    # the diagonal.
    dominant = all(2*numpy.diag(abs(A))- abs(A).sum(1)>0)

    return dominant
\end{pythonsnippet}

\textit{An equally good answer, this time using a loop, is:}
\begin{pythonsnippet}
def diagonally_dominant(A):
    '''diagonally_dominant(A)
    
    return True if A is square and diagonally dominant, 
    False otherwise.'''

    # Check for matrix being square.
    square= numpy.rank(A)==2 and A.shape[0]==A.shape[1]

    if not square:
        return False

    # Check each row for diagonal dominance.
    for i in range(A.shape[0]):
        if abs(A[i,i])<=sum(abs(A[i,:i]))+sum(abs(A[i,i+1:])):
            return False

    # If we get here, then A is diagonally dominant.
    return True
\end{pythonsnippet}

\item Add a function ~split_matrix(A)~ to ~jacobi~ which returns a pair of
  arrays ~D, R~ where ~D~ contains is a the diagonal part of ~A~ and
  ~R~ is the remaining part, as given by equations \ref{eq:D}\ and
  \ref{eq:R}, above.

\textit{Once again, you can either write a pair of loops to set ~D~ and ~R~
  element-wise, or you can use matrix operations to do it all at once. With
  matrix operations, an answer might look like:}

\begin{pythonsnippet}
  def split_matrix(A):
    """split_matrix(A)

    Split A into A=R+D where D is diagonal and R has 
    zero on the diagonal."""
    
    # Pointwise Multiply A by the identity matrix to 
    # select the diagonal entries. 
    D= numpy.eye(len(A))*A
    # R is simply the remaining entries.
    R= A-D

    return D,R
\end{pythonsnippet}

\textit{a loop-based answer might look like:}
\begin{pythonsnippet}
  def split_matrix(A):
    """split_matrix(A)

    Split A into A=R+D where D is diagonal and R has 
    zero on the diagonal."""

    D=numpy.zeros(A.shape)
    R=numpy.zeros(A.shape)

    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if i == j:
                # Diagonal entries go into D.
                D[i,i]=A[i,i]
            else:
                # Off-diagonal entries go into R.
                R[i,j]=A[i,j]

    return D, R
\end{pythonsnippet}

\item Add a function ~inverse_diag(D)~ to ~jacobi~ which returns the inverse
  of the diagonal matrix ~D~.

\textit{The trap in this question is to make sure you only take the
  reciprocal of the diagonal entries. If you take the reciprocal of the
  off-diagonal entries, then you'll end up dividing by zero. Here's a
  version using matrix operations:}

\begin{pythonsnippet}
  def inverse_diag(D):
    """inverse_diag(D)

    Return the inverse of the diagonal matrix D."""

    # numpy.diag(D) extracts the diagonal. The outer 
    # numpy.diag rebuilds a diagonal matrix using the 
    # reciprocal of the original diagonal.
    return numpy.diag(1./numpy.diag(D))
\end{pythonsnippet}

\textit{and here's one using loops:}

\begin{pythonsnippet}
  def inverse_diag(D):
    """inverse_diag(D)

    Return the inverse of the diagonal matrix D."""

    invD = numpy.zeros(D.shape)

    # Take the reciprocal of every diagonal entry.
    for i in range(len(D)):
        invD[i,i]=1./D[i,i]

    return invD
\end{pythonsnippet}

\item Add a function ~solve(A, b, epsilon=1.e-6, N=100)~ to ~jacobi~ which
  returns the solution to the matrix system $\A\x=\b$ computed using the
  Jacobi method. ~epsilon~ should be used as the convergence criterion. Your
  routine should raise ~ValueError~ if $A$ is not diagonally dominant, and
  ~ArithmeticError~ if the maximum number of iterations $N$ is exceeded. You
  may find it useful to call the functions from the previous questions in
  composing your answer.

\textit{For this question, using loops makes life very complicated, so I've
  just coded up the simple version using matrix operations:}

\begin{pythonsnippet}
  def solve(A, b, epsilon=1.e-6, N=100):
    """solve(A, b, epsilon=1.e-6, N=100)

    Solve Ax=b using the jacobi iteration. Convergence is 
    achieved when the norm of the residual has decreased 
    by a factor of epsilon. 
    A maximum of N iterations will be attempted.
    """

    if not diagonally_dominant(A):
        raise ValueError("A is not a square diagonally dominant matrix")
    
    # Use the zero vector as an initial guess.
    x=numpy.zeros(len(A))
    # Calculate the initial residual as the norm of b.
    r_0=numpy.sqrt(numpy.dot(b,b))
    # Start the iteration counter.
    n=0
    # The initial residual is equal to b.
    r=b

    D,R = split_matrix(A)
    Dinv= inverse_diag(D)

    # Check for convergence.A
    while  numpy.sqrt(numpy.dot(r,r))/r_0>epsilon:
        # Increment the counter and error if we 
        # are out of iterations.
        n+=1
        if n>N:
            raise ArithmeticError("Maximum iterations exceeded")

        # Calculate the next iteration.
        x=numpy.dot(Dinv, b-numpy.dot(R,x))
        # Calculate the residual in order to check convergence.
        r=b-numpy.dot(A,x)

    # If we get here, convergence was achieved, so 
    # we return the answer.
    return x
\end{pythonsnippet}

\textit{You weren't asked to provide test code for your module, so no marks
  are associated with that. However you hopefully will have tested your code
  as you went along to make sure it does the right thing. The following code
  illustrates some of the tests which are relevant. Remember, this is just
  for your information, you are \textbf{not}\  expected to have included
  test code in your answer:}

  \begin{pythonsnippet}
  if __name__=="__main__":
    import random_jacobi

    for i in range(3,8):
        a=random_jacobi.random_dominant(i)
        
        if diagonally_dominant(a):
            print "PASS: size "+`i`+" diagonally dominant."
        else:
            print "FAIL: size "+`i`+" diagonally dominant."

    for i in range(3,8):
        a=random_jacobi.random_nondominant(i)
        if not diagonally_dominant(a):
            print "PASS: size "+`i`+" not diagonally dominant."
        else:
            print "FAIL: size "+`i`+" not diagonally dominant."

    for i in range(3,8):
        a=random_jacobi.random_dominant(i)
        d,r=split_matrix(a)
        d_0,r_0=split_matrix(a)
        if (abs(d-d_0)>1.e-12).any():
            print "FAIL: size "+`i`+" split matrix diagonal."
        else:
            print "PASS: size "+`i`+" split matrix diagonal."
        if (abs(r-r_0)>1.e-12).any():
            print "FAIL: size "+`i`+" split matrix off-diagonal."
        else:
            print "PASS: size "+`i`+" split matrix off-diagonal."

    for i in range(3,8):
        d=random_jacobi.random_diag(i)
    
        if (abs(inverse_diag(d)-inverse_diag(d))>1.e-12).any():
            print "FAIL: size "+`i`+" inverse_diag."
        else:
            print "PASS: size "+`i`+" inverse_diag."

    for i in range(3,8):
        a=random_jacobi.random_dominant(i)
        b=random_jacobi.random_vec(i)
        x=solve(a,b)
        r=b-numpy.dot(a,x)
        # Notice that the residual is only as small
        # as the convergence criteria, it's not 0.
        if numpy.sqrt(numpy.dot(r,r))/numpy.sqrt(numpy.dot(b,b))\
           >1.e-6:
            # Solve didn't produce the right answer!
            print "FAIL: size "+`i`+" solve."
        else:
            print "PASS: size "+`i`+" solve."

    for i in range(3,8):
        a=random_jacobi.random_dominant(i)
        b=random_jacobi.random_vec(i)
        try:
            # Solve only allowing one iteration.
            x=solve(a,b,epsilon=1.e-12,N=1)
            # We should exception before we get here.
            print "FAIL: size "+`i`+" iteration count."
        except ArithmeticError:
            print "PASS: size "+`i`+" iteration count."

    a=numpy.array([1,2])
    b=numpy.array([2])

    try:
        x=solve(a,b)
        # We should exception before we get here.
        print "FAIL: nonsquare error."
    except ValueError:
        print "PASS: nonsquare error."

    a=random_jacobi.random_nondominant(4)
    b=random_jacobi.random_vec(4)
    try:
        x=solve(a,b)
        # We should exception before we get here.
        print "FAIL: nondominant error."
    except ValueError:
        print "PASS: nondominant error."

  \end{pythonsnippet}

\end{enumerate}


\pagebreak

The Jacobi method is an \emph{iterative}\ algorithm for solving matrix systems:
\begin{equation}
  \A\x=\b
\end{equation}
where $\A$ is \emph{diagonally dominant}. A matrix is diagonally dominant if
the diagonal entry on each row is larger in magnitude than the sum of all
the other entries on that row. That is, for any $i$:
\begin{equation}
|A_{i,i}|>\sum_{i\neq j}|A_{i,j}|  
\end{equation}
The Jacobi method operates by splitting the matrix $\A$ into a matrix
$\mat{D}$ consisting of the diagonal of $\A$ and matrix $\mat R$ consisting
of the rest of $\A$. That is:
\begin{gather}
  \mat D_{i,j}=
  \begin{cases}
    \A_{i,i} & \textrm{ where } i=j\\
    0 & \textrm{ where } i\neq j
  \end{cases}\label{eq:D}\\
  \mat R_{i,j}=
  \begin{cases}
    0 & \textrm{ where } i=j\\
    \A_{i,j} & \textrm{ where } i\neq j
  \end{cases}\label{eq:R}
\end{gather}

The Jacobi method makes use of $\mat D^{-1}$, the inverse of $\mat D$. The
inverse of a diagonal matrix is very easy to calculate, it is simply given
by taking the reciprocal of each of the diagonal entries:
\begin{equation}
  \mat D^{-1} =
  \begin{bmatrix}
    \D_{0,0} & 0 & \cdots & 0 \\
    0 & \D_{1,1} & & \vdots \\
    \vdots & & \ddots & 0 \\
    0 & \ldots & 0 & \D_{n,n}
  \end{bmatrix}^{-1}
  =
  \begin{bmatrix}
    \frac{1}{\D_{0,0}} & 0 & \cdots & 0 \\
    0 & \frac{1}{\D_{1,1}} & & \vdots \\
    \vdots & & \ddots & 0 \\
    0 & \ldots & 0 & \frac{1}{\D_{n,n}}    
  \end{bmatrix}
\end{equation}

As an iterative method, the Jacobi method starts from an initial guess for
$\vec x$ and calculates a series of improved values of $\vec x$. We will use
an initial guess of the zero vector $\vec{0}$. 

As the Jacobi method defines an iteration, it is also necessary to calculate
when the iteration should terminate. For this, we will use the residual:
\begin{equation}
  \vec r = \b-\A\x
\end{equation}
When we start, with $\x=\vec 0$, the residual will be equal to $\b$ (since
$\A\vec 0 = \vec 0$). We will stop the iteration when the residual has
shrunk by a factor of $\epsilon$ where $\epsilon$ is a small positive
parameter supplied by the user. 

The algorithm for the Jacobi method is given by:
\begin{algorithmic}
  \STATE $\x \gets \vec 0$
  \STATE $r_0 \gets \sqrt{\b\cdot\b}$
  \REPEAT
  \STATE $\x \gets \D^{-1}\left(\b-\vec R\x\right)$
  \STATE $\vec r \gets \b - \A\x$
  \UNTIL $\sqrt{\vec r \cdot \vec r}/r_0<\epsilon$ or maximum iterations exceeded.
\end{algorithmic}



\end{document}

