\documentclass[a4paper,12pt]{article}

\usepackage[margin=2.5cm]{geometry}
%\usepackage{mathpazo}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[usenames,dvipsnames]{color}
\usepackage{float}
\usepackage{colortbl}
\usepackage{amsthm}
\usepackage{units}
\usepackage{mathabx}
\usepackage{hyperref}
\changenotsign

\definecolor{DarkBlue}{rgb}{0.00,0.00,0.55}
\hypersetup{
    linkcolor   = DarkBlue,
    anchorcolor = DarkBlue,
    citecolor   = DarkBlue,
    filecolor   = DarkBlue,
    pagecolor   = DarkBlue,
    urlcolor    = DarkBlue,
    colorlinks  = true,
    pdftitle    = {Numerical Methods 1},
}


\definecolor{paleblue}{rgb}{0.9,0.95,1.0}
\definecolor{palegrey}{rgb}{0.9,0.9,0.9}

\newcommand{\real}[1]{\mathrm{Re}(#1)}
\newcommand{\imag}[1]{\mathrm{Im}(#1)}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\dx}{\,\d x}
\newcommand{\dy}{\,\d y}
\newcommand{\dt}{\,\d t}
\newcommand{\ddx}[2][x]{\frac{\d#2}{\d#1}}
\newcommand{\ddxx}[2][x]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ddt}[2][t]{\frac{\d#2}{\d#1}}
\newcommand{\ddtt}[2][t]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ppx}[2][x]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppy}[2][y]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppz}[2][z]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppt}[2][\theta]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppr}[2][r]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppxx}[2][x]{\frac{\partial^2#2}{\partial#1^2}}
\newcommand{\mat}[1]{\mathrm{#1}}
\DeclareMathOperator{\len}{len}

\newcommand{\A}{\mat A}
\newcommand{\AT}{\mat A^{\mat T}}
\renewcommand{\L}{\mat L}
\newcommand{\U}{\mat U}
\renewcommand{\P}{\mat P}

\newcommand{\Integer}{\mathbb{Z}}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\Rational}{\mathbb{Q}}
\newcommand{\Complex}{\mathbb{C}}
\newcommand{\Natural}{\mathbb{N}}

\renewcommand{\O}{\mathcal{O}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\x}{\vec{x}}

\newcommand{\eps}{\epsilon_{\mathrm{machine}}}

\DeclareMathOperator{\shape}{shape}

\newcommand{\intpi}{\int_{-\pi}^{\pi}}


\newcommand{\T}{{\color{green}T}}
\newcommand{\F}{{\color{red}F}}

\setlength{\parskip}{2ex}
\setlength{\parindent}{0ex}

\floatstyle{boxed}
\newfloat{biography}{tp}{lob}

\usepackage{listings}

\lstloadlanguages{Python}

\lstset{basicstyle=\ttfamily,framerule=0.5ex, backgroundcolor=\color{paleblue},gobble=2}

\lstnewenvironment{pythonprogram}{\lstset{language=Python,frame=l,rulecolor=\color{blue}}}{}
\lstnewenvironment{pythonsnippet}{\lstset{language=Python}}{}

\lstMakeShortInline[language=Python]{~}

\newcommand{\note}[1]{{\Large\color{red}#1}}

\begin{document}
\title{3.09 Numerical Methods 1\\
  Programming exercise}

\maketitle

\section*{Instructions to candidates}

\subsection*{Answering questions}

Each question consists of instructions for a piece of Python code which you
are to write. In each case, the question will specify both the
\emph{algorithm}\ to be employed and the \emph{interface}\ you are to write.
Marks will be awarded for all of the following:
\begin{enumerate}
\item Correct implementation of the algorithm.
\item Correct implementation of the specified interface.
\item Quality of commenting. The comments should be such that another NM1 student would understand the code without difficulty.
\item Logical structure of code: does the sequence of statements in the
  code must make logical sense.

\end{enumerate}

\subsection*{Time limit}

You may work on the problems until 1700 if you wish, however the programming
exercise should take much less time than this.

\subsection*{Submitting your answer}

You must submit your answer by uploading a .tar or .zip archive to ESESIS. This
archive must contain all of the files containing your answers. I suggest you
create a new folder in your home directory for this exercise. From your home
directory, you can then use a command like:
\begin{verbatim}
 tar cfvz answers.tgz dir_name
\end{verbatim}
This creates the archive \verb+answers.tgz+ for you to upload. Of course
you should use the name of your directory, not \verb+dir_name+. To double check the
archive before you upload it, type:
\begin{verbatim}
 tar tfvz answers.tgz
\end{verbatim}
This will list all the files in your archive for you to check.

\subsection*{Allowed materials}

This is an open book exercise. You may use the course text, lecture notes,
your own notes and any other written material which you choose.

\subsection*{Network access}

You may use the web to, for example, access Python documentation. However,
you may \textbf{not}\ use any communication protocol including, but not
limited to, email, any chat program, posting on internet fora, or social
network sites. In essence you may use the web as a reference but you may not
use it to communicate with anyone during the exercise. 

You must ensure that any communication programmes are shut down for the
duration of the exercise.

Violation of this rule is cheating and may constitute an examination offence
under college rules with very serious consequences.

\pagebreak

\begin{enumerate}
  \item Create a module ~function.py~ which contains a function ~sin_cos(x)~
    which implements the following function:
    \begin{equation*}
      f(x)=\sin(x^2)\cos(|x|)
    \end{equation*}

  \item The Taylor series for $\sin(x)$ evaluated near zero is given by:
    \begin{equation*}
      \sin(x)=\sum_{k=0}^\infty (-1)^k\frac{x^{2k+1}}{(2k+1)!}
    \end{equation*}
    \begin{enumerate}
    \item Create a module ~series.py~ which contains a function
      ~taylor_sin(x,n)~. This function should evaluate the first $n$ entries
      in the Taylor series above and return the result. Count the $k=0$
      entry as the first entry. You may find the function ~factorial~
      function from the ~math~ module useful.
    \item Create a Python program ~plot_sin_series.py~ which uses your
      ~series~ module to plot a graph of ~sin(x)~ for $-\pi<0<\pi$. Use only
      the first 3 entries in the Taylor series (ie set $n=3$).

      Your program should also plot $\sin(x)$ on the same axes for the same
      range of $x$ values using the ~sin~ function from ~numpy~. Note that
      the two lines will diverge as $x$ gets further from zero. This is a
      programming exercise so you are to hand in the program which makes the
      plot, not the plot itself.
    \end{enumerate}

  \item Write a module ~linear_algebra.py~ containing a function
    ~matvec(A,b)~ which takes a rank 2 Numpy array ~A~ and a rank 1 numpy
    array ~b~ and returns the corresponding matrix-vector product. Your
    programme should raise ~ValueError~ if the shapes of ~A~ and ~b~ are
    incompatible. \textbf{In answering this question, you may not make use
      of the ~dot~ function from ~numpy~, ~scitools~ or elsewhere}.
\end{enumerate}

\end{document}

