def function_1(a,b):
    from numpy import dot,zeros

    c=zeros((a.shape[0],b.shape[1]))
    
    for i in range(a.shape[0]):
        for j in range(b.shape[1]):
            c[i,j]=dot(a[i,:],b[:,j])
                    
    return c

def function_2(a,b):
    from numpy import dot,zeros

    c=zeros(a.shape[1])
    
    for i in range(a.shape[1]):
        c[i]=dot(a[:,i],b)
                    
    return c


def find_root(f, a, b, eps):
    if f(a)*f(b) > 0:
        return None, 0

    while b-a > eps:
        m = (a + b)/2.0
        print a,b,b-a,m
        if f(a)*f(m) <= 0:
            b = m  # root is in left half of [a,b]
            print 'Root in left half'
        else:
            a = m  # root is in right half of [a,b]
            print 'Root in right half'
    print m
    return m

def test_find_root():
    def g(x):
        return x

    find_root(g, -0.5, 1.,0.5)
