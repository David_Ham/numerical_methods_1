\documentclass[a4paper,12pt]{book}

\usepackage[margin=2.5cm]{geometry}
%\usepackage{mathpazo}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[usenames,dvipsnames]{color}
\usepackage{float}
\usepackage{colortbl}
\usepackage{amsthm}
\usepackage{units}
\usepackage{mathabx}
\usepackage{hyperref}
\changenotsign

\definecolor{DarkBlue}{rgb}{0.00,0.00,0.55}
\hypersetup{
    linkcolor   = DarkBlue,
    anchorcolor = DarkBlue,
    citecolor   = DarkBlue,
    filecolor   = DarkBlue,
    pagecolor   = DarkBlue,
    urlcolor    = DarkBlue,
    colorlinks  = true,
    pdftitle    = {Numerical Methods 1},
}


\definecolor{paleblue}{rgb}{0.9,0.95,1.0}
\definecolor{palegrey}{rgb}{0.9,0.9,0.9}

\renewcommand{\chaptername}{Lecture}

\newcommand{\lecture}[1]{\chapter{#1}}

\newcommand{\real}[1]{\mathrm{Re}(#1)}
\newcommand{\imag}[1]{\mathrm{Im}(#1)}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\dx}{\,\d x}
\newcommand{\dy}{\,\d y}
\newcommand{\dt}{\,\d t}
\newcommand{\ddx}[2][x]{\frac{\d#2}{\d#1}}
\newcommand{\ddxx}[2][x]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ddt}[2][t]{\frac{\d#2}{\d#1}}
\newcommand{\ddtt}[2][t]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ppx}[2][x]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppy}[2][y]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppz}[2][z]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppt}[2][\theta]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppr}[2][r]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppxx}[2][x]{\frac{\partial^2#2}{\partial#1^2}}
\newcommand{\mat}[1]{\mathrm{#1}}
\DeclareMathOperator{\len}{len}

\newcommand{\A}{\mat A}
\newcommand{\AT}{\mat A^{\mat T}}
\renewcommand{\L}{\mat L}
\newcommand{\U}{\mat U}
\renewcommand{\P}{\mat P}

\newcommand{\Integer}{\mathbb{Z}}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\Rational}{\mathbb{Q}}
\newcommand{\Complex}{\mathbb{C}}
\newcommand{\Natural}{\mathbb{N}}

\renewcommand{\O}{\mathcal{O}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\x}{\vec{x}}

\newcommand{\eps}{\epsilon_{\mathrm{machine}}}

\DeclareMathOperator{\shape}{shape}

\newcommand{\intpi}{\int_{-\pi}^{\pi}}

\theoremstyle{definition}
\newtheorem{exercise}{Exercise}[chapter]
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{define}[theorem]{Definition}

\newcommand{\T}{{\color{green}T}}
\newcommand{\F}{{\color{red}F}}

\setlength{\parskip}{2ex}
\setlength{\parindent}{0ex}

\floatstyle{boxed}
\newfloat{biography}{tp}{lob}

\usepackage{listings}

\lstloadlanguages{Python}

\lstset{basicstyle=\ttfamily,framerule=0.5ex, backgroundcolor=\color{paleblue},gobble=2}

\lstnewenvironment{pythonprogram}{\lstset{language=Python,frame=l,rulecolor=\color{blue}}}{}
\lstnewenvironment{pythonsnippet}{\lstset{language=Python}}{}

\lstMakeShortInline[language=Python]{~}

\newcommand{\note}[1]{{\Large\color{red}#1}}

\setcounter{secnumdepth}{0}

\begin{document}
\begin{center}
  \LARGE\bfseries Numerical Methods 1 \\
  Exam formula sheet

\end{center}

\subsection{Floating point numbers}

Binary floating point numbers have the form:
\begin{equation*}
 s1.m\times 2^{e-b}
\end{equation*}

Where:
\begin{itemize}
\item $s$ is the sign (0 for +, 1 for -).
\item $1.m$ is the mantissa.
\item $e$ is the exponent.
\item $b$ is the bias.
\end{itemize}
$s$,$m$,$e$, and $b$ are written in binary.

The bit pattern of a floating point number with 3 exponent bits and 4
mantissa bits is:
\[
\begin{array}{|c|c|c|c|>{\columncolor{palegrey}}c|c|c|c|c|}
  \hline
  s & e & e & e & 1 & m & m & m & m \\\hline
  \pm & 2^2 & 2^1 & 2^0 & 2^0 & 2^{-1} & 2^{-2} & 2^{-3} & 2^{-4} \\\hline
\end{array}
\]
where the leading 1 in the mantissa is not actually stored.

\subsection{Taylor series}

\begin{equation*}
  f(x+h)=\sum_{k=0}^n \frac{h^k}{k!}f^{(k)}(x)+\O(h^{n+1})
\end{equation*}

\subsection{Matrix operations}
The following are true for any matrices or vectors $\mat A$,$\mat
B$,$\mat C$ for which the corresponding dimensions match and for any scalar
$\alpha$:
\begin{itemize}
\item $\mat A\cdot(\mat B\cdot\mat C)=(\mat A\cdot\mat B)\cdot\mat C$
\item $\mat A\cdot(\mat B+\mat C)=\mat A\cdot \mat B+\mat A\cdot \mat C$
\item $(\mat A+\mat B)\cdot \mat C=\mat A\cdot \mat C+\mat A\cdot \mat C$
\item $\mat A+\mat B=\mat B+\mat A$
\item $\alpha \mat A\cdot \mat B= \mat A\cdot\alpha \mat B$
\end{itemize}

\subsection{Linear independence}

For a square matrix $\A$, the following statements are equivalent:

\begin{enumerate}
\item The columns of $\mat{A}$ are linearly independent.
\item The span of the columns of $\A$ is $\Real^n$.
\item $\A$ is invertible. i.e. there exists a matrix $\mat A^{-1}$ such that
  $\A\A^{-1}=\mat{I}$
\item $\A\x=\vec b$ has a unique solution for every $\vec b\in\Real^n$.
\item The unique solution to $\A\x=\vec 0$ is the zero vector, $\vec 0$.
\item $\det(\A)\neq 0$.
\item $0$ is not an eigenvalue of $\A$.
\item $\A$ has full rank.
\end{enumerate}

\end{document}

