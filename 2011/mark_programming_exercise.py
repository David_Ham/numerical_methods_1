#!/usr/bin/python
from numpy import *
import sys
import math
import random_matrices
import traceback

def sin_cos(x):
    return sin(x**2)*cos(abs(x))

def taylor_sin(x,n):

    s=0.0

    for k in range(n):
        s+=(-1)**k*x**(2*k+1)/math.factorial(2*k+1)

    return s

sys.path.insert(0,'.')

x=linspace(-pi,pi)

# try:
#     import function

#     yref=sin_cos(x)
#     ysol=zeros(size(x))
#     for i in range(size(x)):
#         ysol[i]=function.sin_cos(x[i])

#     err=max(abs(yref-ysol))
#     if err>1.e-14:
#         print "sin_cos returns incorrect answers. Max error:",`err`
#     else:
#         print "sin_cos is correct. Max error:",`err`
# except:
#     print "sin_cos raised an exception"
#     traceback.print_exc()

# n=-1
# try:
#     import series

#     for n in range(1,6):
#         yref=taylor_sin(x,n)
#         ysol=zeros(size(x))
#         for i in range(size(x)):
#             ysol[i]=series.taylor_sin(x[i],n)

#         err=max(abs(yref-ysol))
#         if err>1.e-14:
#             print "taylor_sin returns incorrect answers. Max error:",`err`,\
#                   "n= ",n
#         else:
#             print "taylor_sin is correct. Max error:",`err`,"n= ",n
# except:
#     print "series raised an exception."
#     print "n= ",n
#     traceback.print_exc()


n=-1
m=-1
try:
    import linear_algebra

    error=False
    for n in range(1,10):
        for m in range(1,10):
            A=random_matrices.random_nonsquare((n,m))
            b=random_matrices.random_vec(m)

            err=max(abs(linear_algebra.matvec(A,b)-dot(A,b)))
            if err>1.e-14:
                print "Matrix error n=",`n`," m=",`m`," err=",err
                error =True

    if not error:
        print "Matrix vector successful"


except:
    exc_type, exc_value, exc_traceback = sys.exc_info()
    print "linear_algebra raised an exception"
    print "n= ",n,"m= ",m
    traceback.print_exc()

A=random_matrices.random_nonsquare((3,3))
b=random_matrices.random_vec(2)

try:
    import linear_algebra

    try:
        c=linear_algebra.matvec(A,b)
        
        print "Error trapping failed"
        
    except ValueError:
        
        print "Error trapping succeeded"

except:
    print "import linear_algebra raised an exception"

