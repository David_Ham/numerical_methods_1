\documentclass[a4paper, 11pt]{article}


\usepackage[margin=2.5cm]{geometry}
%\usepackage{mathpazo}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{float}
\usepackage{colortbl}
\usepackage{amsthm}
\usepackage{units}
\usepackage{mathabx}
\usepackage{hyperref}
\changenotsign

\definecolor{DarkBlue}{rgb}{0.00,0.00,0.55}
\hypersetup{
    linkcolor   = DarkBlue,
    anchorcolor = DarkBlue,
    citecolor   = DarkBlue,
    filecolor   = DarkBlue,
    pagecolor   = DarkBlue,
    urlcolor    = DarkBlue,
    colorlinks  = true,
    pdftitle    = {Numerical Methods 1},
}


\definecolor{paleblue}{rgb}{0.9,0.95,1.0}
\definecolor{palegrey}{rgb}{0.9,0.9,0.9}

\newcommand{\real}[1]{\mathrm{Re}(#1)}
\newcommand{\imag}[1]{\mathrm{Im}(#1)}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\dx}{\,\d x}
\newcommand{\dy}{\,\d y}
\newcommand{\dt}{\,\d t}
\newcommand{\ddx}[2][x]{\frac{\d#2}{\d#1}}
\newcommand{\ddxx}[2][x]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ddt}[2][t]{\frac{\d#2}{\d#1}}
\newcommand{\ddtt}[2][t]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ppx}[2][x]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppy}[2][y]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppz}[2][z]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppt}[2][\theta]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppr}[2][r]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppxx}[2][x]{\frac{\partial^2#2}{\partial#1^2}}
\newcommand{\mat}[1]{\mathrm{#1}}

\newcommand{\A}{\mat A}
\renewcommand{\L}{\mat L}
\newcommand{\U}{\mat U}

\newcommand{\Integer}{\mathbb{Z}}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\Rational}{\mathbb{Q}}
\newcommand{\Complex}{\mathbb{C}}
\newcommand{\Natural}{\mathbb{N}}

\renewcommand{\O}{\mathcal{O}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\x}{\vec{x}}

\newcommand{\eps}{\epsilon_{\mathrm{machine}}}

\newcommand{\intpi}{\int_{-\pi}^{\pi}}

\newcounter{chapter}

\theoremstyle{definition}
\newtheorem{exercise}{Exercise}[chapter]
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{define}[theorem]{Definition}

\newcommand{\T}{{\color{green}T}}
\newcommand{\F}{{\color{red}F}}

\setlength{\parskip}{2ex}
\setlength{\parindent}{0ex}

\floatstyle{boxed}
\newfloat{biography}{tp}{lob}

\usepackage{listings}

\lstloadlanguages{Python}

\lstset{basicstyle=\ttfamily,framerule=0.5ex, backgroundcolor=\color{paleblue},gobble=2}

\lstnewenvironment{pythonprogram}{\lstset{language=Python,frame=l,rulecolor=\color{blue}}}{}
\lstnewenvironment{pythonsnippet}{\lstset{language=Python}}{}

\lstMakeShortInline[language=Python]{~}

\newcommand{\note}[1]{{\Large\color{red}#1}}


\begin{document}
\title{Feedback on exercise 3.1 - mathematical proofs.}
\maketitle

Exercise 3.1 seems to have caused most people a lot of trouble so this is a
write-up of how to approach this sort of question in painstaking
detail. Questions of this sort are common in maths modules so it is
important to understand how to do them. As
a reminder, the exercise was:

\setcounter{chapter}{3}

\begin{exercise}
  Suppose that $E(h)$ is $\O(h^n)$. If we define:
  \begin{equation*}
    F(h)=E(h)/h
  \end{equation*}
  show that $F(h)$ is $\O(h^{n-1})$
\end{exercise}

\subsection*{Identifying proof questions}

The first thing to notice in this question is the magic word ``show''. In
mathematics courses, a question in which the main verb is ``show'' or
``prove'' is almost always asking you for a mathematical proof, and that is
true here.

\subsection*{Identifying what information you have and what you have to
  prove}

Exercises and exam questions are usually contrived to give you exactly as
much information as is needed, no more and no less. It is therefore
important to work out exactly what information you are being given as you
will need all of it. In this case, you are given two pieces of information:
\begin{enumerate}
\item $E(h)$ is $\O(h^n)$
\item $F(h)=E(h)/h$
\end{enumerate}
It is also very important to work out exactly what it is you are being asked
to prove. In this case, you are being asked to prove that $F(h)$ is
$\O(h^{n-1})$.

So, we have two pieces of information and we are asked to prove one
statement. Clearly we are being asked to put together those two pieces of
information to prove the statement.

\subsection*{What is a proof?}

A valid proof starts from the given information (the assumptions) and then
applies mathematical operations which we already know are true to reach the
conclusion. The important caveat here is that we can only use things
\emph{we already know are true}. In particular, you are not allowed to use the
result you are being asked to prove in the proof itself! This is known as
\emph{begging the question}\ and results in a circular argument which proves
nothing at all!

\subsection*{How \emph{not} to answer exercise 3.1}

By far the most common set of solutions handed in to this question went
approximately as follows:
\begin{align}
  F(h)&=E(h)/h\\
  \intertext{by substituting the definition of $E$:}
  F(h)&=\O(h^n)/h\\
  \intertext{therefore:}
  F(h)&=\O(h^{n-1})
\end{align}
But hang on, how did that last step occur? Who says that you can pass the
division by $h$ inside the brackets of the $\O$ symbol? It's not obviously
true at all. For example, if I write $\sin(h^n)/h$ that is very definitely
not $\sin(h^{n-1})$. 

In fact proving that $\O(h^n)/h=\O(h^{n-1})$ is exactly what this question
is asking you to do, so assuming that it is true and using this in the proof
is \emph{begging the question}.

\subsection*{How to answer exercise 3.1 correctly}

If we look at the previous answer, we notice that we haven't actually used
any information about the first of our pieces of information, that $E(h)$ is
$\O(h^n)$. This should seem strange because the whole lecture was on the
properties of $\O$. So let's try to use this information. Looking back a
page we find the following definition:
\begin{define}
  A function $f(x)$ is $\O(\alpha(x))$ as $x\rightarrow a$ if there exists
  some positive constant $c$ such that for any choice of $x$ sufficiently
  close to $a$:
  \begin{equation}
    |f(x)|<c\alpha(x)
  \end{equation}
\end{define}
We are also told just above exercise 3.1 that if we see $\O(h^n)$ then it's
safe to assume that we're talking about the case where $a=0$ (ie
$h\rightarrow0$). 

We know that $E(h)$ is $O(h^n)$ so we can substitute this into the above
definition by using $h$ instead of $x$ as the independant variable and by
setting $\alpha(h)=h^n$. This means we have:
\begin{gather}\label{eq:E}
  |E(h)| <ch^n
\end{gather}
for some fixed $c$ and for $h$ sufficiently close to $0$. That tells us
something about $E$ but we want to know about $F$. To achieve this, we could
try to write $F$ in terms of $E$. We know that:
\begin{gather}
  F(h)=E(h)/h
\end{gather}
so we can therefore write:
\begin{gather}
  E(h)=hF(h)
\end{gather}
and substitute this into inequality \eqref{eq:E}\ to give:
\begin{gather}
  |hF(h)| <ch^n
\end{gather}
This is good, we now have a statement about $F(h)$ but we want to show that $F$
is $O(h^{n-1})$ so we need to transform this statement into the form of
definition 3.1. We therefore need that factor of $h$ gone from left hand
side. Happily $h$ is positive (it's a distance) so we can take it outside
the absolute value signs:
\begin{gather}
  h|F(h)| <ch^n
\end{gather}
Dividing both sides by $h$ we have:
\begin{gather}
  |F(h)| <ch^{n-1}
\end{gather}
Looking back at definition 3.1, we can see that this statement fits the
definition with $\alpha(h)=h^{n-1}$. So, using this definition we can now
conclude that $F(h)$ is $O(h^{n-1})$. To be utterly pedantically complete, we
could write Q.E.D. to show that we are done.

\subsection*{A correct answer}

The above is very long-winded for such a short question. This is deliberate
as I wanted to carefully motivate and explain each action. In reality, you
just need to provide each step and enough description to demonstrate why
that step is true. If the step is a simple algebraic operation, little or no
explanation is needed. A completely correct answer to this question might
read as follows:

Given that $E$ is $\O(h^n)$, using definition 3.1 we have:
\begin{gather}\label{eq:E2}
  |E(h)| <ch^n
\end{gather}
for some fixed $c$ and for $h$ sufficiently close to $0$. 

Writing $E$ in terms of $F$, we have:
\begin{gather*}
  E(h)=hF(h)
\end{gather*}

Substituting this into inequality \eqref{eq:E2}, we have:
\begin{gather*}
  |hF(h)| <ch^n
\end{gather*}
which, since $h$ is positive, is equivalent to:
\begin{gather*}
  |F(h)| <ch^{n-1}
\end{gather*}
By definition 3.1, $F$ is therefore $\O(h^{n-1})$. Q.E.D.

\subsection*{Another correct answer}

For completeness and to demonstrate that there is often more than one
correct proof, I will also repeat here the equally correct proof which some
students gave:

Given that $E$ is $\O(h^n)$, using definition 3.1 we have:
\begin{gather*}
  |E(h)| <ch^n
\end{gather*}
for some fixed $c$ and for $h$ sufficiently close to $0$. 

Dividing this inequality by $h$ and noting that $h$ is positive, we have:
\begin{gather*}
  \left|\frac{E(h)}{h}\right| <ch^{n-1}
\end{gather*}
Substituting the definition of $F$ gives:
\begin{gather*}
  |F(h)| <ch^{n-1}
\end{gather*}
By definition 3.1, $F$ is therefore $\O(h^{n-1})$. Q.E.D.


\end{document}

