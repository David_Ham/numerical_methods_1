\documentclass[a4paper, 11pt]{article}


\usepackage[margin=2.5cm]{geometry}
%\usepackage{mathpazo}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{float}
\usepackage{colortbl}
\usepackage{amsthm}
\usepackage{units}
\usepackage{mathabx}
\usepackage{hyperref}
\changenotsign

\definecolor{DarkBlue}{rgb}{0.00,0.00,0.55}
\hypersetup{
    linkcolor   = DarkBlue,
    anchorcolor = DarkBlue,
    citecolor   = DarkBlue,
    filecolor   = DarkBlue,
    pagecolor   = DarkBlue,
    urlcolor    = DarkBlue,
    colorlinks  = true,
    pdftitle    = {Numerical Methods 1},
}


\definecolor{paleblue}{rgb}{0.9,0.95,1.0}
\definecolor{palegrey}{rgb}{0.9,0.9,0.9}

\newcommand{\real}[1]{\mathrm{Re}(#1)}
\newcommand{\imag}[1]{\mathrm{Im}(#1)}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\dx}{\,\d x}
\newcommand{\dy}{\,\d y}
\newcommand{\dt}{\,\d t}
\newcommand{\ddx}[2][x]{\frac{\d#2}{\d#1}}
\newcommand{\ddxx}[2][x]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ddt}[2][t]{\frac{\d#2}{\d#1}}
\newcommand{\ddtt}[2][t]{\frac{\d^2#2}{\d#1^2}}
\newcommand{\ppx}[2][x]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppy}[2][y]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppz}[2][z]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppt}[2][\theta]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppr}[2][r]{\frac{\partial#2}{\partial#1}}
\newcommand{\ppxx}[2][x]{\frac{\partial^2#2}{\partial#1^2}}
\newcommand{\mat}[1]{\mathrm{#1}}

\newcommand{\A}{\mat A}
\renewcommand{\L}{\mat L}
\newcommand{\U}{\mat U}

\newcommand{\Integer}{\mathbb{Z}}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\Rational}{\mathbb{Q}}
\newcommand{\Complex}{\mathbb{C}}
\newcommand{\Natural}{\mathbb{N}}

\renewcommand{\O}{\mathcal{O}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\x}{\vec{x}}

\newcommand{\eps}{\epsilon_{\mathrm{machine}}}

\newcommand{\intpi}{\int_{-\pi}^{\pi}}

\newcounter{chapter}

\theoremstyle{definition}
\newtheorem{exercise}{Exercise}[section]
\newtheorem*{unexercise}{Exercise}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{define}[theorem]{Definition}

\newcommand{\T}{{\color{green}T}}
\newcommand{\F}{{\color{red}F}}

\setlength{\parskip}{2ex}
\setlength{\parindent}{0ex}

\floatstyle{boxed}
\newfloat{biography}{tp}{lob}

\usepackage{listings}

\lstloadlanguages{Python}

\lstset{basicstyle=\ttfamily,framerule=0.5ex, backgroundcolor=\color{paleblue},gobble=2}

\lstnewenvironment{pythonprogram}{\lstset{language=Python,frame=l,rulecolor=\color{blue}}}{}
\lstnewenvironment{pythonsnippet}{\lstset{language=Python}}{}

\lstMakeShortInline[language=Python]{~}

\newcommand{\note}[1]{{\Large\color{red}#1}}

\newcommand{\answer}[1]{
  
  \textbf{Answer: }#1
  
}

\begin{document}
\title{Solutions to some selected exercises}
\maketitle

Here are worked solutions to a few of the more challenging questions. The
first two are the proof questions from the additional exercises for lectures
1-3 which were handed out at the start of the spring term. The third is an
example of a question similar to question 1 (c) on the sample exam.

It should be noted in particular with respect to the proof questions that
there is frequently more than one correct proof. What is important is that
the proof procedes from the information given to the result using only clear
logical steps involving results which are already known. For the sake of
clarity, the proofs here are a little longwinded and it would be perfectly
acceptable to be more concise in an answer. For an example of a complete but
shorter proof, look back at the feedback from question 3.1 on ESESIS.

\setcounter{section}{3}

\begin{exercise}
  If $E(h)$ is $\O(h^n)$ and $F(h)=2E(h)$, show that $F(h)$ is
  $\O(h^n)$. \emph{Hint:}\ look at the ESESIS feedback for course exercise
  3.1.
\end{exercise}
\answer{
Using definition 3.1 from the notes, we have:
\begin{equation*}
  |E(h)|<ch^n
\end{equation*}
for some constant $c$ and $h$ sufficiently close to $0$.

We can rewrite the definition of $F(h)$ above as:
\begin{equation*}
  E(h)=\frac{F(h)}{2}
\end{equation*}
and substitute:
\begin{gather*}
  \left|\frac{F(h)}{2}\right|<ch^n.\\
\intertext{Hence:}
  |F(h)|<2ch^n.\\
\end{gather*}
If we define $d=2c$ then we have:
\begin{equation*}
  |F(h)|<dh^n.
\end{equation*}
By substituting back into definition 3.1, we can conclude that F(h) is
$\O(h^n)$, Q.E.D.
}
\pagebreak

\begin{exercise}
  If $E(h)$ is $\O(h^n)$ and $F(h)$ is $\O(h^{n+1})$, show that $E(h) + F(h)$ is
  $\O(h^n)$. \emph{Hint:}\ use theorem 3.2 in the notes.
\end{exercise}
\answer{
  Using theorem 3.2 from the notes, since $F(h)$ is $\O(h^{n+1})$, $F(h)$ is
  also $\O(h^{n})$. Now, using definition 3.1, we have:
  \begin{gather*}
    |E(h)|<ch^n\\
    |F(h)|<dh^n
  \end{gather*}
  for some constants $c,d$ and for $h$ sufficiently close to $0$. By adding
  these inequalities, we have:
  \begin{equation*}
    |E(h)|+|F(h)|<ch^n+dh^n
  \end{equation*}
  Using the triangle inequality ($|a|+|b|\geq|a+b|$) and gathering like
  terms on the right hand side we have:
  \begin{equation*}
    |E(h)+F(h)|<(c+d)h^n
  \end{equation*}
  Defining $e=c+d$, we can transform this inequality into the form of
  definition 3.1:
  \begin{equation*}
    |E(h)+F(h)|<eh^n
  \end{equation*}  
  hence we can conclude that $E(h) + F(h)$ is
  $\O(h^n)$, Q.E.D.
}

\vspace{5ex}

\begin{unexercise}
  Write a mathematical expression using only matrices, vectors and scalars
  which performs the same operation as the following function. In each case,
  state whether each input and output is a matrix, vector or scalar.

  \begin{lstlisting}[language=Python]        
  def function(a):
    from numpy import zeros

    b=zeros((a.shape[1],a.shape[0]))
    
    for i in range(a.shape[0]):
        b[:,i]=a[i,:]
        
    return b
  \end{lstlisting}
\end{unexercise}

\answer{
First, lets work out the possible ranks of the input ~a~, and output
~b~. Since there are references to ~a.shape[1]~ and ~a[i,:]~ has two
indices, we know ~a~ can't be a scalar or vector but must be a
matrix. Similarly, since ~b~ is defined by the ~zeros~ function called with
a shape argument with two entries ~(a.shape[1],a.shape[0])~, ~b~ must also
be a matrix. We could also have worked this out by noticing that ~b~ is
indexed with two indices in ~b[:,i]~.

Now lets look at the ``business end'' of the function to work out what it
actually does. We see that ~i~ loops over the indices of the \emph{rows}\ of
~a~. Now for each ~i~, we execute ~b[:,i]=a[i,:]~. ~b[:,i]~ is the ~i~th
\emph{column}\ of ~b~ while ~a[i,:]~ is the ~i~th
\emph{row}\ of ~a~. So, the rows of ~a~ are the columns of ~b~, in other
words ~a~ is the \emph{transpose}\ of ~b~. This enables us to actually write
the answer:

The function implements:
\begin{equation*}
  \mat{a}=\mat{b}^{\mat T}
\end{equation*}
where $\mat a$ and $\mat b$ are both matrices.
}


\end{document}