\documentclass[a4paper,11pt]{article}

\usepackage[margin=2.5cm]{geometry}

\title{3.09 Numerical methods 1}
\renewcommand{\today}{}

\setcounter{secnumdepth}{0}

\begin{document}

\maketitle

\begin{tabular}{ll}
  \textbf{Lecturer:} & Dr David Ham\\
  \textbf{Email:} & David.Ham@imperial.ac.uk \\
  \textbf{Office:} & 3.55\\
  \textbf{Phone:} & 020 7594 6439 (x46439)\\
  \textbf{Prerequisite:} & Mathematical methods 1\\
  \textbf{Corequisite:} & Python for geoscientists\\
\end{tabular}

\section{Aims}

The course will cover the basic techniques which underlie the computational mathematical techniques in common use in the Earth sciences. It will provide an overview of the capabilities and limitations of scientific computation including the concepts of error and conditioning. The course will cover techniques applicable to both observational data and simulation and will have an emphasis on the practical implementation of the methods studied in the Python programming language.


\section{Objectives}
By the end of this course, you should be able to:
\begin{itemize}
\item write Python code for a number of important and useful mathematical algorithms
\item understand the sources of error in algorithms
\item be able to apply numerical techniques to solve problems which occur in
  geoscience such as curve fitting, solving systems of equations and root finding
\end{itemize}

\section{Lecture plan}

\begin{description}
\item[Lecture 1] Computer arithmetic
\item[Lecture 2] Taylor's theorem and numerical differentiation
\item[Lecture 3] Numerical integration
\item[Lecture 4] Root finding
\item[Lecture 5] Solving linear systems
\item[Lecture 6] Pivotting and orthogonalisation
\item[Lecture 7] Curve fitting and least squares solutions
\item[Lecture 8] Numerical methods for ODEs
\end{description}

\section{Course text}

Langtangen, Hans Petter \emph{A primer on scientific computing with Python},
Springer, 2009.

Possession of this textbook is absolutely required as large parts of the
course will be based on it. This is also the text for 3.08 Python for
geoscientists.

\section{Assessment}

There will be an in-class time limited practical programming exercise in
week 8. This will require you to write programs of the sort included in the
exercises during the module. This exercise will constitute the only assessed
coursework in this module.

There will be two questions on the theoretical aspects of the module on the
integrated exam in January, of which you must answer one. The theoretical
questions will cover the mathematics in the module and will be similar to
the more mathematical exercises.


\section{Exercises}

The lecture notes contain exercises and references to exercises in the
course text and the last hour of each lecture will be devoted to working on
exercises. The lecturer and demonstrators will be available to provide help
and immediate feedback. Full answers to exercises should be submitted as
coursework by the following deadlines:

\begin{tabular}{ll}
  Lectures 1-2 & 25 October, 0900\\
  Lectures 3-4 & 15 November, 0900\\
  Lectures 5-7 & 6 December, 0900
\end{tabular}

It is likely that you will need to devote additional time to completing the
exercises outside of the lectures.


\subsection{What to hand in}

You are expected to hand in a solution to every non-starred exercise in the lecture
notes. The starred exercises are extension work. If you manage to do the
extensions then please do hand them in and you will receive feedback on
them.

You should upload your program files to ESESIS either singly or in a
.tar.gz file. 
You should also hand in a paper submission containing the following:
\begin{itemize}
\item Worked solutions to maths problems.
\item Computer I/O including any plots for practical exercises.
\item A print-out using a2ps or similar of the source code you hand
  in.
\end{itemize}


\subsection{Feedback on exercises}

Exercises are due to be submitted on Monday mornings and will usually be
returned with feedback in the form of annotations and corrections in the
lecture a week later.  Exercises are not assessable and no numerical marks
will be issued.

\pagebreak

\section{Components of a correct program}

\subsection{Interfaces}

Software is comprised of components which have to work together. This is
achieved by strictly sticking to specified interfaces. Coursework questions
will specify module and function names and interfaces. They may also specify
particular exceptions which are to be raised in the event of erroneous input
or the failure of an algorithm.

\subsection{Comments and style}

A program is not just a series of instructions for a computer, it's also a
medium for communicating your ideas to the next programmer. You are expected
to provide a docstring for every function describing what that function
does. In addition, your code should be commented to a standard that it
would be immediately obvious to another student what each line of the
program does. Be careful, though, over-commenting can obscure the meaning
of a program just as much as too few comments.

A consistent and clear programming style is also essential to
readability. Ask yourself do the variable names make sense? Is the indentation
consistent? Does the way in which the program is split into modules and
functions aid or obscure understanding?

\subsection{Functionality}

Ensure that you carefully read the question and that your program does
exactly what is required of it. Use test data to ensure the right answer
and, where available, test your program against the ``official'' version
in Numpy or Scitools.


\section{Plagiarism}

You are encouraged to read widely online and to help each other in solving
coding problems, but directly copying code from other sources without
attribution is plagiarism just like copying text. It's also surprisingly
easy to tell when code has been copied: an experienced programmer will see
differences of coding style just as easily as differences of writing style in
text.

We may also ask you to explain what a particular line of your code does!

Remember, the coursework is not assessable so you gain precisely no marks by
cheating. The in-class programming exercise, however, \emph{is}\ assessed so
\emph{you}\ need to be able to write programs by yourself to pass this
module.


\end{document}
