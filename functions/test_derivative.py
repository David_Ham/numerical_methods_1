import sys
import numpy

try:
    import differentiation
except:
    print "Failed to import differentiation module"

if not hasattr(differentiation, "forward_difference"):
    print "Differentiation module has no forward_difference function."
    sys.exit(1)

if not hasattr(differentiation, "central_difference"):
    print "Differentiation module has no cental_difference function."
    sys.exit(1)


f = lambda x:numpy.exp(x)

def convergence(h,e):
    return numpy.log(numpy.abs(e[1:]/e[:-1]))/numpy.log(h[1:]/h[:-1])

h=numpy.array([0.01,0.005])
e=numpy.array([differentiation.forward_difference(f,0,h_i)-1.0 for h_i in h])
if abs(convergence(h,e)-1)>0.01:
    print "Forward difference method fails to converge at the expected rate."
    sys.exit(1)
else:
    print "Forward difference method converges at the expected rate."

e=numpy.array([differentiation.central_difference(f,0,h_i)-1.0 for h_i in h])
if abs(convergence(h,e)-2)>0.01:
    print "Central difference method fails to converge at the expected rate."
    sys.exit(1)
else:
    print "Central difference method converges at the expected rate."
