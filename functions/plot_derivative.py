from matplotlib import rc
import pylab
from mpl_toolkits.axes_grid.axislines import Subplot
import numpy

def plot_forward(f, df):
    
    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    l=0.1
    r=1.8
    h=0.6
    x_0=1.

    # High resolution background line.
    x = numpy.linspace(l,r,201)
    y = f(x)
    pylab.plot(x,y,'k')

    # Approximate derivative
    x=numpy.array([x_0,x_0+h])
    pylab.plot(x, f(x),'r')

    # Exact derivative.
    dx=numpy.array([-.5*h,.5*h])
    pylab.plot(x_0+dx, f(x_0)+dx*df(x_0),'b')

    # Vertical lines.
    pylab.plot([x_0,x_0],[0,f(x_0)],'g')
    right=pylab.plot([x_0+h,x_0+h],[0,f(x_0+h)],'g')

    pylab.text(x_0,-0.6,"$x_0$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(x_0+h,-0.6,"$x_0+h$",horizontalalignment='center',
               verticalalignment='center')


    # X axis
    dx=r-l
    pylab.plot([l-.1*dx,r+.1*dx],[0.,0.],'k')

    
    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    pylab.margins(0.01)

def plot_central(f, df):
    
    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    l=0.1
    r=1.8
    h=0.6
    x_0=1.

    # High resolution background line.
    x = numpy.linspace(l,r,201)
    y = f(x)
    pylab.plot(x,y,'k')

    # Approximate derivative
    x=numpy.array([x_0-h,x_0+h])
    pylab.plot(x, f(x),'r')

    # Exact derivative.
    dx=numpy.array([-.5*h,.5*h])
    pylab.plot(x_0+dx, f(x_0)+dx*df(x_0),'b')

    # Vertical lines.
    pylab.plot([x_0,x_0],[0,f(x_0)],'g')
    left=pylab.plot([x_0-h,x_0-h],[0,f(x_0-h)],'g')
    right=pylab.plot([x_0+h,x_0+h],[0,f(x_0+h)],'g')

    pylab.text(x_0,-0.6,"$x_0$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(x_0+h,-0.6,"$x_0+h$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(x_0-h,-0.6,"$x_0-h$",horizontalalignment='center',
               verticalalignment='center')


    # X axis
    dx=r-l
    pylab.plot([l-.1*dx,r+.1*dx],[0.,0.],'k')

    
    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    pylab.margins(0.01)



# This hack disables pylab.margins without error if we are on an old machine
# with no margins support.
def margins(x):
    return

if not hasattr(pylab,"margins"):
    pylab.margins=margins


def generate_plots():
    rc('font',**{'family':'serif',
                 'serif':['Palatino'],
                 'size':10})
    rc('text', usetex=True)

    f=lambda x: x**4+1
    df=lambda x: 4*x**3

    pylab.figure(figsize=(6,4))
    plot_forward(f,df)
    pylab.savefig("forward_diff.pdf")

    pylab.figure(figsize=(6,4))
    plot_central(f,df)
    pylab.savefig("central_diff.pdf")



if __name__=="__main__":
    generate_plots()
