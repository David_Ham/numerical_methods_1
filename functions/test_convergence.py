from numpy import array
import numpy
h = array([ 1.        ,  0.5       ,  0.25      ,  0.125     ,  0.0625    ,
        0.03125   ,  0.015625  ,  0.0078125 ,  0.00390625,  0.00195312])
S1 = array([-0.28729851, -0.11166472, -0.0466684 , -0.02093189, -0.00985384,
       -0.00477261, -0.00234758, -0.00116409, -0.00057962, -0.0002892 ])
S2 = array([ -1.51448553e-01,  -3.93110674e-02,  -9.92036981e-03,
        -2.48591253e-03,  -6.21842394e-04,  -1.55483373e-04,
        -3.88722667e-05,  -9.71815566e-06,  -2.42954447e-06,
        -6.07386475e-07])

c1 = array([ 1.36337707,  1.25865553,  1.15674342,  1.08694463,  1.0459076 ,
        1.02360413,  1.01197172,  1.00602332,  1.00303612])

c2 = array([ 1.94582035,  1.98646973,  1.99661837,  1.99915465,  1.99978867,
        1.99994717,  1.99998679,  1.9999967 ,  1.99999176])

def test_convergence(convergence):
    """Check that the convergence function supplied produces the right
    result"""

    n=array(convergence(h,S1))

    if numpy.max(abs(c1-n))<1.e-5:
        print "convergence function returns correct result for first order convergence test."
    else:
        print "convergence function returns incorrect result for first order convergence test:"
        print "function result: ",str(n)
        print "correct answer:  ",str(c1)
        print "difference:      ",str(abs(n-c1))
        print ""

    n=array(convergence(h,S2))

    if numpy.max(abs(c2-n))<1.e-5:
        print "convergence function returns correct result for second order convergence test."
    else:
        print "convergence function returns incorrect result for second order convergence test:"
        print "function result: ",str(n)
        print "correct answer:  ",str(c2)
        print "difference:      ",str(abs(n-c2))
        print ""

if __name__=="__main__":
    import convergence
    test_convergence(convergence.convergence)
