import sys
import numpy
from newton_cotes import newton_cotes

try:
    import quadrature
except:
    print "Failed to import quadrature module"
    sys.exit(1)

if not hasattr(quadrature, "quad"):
    print "Quadrature module does not contain quad function"
    sys.exit(1)


test=lambda rule, n : quadrature.quad(numpy.sin, 0, numpy.pi, n, rule)

def convergence(h,e):
    return numpy.log(numpy.abs(e[1:]/e[:-1]))/numpy.log(h[1:]/h[:-1])

n=numpy.array([8,16])

ans=2


h=numpy.pi/n
e=numpy.array([test("trapezoidal",i) for i in n])-ans
if abs(convergence(h,e)-2)>0.01:
    print "Trapezoidal rule fails to converge fast enough."
    sys.exit(1)
else:
    print "Trapezoidal rule converges at expected rate."

e=numpy.array([test("Simpson",i) for i in n])-ans
if abs(convergence(h,e)-4)>0.01:
    print "Simpson's rule fails to converge fast enough."
    sys.exit(1)
else:
    print "Simpson's rule converges at expected rate."

e=numpy.array([test("Boole",i) for i in n])-ans
if abs(convergence(h,e)-6)>0.01:
    print "Boole's rule fails to converge fast enough."
    sys.exit(1)
else:
    print "Boole's rule converges at expected rate."

try:
    test("foo",2)
    print "Failed to correctly handle invalid quadrature rule."
    sys.exit(1)
except ValueError:
    print "Invalid quadrature rule handled correctly."


print "All tests pass successfully"
