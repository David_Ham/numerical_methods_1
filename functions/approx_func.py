from matplotlib import rc
import pylab
from mpl_toolkits.axes_grid.axislines import Subplot
import numpy
import scipy.integrate as si

def piecewise_constant(f, a, b, n):

    h=float(b-a)/n

    xc=[]
    yc=[]

    for i in range(n):
        xc.append(a+h*i)
        yc.append(f(xc[-1]))
        xc.append(a+h*(i+1))
        yc.append(yc[-1])


    xf=list(numpy.arange(b,a,-h/20))
    yf=list(f(numpy.arange(b,a,-h/20)))

    xf.append(a)
    yf.append(f(a))

    pylab.fill(xc+xf,yc+yf,color=(0.9,0.95,1.0))

    dx=b-a    
    pylab.plot((a-.1*dx,b+.1*dx),(0,0),'k')

    pylab.plot(xf,yf,'k')
    pylab.plot(xc,yc,'b')
    for i in range(n+1):
        x=(h*i,h*i)
        y=(0.0,max(f(x[0]),f(h*(i-1))))
        pylab.plot(x,y,'g')

    for i in range(n):
        pylab.plot(h*i,f(h*i),'bo')

    pylab.margins(0.1)

def constant_error(f, a, b, n):
    '''Return the integral of the error when f is approximated
    over the interval [a,b] by n equal width piecewise constant 
    functions.'''
    import scipy.integrate as si
   
    error=0
    h=float(b-a)/n

    for i in range(n):
        l=a+i*h
        r=(i+1)*h

        def f_err(x):
          return abs(f(x)-f(l))
          
        # si.quad is the numerical integration routine.
        error+=si.quad(f_err,l,r)[0]
    
    return error

def linear_error(f, a, b, n):
    '''Return the integral of the error when f is approximated over the
    interval [a,b] by n equal piecewise linear functions.'''
    
    error=0
    h=float(b-a)/n

    for i in range(n):
        l=a+i*h
        r=a+(i+1)*h
        dy=f(r)-f(l)

        def f_err(x):
            x_bar=(x-l)
            return abs(f(x)-(f(l)+x_bar*dy/h))

        error+=si.quad(f_err,l,r)[0]
    
    return error

def plot_error(f, a, b):
    
    h=[]
    e_0=[]
    e_1=[]

    for n in range(1,32):
        h.append((b-a)/n)
        e_0.append(constant_error(f, a, b, n))
        e_1.append(linear_error(f, a, b, n))

    fig=pylab.gcf()

    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)

    pylab.plot(h,e_0,'bx-')
    pylab.plot(h,e_1,'gx-')

    ax=pylab.gca()
    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)

    pylab.xlabel("$h$")
    pylab.ylabel("error")
    pylab.legend(("piecewise constant","piecewise linear"),loc="best")
        

def piecewise_linear(f, a, b, n):

    h=float(b-a)/n

    xl=[]
    yl=[]

    for i in range(n+1):
        xl.append(a+h*i)
        yl.append(f(xl[-1]))


    xf=list(numpy.arange(b,a,-h/20))
    yf=list(f(numpy.arange(b,a,-h/20)))

    xf.append(a)
    yf.append(f(a))

    pylab.fill(xl+xf,yl+yf,color=(0.9,0.95,1.0))

    dx=b-a    
    pylab.plot((a-.1*dx,b+.1*dx),(0,0),'k')

    pylab.plot(xf,yf,'k')
    pylab.plot(xl,yl,'b')
    for i in range(n+1):
        x=(a+h*i,a+h*i)
        y=(0.0,f(x[0]))
        pylab.plot(x,y,'g')

        pylab.plot(x[0],f(x[0]),'bo')

    pylab.margins(0.1)
    

def generate_plots():
    rc('font',**{'family':'serif',
                 'serif':['Palatino'],
                 'size':10})
    rc('text', usetex=True)

    f=lambda x: numpy.sin(x)
    
    pylab.figure(figsize=(6,4.5))
    plot_error(f,0,numpy.pi)
    pylab.savefig("approx_convergence.pdf")

    pylab.figure(figsize=(4,3))
    piecewise_constant(f,0,numpy.pi, 4)
    pylab.savefig("piecewise_constant_4.pdf")

    rc('figure.subplot',**{'left':0.05,
                           'right':.95,
                           'bottom':.02,
                           'top':0.99,
                           'wspace':.25,
                           'hspace':.25})
    pylab.figure(figsize=(6.5,9.5))

    for j in range(4):
        pylab.subplot(4,2,2*j+1)
        piecewise_constant(f,0,numpy.pi, 2*(j+2))

        pylab.subplot(4,2,2*j+2)
        piecewise_linear(f,0,numpy.pi, 2*(j+2))        
    pylab.savefig("approximations.pdf")

# This hack disables pylab.margins without error if we are on an old machine
# with no margins support.
def margins(x):
    return

if not hasattr(pylab,"margins"):
    pylab.margins=margins


if __name__=="__main__":
    generate_plots()
