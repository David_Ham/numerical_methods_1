import numpy

class Quadrature(object):
    '''Object encapsulating a quadrature rule. This has two members:
    x - the positions of the quadrature points in the interval [0,1]
    w - the corresponding quadrature wieghts.'''
    def __init__(self,x,w):
        self.x=x
        self.w=w

newton_cotes={}

newton_cotes["trapezoidal"]=Quadrature(
    x=numpy.array([0.,1.]),
    w=numpy.array([0.5,0.5]))
newton_cotes["Simpson"]=Quadrature(
    x=numpy.array([0.,0.5,1.]),
    w=numpy.array([1./6.,2./3.,1./6.]))
newton_cotes["Boole"]=Quadrature(
    x=numpy.array([0.,0.25,0.5,0.75,1.]),
    w=numpy.array([7./90.,16./45.,6./45.,16./45.,7./90.]))

