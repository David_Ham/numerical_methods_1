from newton_cotes import newton_cotes

def quad(f, a, b, n, rule):
    
    try:
        q=newton_cotes[rule]
    except KeyError:
        raise ValueError("Unknown quadrature rule "+str(rule))
    
    h=float(b-a)/n
    
    integral=0.0
    for i in range(n):
        for x,w in zip(q.x,q.w):
            integral+=h*w*f(a+i*h+x*h)

    return integral

    
    
