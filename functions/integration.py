from matplotlib import rc
import pylab
from mpl_toolkits.axes_grid.axislines import Subplot
import numpy
import scipy.integrate as si
from matplotlib.widgets import Slider

def trapezoidal_error(f, l, h):

    # High resolution background line.
    x_f = numpy.linspace(l-.5*h,l+1.5*h,201)
    y_f = f(x_f)

    # High resolution in [l,l+h].
    x_t = numpy.linspace(l,l+h,201)
    y_t = f(x_t)

    # Linear approximation.
    m=(f(x_t[-1])-f(x_t[0]))/h
    y_m = f(x_t[0])+m*(x_t-x_t[0])

    # Maximum error.
    e_i=numpy.argmax(numpy.abs(y_m-y_t))
    x_e=x_t[e_i]
    y_e=y_t[e_i]-y_m[e_i]

    ebox_x=numpy.array([x_t[0],x_t[-1],x_t[-1],x_t[0]])
    ebox_y=f(ebox_x)
    ebox_y[2:]+=y_e

    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    # Pink box
    pink=pylab.fill(ebox_x,ebox_y,color=(1.0,0.9,0.9))
    # Blue box
    blue=pylab.fill(x_t, y_t, color=(0.9,0.95,1.0))
    # Green box
    pylab.fill_between(x_t,y_m, color=(0.9,1.0,0.9))               
    
    # X axis
    pylab.plot([l-.5*h,l+1.5*h],[0.,0.],'k')

    # Vertical lines.
    pylab.plot([x_t[0],x_t[0]],[0,y_t[0]],'g')
    pylab.plot([x_t[-1],x_t[-1]],[0,y_t[-1]],'g')

    pylab.plot(x_f,y_f,'k')
    pylab.plot(x_t,y_m,'b')
    pylab.plot([x_e, x_e],[y_t[e_i],y_m[e_i]],'r')

    pylab.text(x_t[0],-0.06,"$l$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(x_t[-1],-0.06,"$l+h$",horizontalalignment='center',
               verticalalignment='center')

    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    #pylab.margins(0.1)

def trapezoidal_animate(f, l, h):

    # High resolution background line.
    x_f = numpy.linspace(l-.5*h,l+1.5*h,201)
    y_f = f(x_f)

    def approx(h):
        # High resolution in [l,l+h].
        x_t = numpy.linspace(l,l+h,201)
        y_t = f(x_t)

        # Linear approximation.
        m=(f(x_t[-1])-f(x_t[0]))/h
        y_m = f(x_t[0])+m*(x_t-x_t[0])

        # Maximum error.
        e_i=numpy.argmax(numpy.abs(y_m-y_t))
        x_e=x_t[e_i]
        y_e=y_t[e_i]-y_m[e_i]

        return x_t, y_t, m, y_m, e_i, x_e, y_e

    x_t, y_t, m, y_m, e_i, x_e, y_e = approx(h)

    ebox_x=numpy.array([x_t[0],x_t[-1],x_t[-1],x_t[0]])
    ebox_y=f(ebox_x)
    ebox_y[2:]+=y_e

    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    patch=[]

    # Pink box
    patch.append(pylab.fill(ebox_x,ebox_y,color=(1.0,0.9,0.9))[0])
    # Blue box
    patch.append(pylab.fill(x_t, y_t, color=(0.9,0.95,1.0))[0])
    # Green box
    patch.append(pylab.fill_between(x_t,y_m, color=(0.9,1.0,0.9)))
    
    # X axis
    pylab.plot([l-.5*h,l+1.5*h],[0.,0.],'k')

    # Vertical lines.
    pylab.plot([x_t[0],x_t[0]],[0,y_t[0]],'g')
    right=pylab.plot([x_t[-1],x_t[-1]],[0,y_t[-1]],'g')

    pylab.plot(x_f,y_f,'k')
    blue=pylab.plot(x_t,y_m,'b')
    red=pylab.plot([x_e, x_e],[y_t[e_i],y_m[e_i]],'r')

    pylab.text(x_t[0],-0.06,"$l$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(x_t[-1],-0.06,"$l+h$",horizontalalignment='center',
               verticalalignment='center')

    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    axcolor = 'lightgoldenrodyellow'
    axslid = pylab.axes([0.2, 0.03, 0.6, 0.02], axisbg=axcolor)

    hslider = Slider(axslid, '$h$', 0., 1.1*h, valinit=h)

    def update(val):
        x_t, y_t, m, y_m, e_i, x_e, y_e = approx(val)

        ebox_x=numpy.array([x_t[0],x_t[-1],x_t[-1],x_t[0]])
        ebox_y=f(ebox_x)
        ebox_y[2:]+=y_e

        right[0].set_xdata([x_t[-1],x_t[-1]])
        right[0].set_ydata([0,y_t[-1]])
        
        red[0].set_xdata([x_e, x_e])
        red[0].set_ydata([y_t[e_i],y_m[e_i]])

        blue[0].set_xdata(x_t)
        blue[0].set_ydata(y_m)

        for i in range(len(patch)):
            p=patch.pop()
            p.remove()
        
        pylab.draw()
        pylab.axes(ax)

        
        #Pink box
        patch.append(pylab.fill(ebox_x,ebox_y,color=(1.0,0.9,0.9))[0])
        # Blue box
        patch.append(pylab.fill(x_t, y_t, color=(0.9,0.95,1.0))[0])
        # Green box
        patch.append(pylab.fill_between(x_t,y_m, color=(0.9,1.0,0.9)))

        pylab.draw()

    hslider.on_changed(update)

    #pylab.margins(0.1)

    pylab.show()
    return patch[0]

def simpson(f, l, h, x):
    # Evaluate a quadratic approximation to f over the interval [l,l+h]
    # at the point x.

    x_0=(x-l)/h
    x_1=1.-x_0

    return f(l)*x_1*(2*x_1-1)+f(l+.5*h)*4*x_0*x_1+f(l+h)*x_0*(2*x_0-1)

def simpson_error(f, l, h):

    # High resolution background line.
    x_f = numpy.linspace(l-.5*h,l+1.5*h,201)
    y_f = f(x_f)

    # High resolution in [l,l+h].
    x_t = numpy.linspace(l,l+h,20)
    y_t = f(x_t)

    # Quadratic approximation.
    y_m = simpson(f, l, h, x_t)

    # Maximum error.
    #e_i=numpy.argmax(numpy.abs(y_m-y_t))
    #x_e=x_t[e_i]
    #y_e=y_t[e_i]-y_m[e_i]

    #ebox_x=numpy.array([x_t[0],x_t[-1],x_t[-1],x_t[0]])
    #ebox_y=f(ebox_x)
    #ebox_y[2:]+=y_e

    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    # Pink box
    #pylab.fill(ebox_x,ebox_y,color=(1.0,0.9,0.9))
    # Green box
    pylab.fill_between(x_t,y_m, color=(0.9,1.0,0.9))               
    # Blue box
    #pylab.fill_between(x_t, y_t, y_m, color=(0.9,0.95,1.0))
    
    # X axis
    pylab.plot([l-.5*h,l+1.5*h],[0.,0.],'k')

    # Vertical lines.
    pylab.plot([x_t[0],x_t[0]],[0,y_t[0]],'g')
    pylab.plot([x_t[0]+.5*h,x_t[0]+.5*h],[0,f(l+.5*h)],'g')
    pylab.plot([x_t[-1],x_t[-1]],[0,y_t[-1]],'g')

    pylab.plot(x_f,y_f,'k')
    pylab.plot(x_t,y_m,'b')
    #pylab.plot([x_e, x_e],[y_t[e_i],y_m[e_i]],'r')

    pylab.text(x_t[0],-0.06,"$l$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(l+.5*h,-0.06,"$l+\\frac{h}{2}$",horizontalalignment='center',
               verticalalignment='center')
    pylab.text(x_t[-1],-0.06,"$l+h$",horizontalalignment='center',
               verticalalignment='center')

    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    pylab.margins(0.1)


def generate_plots():
    rc('font',**{'family':'serif',
                 'serif':['Palatino'],
                 'size':10})
    rc('text', usetex=True)

    f=lambda x: .7+0.5*numpy.sin(x)

    pylab.figure(figsize=(6,4))
    trapezoidal_error(f,1,numpy.pi)
    pylab.savefig("trapezoidal_error.pdf")

    pylab.figure(figsize=(6,4))
    simpson_error(f,1,numpy.pi)
    pylab.savefig("simpson_error.pdf")


if __name__=="__main__":
    generate_plots()
