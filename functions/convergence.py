import numpy 

def convergence(h,s):

    n=[]

    for i in range(len(h)-1):
        n.append(numpy.log(s[i]/s[i+1])/numpy.log(h[i]/h[i+1]))

    return n
