from matplotlib import rc
import pylab
from mpl_toolkits.axes_grid.axislines import Subplot
import numpy
import scipy.integrate as si
from matplotlib.widgets import Slider

from integration import *

if __name__=="__main__":
    rc('font',**{'family':'serif',
                 'serif':['Palatino'],
                 'size':10})
    rc('text', usetex=True)

    f=lambda x: .7+0.5*numpy.sin(x)

    trapezoidal_animate(f,1,numpy.pi)
