from matplotlib import rc
import pylab
from mpl_toolkits.axes_grid.axislines import Subplot
import numpy
import scipy.integrate as si
from matplotlib.widgets import Slider

def plot_forward(f, df):
    
    fig=pylab.gcf()
    ax = Subplot(fig, 111)
    fig.add_subplot(ax)

    l=0.1
    r=1.8
    h=0.6
    x_0=1.

    # High resolution background line.
    x = numpy.linspace(l,r,201)
    y = f(x)
    pylab.plot(x,y,'k')

    # Approximate derivative
    x=numpy.array([x_0,x_0+h])
    diff=pylab.plot(x, f(x),'r')

    # Exact derivative.
    dx=numpy.array([-.5*h,.5*h])
    pylab.plot(x_0+dx, f(x_0)+dx*df(x_0),'b')

    # Vertical lines.
    pylab.plot([x_0,x_0],[0,f(x_0)],'g')
    right=pylab.plot([x_0+h,x_0+h],[0,f(x_0+h)],'g')

    pylab.text(x_0,-0.6,"$x_0$",horizontalalignment='center',
               verticalalignment='center')
    dtext=pylab.text(x_0+h,-0.6,"$x_0+h$",horizontalalignment='center',
               verticalalignment='center')


    # X axis
    dx=r-l
    pylab.plot([l-.1*dx,r+.1*dx],[0.,0.],'k')

    
    ax.axis["right"].set_visible(False)
    ax.axis["top"].set_visible(False)
    ax.axis["left"].set_visible(False)
    ax.axis["bottom"].set_visible(False)

    pylab.margins(0.01)

    axcolor = 'lightgoldenrodyellow'
    axslid = pylab.axes([0.2, 0.03, 0.6, 0.02], axisbg=axcolor)

    hslider = Slider(axslid, '$h$', 0.001, 1.1*h, valinit=h)
    h_0=0.6

    def update(val):
        h=val

        right[0].set_xdata([x_0+h,x_0+h])
        right[0].set_ydata([0,f(x_0+h)])


        h_l=max(h,h_0)
        diff[0].set_xdata([x_0,x_0+h_l])

        dfdx=(f(x_0+h)-f(x_0))/h
        diff[0].set_ydata([f(x_0),f(x_0)+h_l*dfdx])        
        dtext.set_x(x_0+h)

        pylab.draw()

    hslider.on_changed(update)
    pylab.show()



if __name__=="__main__":
    rc('font',**{'family':'serif',
                 'serif':['Palatino'],
                 'size':10})
    rc('text', usetex=True)

    f=lambda x: x**4+1
    df=lambda x: 4*x**3

    plot_forward(f, df)
