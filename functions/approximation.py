# Import the plotting system.
import pylab
import numpy
from matplotlib import rc


def plot_constant(f, a, b, n):
    '''plot_constant(f, a, b, n)

    Plot a piecewise constant approximation to f over the interval [a,b]
    using n equal width intervals.'''
    # Import the plotting system.
    import pylab

    # Calculate the width of one interval.
    h=(b-a)/n

    # Create a new figure.
    fig=pylab.figure()

    # Plot each interval.
    for i in range(n):
        x=(a+h*i, a+h*(i+1))
        y=(f(x[0]), f(x[0]))
        pylab.plot(x,y,'b')

    # Expand the margins by 10% in each direction so we can see the edge
    # lines.
    pylab.margins(0.1)

    
def plot_constant_error(f, a, b, n):
    '''plot_constant_error(f, a, b, n)

    Plot a piecewise constant approximation to f over the interval [a,b]
    using n equal width intervals. Also show the error in the approximation
    and the points at which the function is evaluated.'''
    # Create a new figure.
    pylab.figure()
    # Calculate the width of one interval.
    h=(b-a)/n
    
    xc=[]
    yc=[]
    # Calculate the x and y values for each interval.
    for i in range(n):
        xc.append(a+h*i)
        yc.append(f(xc[-1]))
        xc.append(a+h*(i+1))
        yc.append(yc[-1])

    # Calculate a high-resolution piecewise linear approximation to the function.
    # Note these sequences go from right to left to make the are plot work.
    xf=list(numpy.arange(b,a,-h/20))
    yf=list(f(numpy.arange(b,a,-h/20)))

    xf.append(a)
    yf.append(f(a))

    # Plot the error area.
    pylab.fill(xc+xf,yc+yf,color=(0.9,0.95,1.0))

    # Plot the x axis.
    dx=b-a    
    pylab.plot((a-.1*dx,b+.1*dx),(0,0),'k')

    # Plot the high resolution "true" function.
    pylab.plot(xf,yf,'k')
    # Plot the piecewise constant approximation.
    pylab.plot(xc,yc,'b')
    # Plot the vertical green lines.
    for i in range(n+1):
        x=(a+h*i,a+h*i)
        y=(0.0,max(f(x[0]),f(a+h*(max(i-1,0)))))
        pylab.plot(x,y,'g')

    # Plot the blue dots.
    for i in range(n):
        pylab.plot(a+h*i,f(a+h*i),'bo')

    # Expand the margins by 10% in each direction so we can see the edge
    # lines.
    pylab.margins(0.1)

def piecewise_constant(f, x, a, b, n):
    '''Evaluate the piecewise constant approximation to f(x) using n equal
    width subintervals on the interval [a,b]. This relies on the student 
    supplying an interval module containing a correct interval function.'''
    from interval import interval

    i,h=interval(x,a,b,n)

    return f(h*i)

def constant_error(f, a, b, n):
    '''Return the integral of the error when f is approximated
    over the interval [a,b] by n equal width piecewise constant 
    functions.'''
    import scipy.integrate as si
   
    error=0
    h=float(b-a)/n

    for i in range(n):
        l=a+i*h
        r=(i+1)*h

        def f_err(x):
          return abs(f(x)-f(l))
          
        # si.quad is the numerical integration routine.
        error+=si.quad(f_err,l,r)[0]
    
    return error

def linear_error(f, a, b, n):
    '''Return the integral of the error when f is approximated over the
    interval [a,b] by n equal width piecewise linear functions.'''
    import scipy.integrate as si
    
    error=0
    h=float(b-a)/n

    for i in range(n):
        l=a+i*h
        r=a+(i+1)*h
        dy=f(r)-f(l)

        def f_err(x):
            x_bar=(x-l)
            return abs(f(x)-(f(l)+x_bar*dy/h))

        error+=si.quad(f_err,l,r)[0]
    
    return error

def plot_error(f, a, b):
    """Plot the error in the piecewise constant and piecewise linear
    approximations of f on the interval [a,b] as a function of the
    subinterval width, h."""
    from mpl_toolkits.axes_grid.axislines import Subplot

    h=[]
    e_0=[]
    e_1=[]

    # Calculate the error for various values of n.
    for n in range(1,32):
        h.append((b-a)/n)
        e_0.append(constant_error(f, a, b, n))
        e_1.append(linear_error(f, a, b, n))

    # Create a new figure
    fig=pylab.figure()

    # Plot the errors. Blue for constant, green for linear.
    pylab.plot(h,e_0,'bx-')
    pylab.plot(h,e_1,'gx-')

    pylab.xlabel("h")
    pylab.ylabel("error")
    pylab.legend(("piecewise constant","piecewise linear"),loc="best")


# This hack disables pylab.margins without error if we are on an old machine
# with no margins support.
def margins(x):
    return

if not hasattr(pylab,"margins"):
    pylab.margins=margins

if __name__=="__main__":
    from numpy import sin, pi

    def f(x):
        return sin(x)

    plot_constant(f, 0, pi, 4)
    
    pylab.show()
