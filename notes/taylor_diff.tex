\lecture{Taylor series and numerical integration}

\section{Taylor series and error}

From maths methods 2, you know that a smooth function can be approximated
close to a point $x_0$ by a Taylor
series, the formula for which is:
\begin{equation}
  f(x_0+h)=\sum_{k=0}^{\infty}\frac{h^k}{k!}f^{(k)}(x_0)
\end{equation}
The derivation of the Taylor series is presented in appendix \ref{app:taylor}.
When we actually use a Taylor series to approximate a function, we can't
evaluate the whole infinite series, instead we evaluate the first few
terms. It is therefore interesting to investigate what sort
of error we make by just summing a finite number of terms in the series.

Let's look at the $k$-th term of the Taylor series:
\begin{equation}
  f_k=\frac{h^k}{k!}f^{(k)}(x_0)
\end{equation}
The factor:
\begin{equation}
  \frac{f^{(k)}(x_0)}{k!}
\end{equation}
is just a constant value so we can replace this by some constant $c_k$ so
that:
\begin{equation}
  f_k=c_kh^k
\end{equation}
That is, $f_k$ is $\O(h^k)$. 

 Suppose we evaluate $n$ terms of the series. Using \O notation for
the remaining terms, we have:
\begin{equation}
  f(x_0+h)=\sum_{k=0}^{n-1}\frac{h^k}{k!}f^{(k)}(x_0)+\sum_{k=n}^{\infty}\O(h^k)
\end{equation}

Next we can look back at theorem \ref{thm:Oadd}, the addition rule for
\O. This tells us that if we add several functions which are $\O(h^k)$ for
various values of $k$, the
result will be $\O(h^m)$ where $m$ is the least of the $k$
values\footnote{In explaining this, we intentionally skip over sum of the subtleties of infinite
  sums. The result stated is correct in all cases where the radius of
  convergence of the Taylor series is not 0.}. In our
case, the least order is $n$ so we may write:
\begin{equation}
  f(x_0+h)=\sum_{k=0}^{n-1}\frac{h^k}{k!}f^{(k)}(x_0)+\O(h^n)
\end{equation}
In other words, if we approximate a smooth function by the first $n$ terms
of a Taylor series, then the error in the value $f(x_0+h)$ is $\O(h^n)$.

\section{Adding $\O(h^n)$ error terms}

In the preceding section we used expressions of the form:
\begin{equation}
  f(x_0+h)=\sum_{k=0}^{n-1}a_kh^k + \O(h^{n})
\end{equation}
This is a new use of the $\O$ notation: previously we merely used $\O$ as a
property of a function in statements of the form ``$f(h)$ is
$\O(h^n)$''. Adding $\O(h^n)$ to the end of an expression is a very common
practice to indicate the size of an error term. The notation ``$+\O(h^n)$'' should be understood as
meaning ``plus an unknown function which is $\O(h^n)$''. 

This notation has a couple of possibly surprising properties. First, adding
multiple $O$ terms \emph{never}\ cancels to zero. For example, suppose we have two
equations:
\begin{gather}
  f(h)=ah+bh^2+\O(h^3)\\
  g(h)=ch+dh^2+\O(h^3)
\end{gather}
If we subtract $g$ from $f$ then we have:
\begin{gather}
  f(h)-g(h)=ah+bh^2-ch-dh^2+\O(h^3)
\end{gather}
This is because each use of $\O$ is potentially a different unknown error
function. Subtracting them from each other therefore does not result in
cancellation. In accordance with the addition rule for $\O$ (theorem
\ref{thm:Oadd}), if error terms with different powers of $h$ are added, only
the term with the \emph{smallest}\ power need be kept. 

Second, the $\O$ term, by convention, is always positive. The absolute value
signs in the definition of $\O$ (definition \ref{def:bigO}) mean that the
sign of a function does not change its $\O$ properties. 

\section{Taylor series and function approximation}

The Taylor series is possibly the most important tool in numerical
analysis. To demonstrate this, we can apply it to our function approximations from the
previous chapter. Let's start with our piecewise constant approximation. In
the $i$-th interval, the expression for this approximation is:
\begin{equation}
  \fhat(x)=f(a+ih)\quad a+ih\leq x < a+(i+1)h
\end{equation}
Remember that:
\begin{equation}
  h=\frac{b-a}{n}
\end{equation}
where $h$ is the sub-interval width, $b$ is the right hand limit of the
whole interval, $a$ is the left hand limit and $n$ is the number of sub-intervals.

In order to work out the error, we can write the original function as a
Taylor series:
\begin{equation}\label{eq:taylorconstant}
  f(a+ih+\delta x)=f(a+ih)+\O(\delta x)\qquad \delta x=x-(a+ih)
\end{equation}
Now we can write the error at a single point as:
\begin{equation}
  \begin{split}
    E(x)&=\left|f(x)-\fhat(x)\right|\\
      &=\left|f(a+ih)+\O(\delta x)-f(a+ih)\right|\\
      &=\left|\O(\delta x)\right|
  \end{split}
\end{equation}
Due to the arbsolute value signs inside the definition of $\O$, we can drop these
absolute value signs and simpy say that the error is $\O(\delta x)$. That's
not quite what we were after, though, because we really wanted an expression
for the error in terms of the interval size $h$. Fortunately we know how to
relate $\delta x$ and $h$. Remember that for any $x$ we can find the
interval containing $x$ and label it $i$. This implies
\begin{equation}
 a+ih\leq x < a+(i+1)h 
\end{equation} 
In equation \ref{eq:taylorconstant}\ we then defined:
\begin{equation}
  \delta x=x-(a+ih)
\end{equation}
By combining these together we see that $\delta x<h$. From this it can be shown that the error in the piecewise constant
approximation is $\O(h)$.

\begin{exercise}
  If $F(h)$ is $\O(\delta x)$ and $0<\delta x<h$, prove that $F$ is $\O(h)$. 
\end{exercise}

\section{The error in the piecewise linear approximation}

We can apply the same technique, in slightly more complex form, to derive
the error in the piecewise linear approximation.

Assume we wish to evaluate the piecewise approximation $\fhat$ at a point
$x$ and that $a+ih<x<a+(i+1)h$. To simplify the notation a bit, we will
introduce $l$ and $l+h$ as the ends of the interval containing $x$. That is,
\begin{gather}
  l=a+ih\\
  l+h=a+(i+1)h
\end{gather}
We'll also write $\delta x=x-l$ for the position of $x$ in the interval. The
piecewise linear approximation given in equation \ref{eq:linearapprox}\
therefore becomes:
\begin{gather}\label{eq:linapprox}
  \fhat(l+\delta x)=f(l)+\delta x\frac{f(l+h)-f(l)}{h}
\end{gather}
Now, we can write $f(l+h)$ as a Taylor series centred on $l$:
\begin{gather}
  f(l+h)=f(l)+hf'(l)+\O(h^2)
\end{gather}
If we substitute this into equation \ref{eq:linapprox}, we have:
\begin{equation}
  \begin{split}
    \fhat(l+\delta x)&=f(l)+\delta x\frac{f(l)+hf'(l)+\O(h^2)-f(l)}{h}\\
    &=f(l)+\delta x f'(l) +\frac{\delta x}{h}\O(h^2)
  \end{split}
\end{equation}
If we note that $0<\delta x<h$ then we can conclude that the error term is
still bounded by $\O(h^2)$:
\begin{equation}\label{eq:nearlydx2}
  \fhat(l+\delta x)=f(l)+\delta x f'(l) +\O(h^2)
\end{equation}
This looks quite close to what we're after, but we want the error in
$\fhat(l+\delta x)$ as compared with $f(l+\delta x)$, so we now write a
Taylor series expansion for the original function $f(l+\delta x)$ centred at
$l$:
\begin{gather}
  f(l+\delta x)=f(l)+\delta x f'(l)+\O(h^2)
\end{gather}
As before, we have used $0<\delta x<h$ to write the error term as $\O(h^2)$
rather than $\O(\delta x^2)$. We can rearrange this to isolate the terms on
the right hand side of equation \ref{eq:nearlydx2}:
\begin{equation}
  f(l)+\delta x f'(l)=f(l+\delta x)+\O(h^2)  
\end{equation}
We can substitute this last equation into equation \ref{eq:nearlydx2}\ in
order to finally write:
\begin{equation}\label{eq:finallinearerror}
  \fhat(l+\delta x)=f(l+\delta x) +\O(h^2)  
\end{equation}
We can therefore conclude that the pointwise error in the piecewise linear
approximation of a (smooth) function is $\O(h^2)$. Another way to say this
is to describe the piecewise linear approximation as \emph{second order}.

\section{Types of error}

In everyday parlance, the word ``error'' means a mistake, or something which is
wrong. In science and in scientific computing in particular, error has a
different meaning. In these contexts, ``error'' is the difference between a
computed result and the ``true'' result. There are a number of forms of
error, some of the most important of which are:
\begin{description}
\item[Rounding error] is due to the finite precision of floating point
  arithmetic. It is the difference between the result of a floating point
  operation and the infinite precision result.
\item[Truncation error] is due to ignoring the higher order terms in an
  infinite series, such as a Taylor series.
\item[Model error] is due to the approximations made in our description of
  the real world. For example, if we model a pendulum neglecting friction in
  the fulcrum and air resistance, these are model errors.
\end{description}

Error is an inevitable part of science but, as we saw with the catastrophic
cancellation examples, it is essential to remain aware of error in
calculations lest it come to dominate the actual result!


\section{Numerical Integration}

In section \ref{sec:betterapprox}, we were interested in the error
integrated over the whole interval $[a,b]$. This is particularly relevant
for one very important use of numerical mathematics: integrating functions
which we don't know how to integrate analytically. 
Let's first consider the
piecewise linear approximation to a function. On a single interval,
$[l,l+h]$, we can use equation \ref{eq:finallinearerror}\ to write:
\begin{equation}
  \int_l^{l+h}\fhat(x)\d x = \int_l^{l+h}f(x)\d x+\int_l^{l+h}\O(h^2)\d x
\end{equation}
We understand the first two terms of this equation: the integral of the
piecewise linear approximation is equal to the integral of the original
equation plus an error term. But what happens when we integrate $\O(h^2)$?
Remember, the notation $\O(h^2)$ when used as an error term, means an unknown
function $E(x)$ which is $\O(h^2)$. The integral of the error function is
bounded by the maximum magnitude of that function multiplied by the width of
the interval over which integration occurs. That is:
\begin{equation}
  \int_l^{l+h}E(x)\d x < h\left(\max_{l<x<x+h}E(x)\right)
\end{equation}
Figure \ref{fig:trapezoidal} illustrates this bound. The blue shaded area is
the true error on the left of the inequality, while the right of the
inequality is the sum of the blue and pink areas.
\begin{figure}[ht]
  \centering
  \includegraphics{functions/trapezoidal_error}
  \caption{Integration of the piecewise linear approximation to a function
    over one sub-interval $[l,l+h]$. The approximate integral is shown in
    green and the error in blue. The maximum pointwise error is shown by a
    red line and the bound on the integral error is given by the pink area
    plus the blue area. It is clear that the sum of the pink area and the
    blue area is greater than the blue area on its own.}\label{fig:trapezoidal}
\end{figure}

We know $E(x)$ is $O(h^2)$ for any $x$ in $[l,l+h]$ so in particular, it is
$O(h^2)$ at its maximum. This means that the integral error is $h\O(h^2)$. By
inverting the division rule for $\O$ (exercise \ref{ex:odivision}) this
yields the result that the error is $\O(h^3)$. 

\subsection{Evaluating the trapezoidal rule}

The shape of the approximate integration area shown in figure
\ref{fig:trapezoidal}\ gives this form of numerical integration its name: the
trapezoidal rule. To actually evaluate the integral, we can write out the
definition of $\fhat$ and integrate it. Using equation \ref{eq:linapprox}, we
have:
\begin{equation}
  \int_l^{l+h}\fhat(x)\d x=\int_0^{h}f(l)+\delta x
  \frac{f(l+h)-f(l)}{h}\d\delta x
\end{equation}
Note that we have changed variable to $\delta x$ in the right hand side
integral. This is a very easy integral to evaluate:
\begin{equation}
  \begin{split}
  \int_l^{l+h}\fhat(x)\d x&=hf(l)+\frac{h^2}{2}
  \frac{f(l+h)-f(l)}{h}\\
  &=hf(l)+\frac{h}{2}\left(f(l+h)-f(l)\right)\\
  &=\frac{h}{2}f(l)+\frac{h}{2}f(l+h)
  \end{split}
\end{equation}
We can combine this with the error result to write a single statement for
the trapezoidal rule and its error:
\begin{equation}
  \label{eq:trapsingle}
  \int_{l}^{l+h}f(x)=\frac{h}{2}f(l)+\frac{h}{2}f(l+h)+\O(h^3)
\end{equation}

\subsection{The multi-interval trapezoidal rule}\label{sec:multitrapezoidal}

The formula above is a useful integration rule for a single sub-interval, but
we really need to integrate over $[a,b]$ divided into any number of
sub-intervals. Fortunately this is easily achieved by applying equation
\ref{eq:trapsingle}\ to each sub-interval:
\begin{equation}
  \int_{a}^{b}f(x)\d
  x=\sum_{i=0}^{n-1}\left(\frac{h}{2}f(a+ih)+\frac{h}{2}f(a+(i+1)h)+\O(h^3)\right)\quad n=\frac{b-a}{h}
\end{equation}
This gives us a rule for calculating the integral, but what happens to the
error? We sum the $n$ error terms, each of which is $\O(h^3)$. If we sum a
fixed number of $\O(h^3)$ error terms then we already know that the result
will be $\O(h^3)$. However, $n$ isn't fixed, it's a function of $h$ so as
$h$ becomes small, $n$ becomes ever larger. Let's write $E_i(h)$ as the
$i$-th error term. We can once again bound the error using the largest error term:
\begin{equation}
%  \begin{split}
    \sum_{i=0}^{n-1} E_i(h)<n\left(\max_{0\leq i<n}|E_i(h)|\right)
    =\frac{a-b}{h}\left(\max_{0\leq i<n}|E_i(h)|\right)
 % \end{split}
\end{equation}
Now, $E_i(h)$ is $\O(h^3)$ and we know from exercise  \ref{ex:odivision}\
that dividing an $\O(h^3)$ by $h$ produces an $\O(h^2)$ function. $b-a$ is
the length of the whole interval and doesn't change when we change the number of
sub-intervals, so it will be absorbed in the arbitrary constant, $c$, in the
definition of $\O$. We can therefore conclude that the overall error in the
multi-interval trapezoidal rule is $\O(h^2)$: 
\begin{equation}
  \int_{a}^{b}f(x)\d
  x=\sum_{i=0}^{n-1}\left(\frac{h}{2}f(a+ih)+\frac{h}{2}f(a+(i+1)h)\right)+\O(h^2)\quad n=\frac{b-a}{h}
\end{equation}
The analysis above does not depend on the particular characteristics of the
trapezoidal rule: it is a general rule that the multi-interval error term
will be one order lower than that for the single interval. The general term
form the single interval error is the \emph{local truncation error}\ while
multi-interval term is the \emph{global truncation error}.

\subsection{Simpson's rule}

We can obtain a still more accurate representation of the integral of a
function, $f$, over a single sub-interval $[l,l+h]$ by approximating $f$
with a quadratic function. Figure \ref{fig:simpson}\ illustrates this
approximation. 
\begin{figure}[ht]
  \centering
  \includegraphics{functions/simpson_error}
  \caption{Simpson's rule approximates the integral of $f(x)$ on the interval $[l, l+h]$ using a qudratic function.}
  \label{fig:simpson}
\end{figure}

Since a quadratic function is defined by the function value at three points (as
opposed to two for a linear function and one for a constant function),
Simpson's rule employs the value of the function at the centre of the
interval as well as the end points. We will state without proof that the
formula for Simpson's rule on a single interval is:
\begin{equation}
  \int_{l}^{l+h}f(x)\d x=  h\left(\frac{1}{6}f(l) +
    \frac{2}{3}f\left(l+\frac{1}{2}h\right) + \frac{1}{6}f(l+h)\right) + \O(h^5)
\end{equation}
Using the same argument as above, the formula for Simpson's rule over
multiple intervals is:
\begin{multline}
  \int_{a}^{b}f(x)\d
  x=\sum_{i=0}^{n-1}h\left(\frac{1}{6}f(a+ih)+\frac{2}{3}f\left(a+\left(i+\frac{1}{2}\right)h\right)+\frac{h}{6}f(a+(i+1)h)\right)+\O(h^4)\quad\\ n=\frac{b-a}{h}
\end{multline}

\section{Numerical Quadrature}

The trapezoidal rule and Simpson's rule are two specific examples of the
general system of numerical integration known as numerical quadrature. The
basic principle of numerical quadrature is that the integral of a function
can be calculated by evaluating that function at a particular set of points
and computing a weighted sum of those values. The general form of a
quadrature rule is a set of quadrature points $\{x_j|j=0\ldots q-1\}$ in the
interval $[0,1]$ and a set of corresponding weights $\{w_j|j=0\ldots
q-1\}$. The integral on a single sub-interval $[l,l+h]$ is then approximated
using the formula:
\begin{equation}
  \int_{l}^{l+h}f(x)\d
  x\approx\sum_{j=0}^{q-1}w_j hf(l+x_jh)
\end{equation}
In order to calculate the integral over the full interval $[a,b]$, this
procedure is repeated for each sub-interval:
\begin{equation}\label{eq:generalquad}
  \int_{a}^{b}f(x)\d
  x\approx\sum_{i=0}^{n-1}\sum_{j=0}^{q-1}w_j hf(a+ih+x_jh)
\end{equation}
The trapezoidal rule and Simpson's rule are two members of a more general
family known as the Newton-Cotes formulae. Table \ref{tab:newtoncotes}\
shows three of its members.
\begin{table}[ht]
  \centering
\begin{tabular}{llll}
  Name & Quadrature points ($x_j$) & Weights ($w_j$) & Global truncation error\\\hline
  trapezoidal rule & $[0,1]$ & $\left[\frac{1}{2}, \frac{1}{2}\right]$ & $\O(h^2)$\\
  Simpson's rule & $\left[0,\frac{1}{2},1\right]$ &
  $\left[\frac{1}{6},\frac{2}{3},\frac{1}{6}\right]$&$\O(h^4)$\\
  Boole's rule & $\left[0,\frac{1}{4},\frac{1}{2},\frac{3}{4},1\right]$ & $\left[\frac{7}{90},\frac{16}{45},\frac{6}{45},\frac{16}{45},\frac{7}{90}\right]$&$\O(h^6)$
\end{tabular}  
  \caption{Newton-Cotes quadrature rules. This form of the rules is for $h$ in the interval $[0,1]$. Other sources may use other defintions of $h$ with appropriate rescaling of the weights.}
  \label{tab:newtoncotes}
\end{table}

Some patterns are apparent in these quadrature rules. The first is that the
quadrature points are equally spaced in the interval $[0,1]$. This is
characteristic of the Newton-Cotes quadrature rules but is not true for all
families of quadrature. For instance, the widely used Gauss-Lobatto
quadrature rules employ unevenly spaced points. The second pattern to notice
is that the quadrature weights always sum to 1. This is a general pattern
which has to be true for all quadrature formulae on the interval $[0,1]$. To
see why this is so, consider the constant function $f(x)=1$. A quadrature
rule which can not even evaluate the integral of this function would be
rather useless.

A further important pattern is that more accurate quadrature rules generally
require the function to be evaluated at more quadrature points. 

The file ~/numerical-methods-1/newton_cotes.py~ contains Python code for these\linebreak
quadrature rules. The module contains a dictionary ~newton_cotes~ containing
the three quadrature rules from table \ref{tab:newtoncotes}\ under the keys
``trapezoidal'', ``Simpson'' and ``Boole'' respectively. Each quadrature
rule is represented by an object of class ~Quadrature~ containing a member
~x~, which is the array of quadrature points and ~w~, the array of
weights. This can be accessed as follows:
\begin{pythonsnippet}
  In [1]: import newton_cotes

  In [2]: q=newton_cotes.newton_cotes["Simpson"]

  In [3]: q.x
  Out[3]: array([ 0. ,  0.5,  1. ])

  In [4]: q.w
  Out[4]: array([ 0.16666667,  0.66666667,  0.16666667])
\end{pythonsnippet}

\begin{exercise}
  Write a Python module named ~quadrature~ containing a function\linebreak 
  ~quad(f, a, b, n, rule)~. ~quadrature.quad~ should calculate:
  \begin{equation*}
    \int_a^bf(x)\d x
  \end{equation*}
  numerically using equation \ref{eq:generalquad}\ over ~n~
  sub-intervals. The argument ~rule~ should be a string containing
  ``trapezoidal'', ``Simpson'' or ``Boole'' and you should use the
  corresponding quadrature points and weights from the ~newton_cotes~
  module. If ~rule~ is not one of the known quadrature rules, your function
  should raise ~ValueError~ with an appropriate error message.

  You can use the script ~test_quadrature.py~ from ~/numerical-methods-1~ to
  test your function by typing:
  \begin{bash}
  > python test_quadrature.py
  \end{bash}
  For this test to work, ~test_quadrature.py~, ~quadrature.py~ and
  ~newton_cotes.py~ must all be in the same directory.
\end{exercise}

\section{Quadrature in Python}

The ~scipy.integrate~ module contains a very sophisiticated quadrature
function ~quad~ which will evaluate integrals numerically to a very high
precision. For a simple case, we can try integrating $f(x)=x\tan(x)$ on the
interval $[0,1]$:
\begin{pythonsnippet}
  In [1]: from numpy import tan

  In [2]: from scipy.integrate import quad

  In [3]: def f(x):
     ...:     return x*tan(x)
     ...: 

  In [4]: quad(f,0,1)
  Out[4]: (0.42808830136517606, 1.1548411421164902e-14)
\end{pythonsnippet}
The first number in the output is the answer, the second is an error estimate.

\begin{exercise}
  $f(x)=x\tan(x)$ is a very simple example of a function with no simple
  integral. 
  Write a python script ~integrate_x_tan.py~ which uses your ~quadrature.quad~
  function to integrate this function on the interval $[0,1]$ using each
  of the quadrature rules above and for $n=2,4,8,16$. Using your
  ~convergence~ function from exercise \ref{ex:convergence}, check that the
  integral converges at the expected rate from table
  \ref{tab:newtoncotes}\ for each quadrature rule. Use
  ~scipy.integrate.quad~ to generate a ``true'' answer in order to compute
  the error for your convergence calculations. Your script should print out
  the convergence rate estimates along with suitable documentation.
\end{exercise}


