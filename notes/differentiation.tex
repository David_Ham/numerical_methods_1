\lecture{Numerical Differentiation}

\section{Forward differencing}

Just as numerical quadrature enables us to integrate functions by simply
evaluating the function at a number of points, we can also calculate
approximations to the derivatives of a function using weighted sums of
function evaluations.

Remember the elementary definition of the derivative of a function $f$ at a
point $x_0$:
\begin{equation}
  f'(x_0)=\lim_{h\rightarrow 0} \frac{f(x+h)-f(x)}{h}
\end{equation}
We can turn this into an approximation rule for $f'(x)$ by replacing the
limit as $h$ approaches $0$ with a small but finite $h$:
\begin{equation}\label{eq:forwarddiff}
  f'(x_0)\approx \frac{f(x_0+h)-f(x)}{h},\qquad h>0
\end{equation}
Figure \ref{fig:forwarddiff}\ illustrates this approximation. Because the
approximate gradient is calculated using values of $x$ greater than $x_0$,
this algorithm is known as the \emph{forward difference method}.
\begin{figure}[ht]
  \centering
  \includegraphics{functions/forward_diff}
  \caption{Forward difference method for approximating $f'(x_0)$. The derivative is approximated by the slope of the red line, while the true derivative is the slope of the blue line.}\label{fig:forwarddiff}
\end{figure}

So how accurate is this method? For this we can fall back on our old friend
the Taylor series. We can write:
\begin{equation}
  f(x_0+h)=f(x_0)+hf'(x_0)+\O(h^2)
\end{equation}
If we rearrange this expression to isolate the gradient term $f'(x_0)$ on the
left hand side, we find:
\begin{gather}
  hf'(x_0)=f(x_0+h)-f(x_0) +\O(h^2)\\
  f'(x_0)=\frac{f(x_0+h)-f(x_0)}{h}+\O(h)
\end{gather}
So the forward difference method is first order. 

\section{Central differencing}

Compared with our algorithms for numerical integration, a first order method
is not very impressive. In an attempt to derive a more accurate method, we
can observe that all of the higher order integration methods have quadrature
coefficients which are symmetric on each interval. Let's then try to form a
differentiation rule by applying more symmetry. To do this, we'll try using
two Taylor series expansions, one in the positive $x$ direction from $x_0$
and one in the negative direction. Because we hope to achieve better than
first order, we include an extra term in the series:
\begin{gather}
  f(x_0+h)=f(x_0)+hf'(x_0)+\frac{h^2}{2}f''(x_0) + \O(h^3)\\
  f(x_0-h)=f(x_0)-hf'(x_0)+\frac{(-h)^2}{2}f''(x_0) + \O((-h)^3)
\end{gather}
Using the fact that $(-h)^2=h^2$ and the absolute value signs from the
definition of $\O$, this is equivalent to:
\begin{gather}
  f(x_0+h)=f(x_0)+hf'(x_0)+\frac{h^2}{2}f''(x_0) + \O(h^3)\label{eq:fd2}\\
  f(x_0-h)=f(x_0)-hf'(x_0)+\frac{h^2}{2}f''(x_0) + \O(h^3)\label{eq:bd2}
\end{gather}
Remember that we are looking for an expression for $f'(x_0)$. Noticing the
sign change between the derivative terms in the two equations, we subtract
equation \ref{eq:bd2}\ from equation \ref{eq:fd2}\ to give:
\begin{gather}
  f(x_0+h)-f(x_0-h)=2hf'(x_0)+\O(h^3)
\end{gather}
Rearranging a little, we have:
\begin{gather}
  f'(x_0)=\frac{f(x_0+h)-f(x_0-h)}{2h} + \O(h^2)
\end{gather}
This indicates that we have been successful: by taking an interval symmetric
about $x_0$, we have created a second order approximation for the derivative
of $f$. This symmetry gives the scheme its name: the central difference
method. Figure \ref{fig:centraldiff}\ illustrates this scheme.
\begin{figure}[ht]
  \centering
  \includegraphics{functions/central_diff}
  \caption{Central difference method for approximating $f'(x_0)$. The derivative is approximated by the slope of the red line, while the true derivative is the slope of the blue line.}\label{fig:centraldiff}
\end{figure}

\begin{exercise}
  Create a python module named ~differentiation~ containing a function
  ~forward_difference(f,x,h)~ which uses the forward difference method to
  approximate the derivative of $f$ at the point $x$ using a step size of
  $h$.
\end{exercise}

\begin{exercise}
  Add a second function ~central_difference(f,x,h)~ to your\linebreak
  ~differentiation~ module. This should approximate the derivative of $f$ at
  $x$ using the central difference method with the value of $h$ supplied.
\end{exercise}

You can use the script ~test_derivative.py~ from the ~/numerical-methods-1~
directory to test your differentiation functions.

\begin{exercise}
  Write a Python script ~plot_derivative_error.py~ which uses your\linebreak
  ~differentiation~ module to calculate the derivative of $f(x)=e^x$ at the
  point $x=1$. Your script should plot the error in the derivative of $f$ at
  $x=1$ with $100$ values of $h$ between $0.0001$ and $0.1$ for each of the
  forward difference method and the central difference method. 

  Label the $x$ axis $h$ and the $y$ axis \emph{error}\ using the
  ~pylab.xlabel~ and ~pylab.ylabel~ commands. Also include a legend
  labelling the lines corresponding to central and forward difference
  methods using the ~pylab.legend~ command. Give your figure a suitable
  title using ~pylab.title~.

  Submit your ~plot_derivative_error.py~ script. There is no need to submit
  a printout of the plot.
\end{exercise}

\section{Calculating second derivatives}

Numerical differentiation may be extended to the second derivative by noting
that the second derivative is the derivative of the first derivative. That
is, if we define:
\begin{equation}
  g(x)=f'(x)
\end{equation}
then
\begin{equation}
  f''(x)=g'(x)
\end{equation}
We have noted above that the central difference method, being second order,
is superior to the forward difference method so we will choose to extend
that.

In order to calculate $f''(x_0)$ using a central difference method, we first
calculate $f'(x)$ for each of two half intervals, one to the left of $x_0$
and one to the right:
\begin{gather}
  f'(x_0+\frac{h}{2})\approx\frac{f(x_0+h)-f(x_0)}{h}\\
  f'(x_0-\frac{h}{2})\approx\frac{f(x_0)-f(x_0-h)}{h}
\end{gather}
We can now calculate the second derivative using these two values. Note that
we know $f'(x)$ at the points $x_0\pm\frac{h}{2}$, which are only $h$ rather
than $2h$ apart. Hence:
\begin{align}
    f''(x_0)&\approx\frac{f'(x_0+\frac{h}{2})-f'(x_0-\frac{h}{2})}{h}\\
    &\approx\frac{\frac{f(x_0+h)-f(x_0)}{h}-\frac{f(x_0)-f(x_0-h)}{h}}{h}\\
    &\approx\frac{f(x_0+h)-2f(x_0)+f(x_0-h)}{h^2}\label{eq:central2nd}
\end{align}

\begin{exercise}
  By substituting power series for $f(x+h)$ and $f(x-h)$ into the right hand
  side of equation
  \ref{eq:central2nd}, prove that the central difference method for the
  second derivative is $\O(h^2)$.
\end{exercise}

\section{A very brief primer on numerical methods for ODEs*}

One of the most important applications of numerical mathematics in the
sciences is the numerical solution of differential equations. This is a vast
topic which rapidly becomes somewhat advanced, so we will restrict ourselves
here to a very brief introduction to the solution of first order ordinary
differential equations (ODEs). A much more comprehensive treatment of
this subject is to be found in the Numerical Methods 2 module.

Suppose we have the general first order ODE:
\begin{gather}\label{eq:generalode}
  x'(t)=f(x,t)\\
  x(t_0)=x_0
\end{gather}
That is, the derivative of $x$ with respect to $t$ is some known function of
$x$ and $t$, and we also know the initial condition of $x$.

If we manage to solve this equation analytically, the solution will be a
function $x(t)$ which is defined for every $t>t_0$. In common with all of the
numerical methods we have encountered in this module, our objective will be
to find an approximate solution to equation \ref{eq:generalode}\ at a
finite set of points. In this case, we will attempt to find approximate
solutions at $t=t_0,t_0+h,t_0+2h,t_0+3h,\ldots$.

It is frequently useful to think of the independent variable, $t$, as
representing time. A numerical method steps forward in time units of $h$,
attempting to calculate $x(t+h)$ in using the previously calculated value
$x(t)$. 

\subsection{Euler's method*}

To derive a numerical method, we can first turn once again to the Taylor
series. In this case, we could write:
\begin{equation}
  x(t+h)=x(t)+h x'(t) +\O(h^2)
\end{equation}
Using the definition of our ODE from equation \ref{eq:generalode}, we can
substitute in for $x'(t)$:
\begin{equation}
  x(t+h)=x(t)+h f(t,x(t))+\O(h^2)
\end{equation}
Notice that the value of $x$ used in the evaluation of $f$ is that at time
$t$. This simple scheme is named \emph{Euler's method} after the 18th
century Swiss mathematician, Leonard Euler. 

The formula given is used to calculate the value of $x$ one time step forward
from the last known value. The error is therefore the local truncation
error. If we actually wish to know the value at some fixed time $T$ then we
will have to calculate $(T-t_0)/h$ steps of the method. Using the same
argument presented in section \ref{sec:multitrapezoidal}, this sum over
$\O(1/h)$ steps results in a global truncation error for Euler's method of
$\O(h)$. In other words, Euler's method is first order.

To illustrate Euler's method, and convey the fundamental idea of all
time stepping methods, we'll use Euler's method to solve one of the simplest
of all ODEs:
\begin{gather}
  x'(t)=x\\
  x(0)=1
\end{gather}
We know, of course, that the solution to this equation is $x=e^t$, but let's
ignore that for one moment and evaluate $x(0.1)$ using Euler's method with
steps of $0.05$. The first step is:
\begin{align}
  x(0.05)&\approx x(0)+0.05x'(0)\\
  &\approx1+.05\times1\\
  &\approx 1.05
\end{align}
Now that we know $x(0.05)$, we can calculate the second step:
\begin{align}
  x(0.1)&\approx x(0.05)+0.05x(0.05)\\
  &\approx 1.05+.05\times1.05\\
  &\approx 1.1025
\end{align}
Now the actual value of $e^{0.1}$ is around $1.1051$ so we're a couple of
percent off even over a very short time interval. 

\begin{exercise}*
  Write a python module named ~ode~ containing a function\linebreak
  ~ode.euler(f,x_0,t_0,T,h)~ which calculates the approximate solution of
  the system:
  \begin{gather*}
    x'(t)=f(x,t)\\
    x(t_0)=x_0
  \end{gather*}
  using steps of size $h$ from $t_0$ to $T$. Your function should return
  ~(t,x)~ where ~t~ is the array $t,t+h,t+2h\ldots T$ and ~x~ is the
  corresponding array of approximate solutions. 

  You could test your function by solving the simple ODE above using
  decreasing values of $h$ and checking that you converge to the analytic
  solution at $\O(h)$.
\end{exercise}

\subsection{Heun's method*}

Euler's method is first order because it calculates the derivative using
only the information available at the beginning of the time step. As we
observed for numerical integration and differentiation, higher order
convergence can be obtained if we also employ information from other points in
the interval. The method known as Heun's method may be derived by attempting
to use derivative information at both the start and the end of the interval:
\begin{align}
  x(t+h)&\approx x(t)+\frac{h}{2}\left(x'(t)+x'(t+h)\right)\\
  &\approx x(t)+\frac{h}{2}\big(f(x,t)+f(t+h,x(t+h))\big)
\end{align}
The difficulty with this approach is that we now require $x(t+h)$ in order
to calculate the final term in the equation, and that's what we set out to
calculate so we don't know it yet! The solution to this dilemma adopted in
Heun's method is to use a first guess at $x(t+h)$ calculated using Euler's
method:
\begin{gather}
  \tilde{x}(t+h)=x(t)+hf(x,t)\\
  x(t+h)\approx x(t)+\frac{h}{2}\big(f(x,t)+f(t+h,\tilde{x}(t+h))\big)
\end{gather}
The generic term for schemes of this type is \emph{predictor-corrector}. The
initial calculation of $\tilde{x}(t+h)$ is used to predict the new value of
$x$ and then this is used in a more accurate calculation to produce a more
correct value. We will state without derivation or proof that the global
truncation error in Heun's method is $\O(h^2)$. 

\begin{exercise}*
  Add a function ~heun(f,x_0,t_0,T,h)~ to your ~ode~ module. The function
  should solve the same problem as ~ode.euler~ but use Heun's method rather
  than Euler's method. 
\end{exercise}

\begin{exercise}*
  Produce a Python script ~plot_ode.py~ which uses ~ode.euler~ and
  ~ode.heun~ to produce a plot comparing Euler's method and Heun's method to
  the analytic solution of the ODE:
  \begin{gather}
    x'(t)=x\\
    x(0)=1
  \end{gather}
  Use $h=0.5$ and calculate the solution in the range $[0,2.5]$. Ensure that
  your plot has labelled $x$ and $t$ axes, a title and a legend. If you
  choose to submit an answer to this exercise, you need only submit your
  Python code, it is not necessary to print the plot.
\end{exercise}