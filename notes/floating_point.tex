\lecture{Floating point numbers}

\section{Floating point numbers}

Fixed sized numbers present even stronger problems for representing real
numbers than for integers, as there are infinitely many reals between every
two integers. It is also fairly common to wish to represent real numbers at
a huge range of scales. For example, the angular velocity of the earth is
approximately $\unit[7.3\times10^{-5}]{rad/s}$ while the age of the Earth is
around $\unit[1.4\times10^{17}]{s}$.

In fact, scientific notation gives us a hint as to how computers deal with
these numbers. A \emph{floating point number}\ is represented as follows:
\begin{equation}
 s1.m\times 2^{e-b}
\end{equation}
Where: $s$ is the sign (0 for $+$, $-$ for minus), $m$ is known as the
\emph{mantissa}\ and $e$ is the \emph{exponent}. $b$ is the exponent $bias$
which is present to enable the exponent to take negative values, since $e$
is represented as an unsigned integer.

Lets unpack this notation a little. Why do we know that the first digit of
the mantissa is always a 1? Well actually scientific notation is ambiguous:
$3.0\times10^0$ is the same as $0.3\times10^1$. However it's much more
efficient to have a single storage form for each number. Floating point
arithmetic therefore adopts the convention that the exponent will be set so
that the digit before the point is non-zero. This is known as the
\emph{normal form}\ of the number. For example, the normal form of 3.0 is
$3.0\times10^0$. Of course computer floating point arithmetic is binary so
the normal form of 3.0 is actually $1.1_2\times2^1$. Since there are only two
binary digits (1 and 0), by choosing that the first digit is not zero we
have actually exactly chosen it to be 1. Because we know that the first
digit will be a 1, we don't actually have to store it. We can ``steal'' the
extra digit. Suppose we have a very small floating point number with 1 sign
bit, 4 bits of mantissa and 3 bits of exponent. For simplicity we'll assume
the bias is 0. Standard floating point numbers have the sign bit first,
followed by the exponent and then the mantissa. So the number 3.0 will look
like:
\[
\begin{array}{|c|c|c|c|>{\columncolor{palegrey}}c|c|c|c|c|}
  \hline
  s & e & e & e & 1 & m & m & m & m \\\hline
  \pm & 2^2 & 2^1 & 2^0 & 2^0 & 2^{-1} & 2^{-2} & 2^{-3} & 2^{-4} \\\hline
  0 & 0 & 1 & 0 & 1 & 1 & 0 & 0 & 0 \\\hline 
\end{array}
\]
Recall that we don't actually store the 1 bit so the number will appear as
00101000. 

This isn't actually a very useful floating point number format, though,
because we have no capacity to store negative exponents so we can't store
any numbers with absolute value less than 1. Our exponent can take values
from 1 to 6\footnote{an exponent value of zero or all ones has special meaning: see
  section \ref{sect:floatingspecial}}. We therefore choose a bias of 3 with the result that our effective
exponent runs from -2 to +3. In this representation, the number 3 becomes:
\[
\begin{array}{|c|c|c|c|>{\columncolor{palegrey}}c|c|c|c|c|}
  \hline
  s & e & e & e & 1 & m & m & m & m \\\hline
  \pm & 2^2 & 2^1 & 2^0 & 2^0 & 2^{-1} & 2^{-2} & 2^{-3} & 2^{-4} \\\hline
  0 & 1 & 0 & 0 & 1 & 1 & 0 & 0 & 0 \\\hline 
\end{array}
\]
The binary representation of 3.0 is therefore 01001000.

\begin{exercise}
  Find the binary representation of the following numbers in a floating
  point format with one sign bit, a 3 bit exponent, a 4 bit mantissa and a
  bias of 3:
  \begin{enumerate}
  \item 1.0
  \item 0.5
  \item 10
  \end{enumerate}
\end{exercise}

\subsection{Round-off error}

Because floating point numbers have only finite precision, there are always
numbers which can't be represented. In particular, it is not true that an
arithmetic operation on two floating point numbers, if calculated exactly,
will always result in a number which can be represented exactly. We are
already familiar with this concept from decimal notation. For example there
is no exact finite precision decimal representation of $\frac{1}{3}$.

By the definition of floating point numbers, the relative distance between
adjacent representable numbers is fixed. Indeed, the largest \emph{relative}\
gap in floating point numbers is given by the difference between 1 and the
next floating point number greater than 1. In the case of our 8 bit floating
point representation, the next number greater than 1 is given by $1+2^{-4}$
so the relative gap is $2^{-4}=0.0625$. More generally, if we have an $n_m$
bit mantissa, then the relative gap is $2^{-n_m}$. This means that for every
floating point number $x$, there is another floating point number $y$ for
which:
\begin{equation}
  \frac{|x-y|}{|x|}\leq 2^{-n_m}
\end{equation}
Another way of saying this is that for every real number $x$ which lies
within the range of our floating point numbers, there is a floating point
number $x'$ which lies not more than half this distance away:
\begin{equation}
  |x-x'|\leq \frac{1}{2}2^{-n_m}|x|
\end{equation}
This quantity has a special name, \emph{machine epsilon}:
\begin{align}
  \eps&=\frac{1}{2}2^{-n_m}\\
  &=2^{-n_m-1}
\end{align}
The difference between a real number and its  nearest floating point
equivalent is known as round-off error. The relative round-off error is
guaranteed to be no more than $\eps$.

\subsection{IEEE standard floating point numbers}

The floating point numbers in use on essentially all modern computers employ
many more bits than our simple 8. The Institute of Electronic and Electrical
Engineers standards define various sizes:

\begin{tabular}{lccccc}
  precision & total size &sign bits & mantissa bits & exponent bits & exponent bias\\
  half & 16 & 1 & 11 & 5 & 15 \\
  single & 32 & 1 & 23 & 8 & 127 \\
  double & 64 & 1 & 52 & 11 & 1023 \\ 
  quad & 128 & 1 & 112 & 15 & 1383
\end{tabular}

These values do not include the implicit 1 bit in the mantissa. In Python,
the default is double precision and it is exceptionally rare to use any
other size. For applications written in languages other than Python,
single precision is often used. Quad precision is reserved for special
applications and tends to be slow as hardware tends to only natively support
single and double precision. Half precision floating point numbers are
essentially never used in modern practice.

\begin{exercise}
  Calculate $\eps$ for IEEE single and double precision floating point numbers.
\end{exercise}

\begin{exercise}
  Using the value of machine epsilon for double precision numbers, use
  Python to calculate ~1.0+eps~ and ~1.0+2*eps~. What is the answer?
\end{exercise}

\subsection{Floating point arithmetic}

The four basic operations of arithmetic all have floating point equivalents,
which, when it is necessary to make a distinction, are usually written:
$\oplus$, $\ominus$, $\otimes$, and $\odiv$. If we write $\odot$ for any
floating point operation and $\cdot$ for the corresponding exact operation,
then the fundamental rule of floating point arithmetic is that for any
floating point numbers $x$ and $y$, there exists some $\epsilon$ with
$|\epsilon|\leq\eps$ such that:
\begin{equation}
  x\odot y=(1+\epsilon)(x \cdot y)
\end{equation}
In other words, the largest relative error which can occur in a single
floating point operation is $\eps$. 

\subsection{Catastrophic cancellation}\label{sec:catastrophic}

A relative error of $\eps$ might seem acceptable for all practical purposes,
especially for double precision floats as $\eps$ is really rather
small. However, a particular problem occurs when adding and subtracting
numbers which are very close together. Suppose we have some number $x$ and a
much smaller number $\delta$ and we calculate:
\begin{gather}
  y=x+\delta\\
  z=y-x
\end{gather}
Clearly in exact arithmetic, $z=\delta$. What happens in floating point?
\begin{equation}
  \begin{split}
    y&=(1+\epsilon_1)(x+\delta) \\
    &=x+\delta+ \epsilon_1x+\epsilon_1\delta
  \end{split}
\end{equation}
for some $|\epsilon_1|<\eps$. Then for some $|\epsilon_2|<\eps$:
\begin{equation}
  \begin{split}
    z&=y-x\\
    &=(1+\epsilon_2)(\delta+ \epsilon_1x+\epsilon_1\delta)\\
    &=\delta+ \epsilon_1x+\epsilon_1\delta+ \epsilon_2\delta+ \epsilon_2\epsilon_1x+\epsilon_2\epsilon_1\delta.
  \end{split}
\end{equation}
Most of the terms are guaranteed to be
very small compared with the correct answer of $\delta$ but $x$ is much
larger than $\delta$ so $\epsilon_1x$ may actually be significant. For
sufficiently unfortunate input values and especially for repeated additions
and subtractions of numbers very close together, the answer can end up with
no correct digits!

\begin{exercise}
  The expression $(1-x)^{10}$ can be expanded as 
  \[
  1 - 10 x + 45 x^{2} - 120
  x^{3} + 210 x^{4} - 252 x^{5} + 210 x^{6} - 120 x^{7} + 45 x^{8} - 10
  x^{9} + x^{10}.
  \]
  Use Python to evaluate this expression using the bracket form and then the
  expanded formula for the values $0.91$, $0.92$,
  $0.93$, $0.94$, $0.95$ and $0.96$. Which formula is more accurate? Why?
  \emph{Hint:} how can you tell you have the correct answer?
\end{exercise}

\subsection{Special values: 0, Inf and NaN*}\label{sect:floatingspecial}

The astute reader will have noticed a problem with the floating point format
described here. If the first digit of the mantissa is always implicitly 1,
how do we represent the number 0.? The answer is that the value 0 in the
exponent field is treated as special. For our 8 bit floating point format, 0.
is represented by:
\[
\begin{array}{|c|c|c|c|>{\columncolor{palegrey}}c|c|c|c|c|}
  \hline
  s & e & e & e & 1 & m & m & m & m \\\hline
  \pm & 2^2 & 2^1 & 2^0 & 2^0 & 2^{-1} & 2^{-2} & 2^{-3} & 2^{-4} \\\hline
  0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\\hline 
\end{array}
\]
so the actual stored value is 00000000. There is also a separate -0. value:
\[
\begin{array}{|c|c|c|c|>{\columncolor{palegrey}}c|c|c|c|c|}
  \hline
  s & e & e & e & 1 & m & m & m & m \\\hline
  \pm & 2^2 & 2^1 & 2^0 & 2^0 & 2^{-1} & 2^{-2} & 2^{-3} & 2^{-4} \\\hline
  1 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\\hline 
\end{array}
\]
or 10000000. Positive and negative zero obey the usual sign rules for
multiplication and division but compare equal. That is ~1.0*-0.0~ returns
~-0.0~ but ~0.0==-0.0~ is ~True~.

Just as for integers, the problem of overflow occurs for floats: for any
given size of floating point number, there are values too large to be
represented. A related issue occurs if division by zero or another undefined
operation occurs. For plain Python floating point numbers, the response in
each of these cases is that an exception occurs:
\begin{pythonsnippet}
  In [97]: 10.**400
  -------------------------------------------------------------
  OverflowError               Traceback (most recent call last)

  /home/dham/<ipython console> in <module>()

  OverflowError: (34, 'Numerical result out of range')

  In [98]: 0./0.
  -------------------------------------------------------------
  ZeroDivisionError           Traceback (most recent call last)

  /home/dham/<ipython console> in <module>()

  ZeroDivisionError: float division
\end{pythonsnippet}
However as was the case with integer overflow, if we use a numeric Python
array then we receive the floating point answer direct from the hardware:
\begin{pythonsnippet}
  In [101]: 0.0/numpy.array([0.0])
  Out[101]: numpy.array([ NaN])
  
  In [102]: numpy.array([10.])**400
  Out[102]: numpy.array([ Inf])
\end{pythonsnippet}
These are two more special floating point values. ~Inf~ is short for
infinity but should really be thought of as ``a value too big to be
represented''. ~Inf~ is a signed quantity and follows the usual rules for
positive and negative numbers so, for example\linebreak
~-1./numpy.array([0.])==numpy.array([-Inf])~. ~NaN~ means ``not a number'' and is
returned in circumstances where even the sign of the answer is unknown. 
Some of the properties of the special floating point numbers are summarised
in the following tables. In each case, $x$ is an arbitrary finite positive
floating point number. For each table, the row label comes before the operator
and the column label after.

\begin{tabular}{r|ccccccc}
  == & $x$ & $-x$ & $0.0$ & $-0.0$ & ~Inf~ & ~-Inf~ & ~NaN~ \\\hline
  $x$ & \T & \F & \F & \F & \F& \F & \F\\
  $-x$ & \F& \T& \F& \F&\F &\F & \F\\
  $0.0$ &\F &\F &\T &\F &\F &\F &\F \\
  $-0.0$ &\F &\F &\F &\T &\F &\F &\F \\
  ~Inf~  &\F &\F & \F& \F& \T& \F&\F \\
  ~-Inf~ &\F &\F &\F &\F &\F &\T &\F \\
  ~NaN~ &\F & \F& \F& \F& \F& \F& \textbf{\F}\\
\end{tabular}

Notice that ~NaN~ compares false to itself!

\begin{tabular}{r|ccccccc}
  $>$ & $x$ & $-x$ & $0.0$ & $-0.0$ & ~Inf~ & ~-Inf~ & ~NaN~ \\\hline
  $x$ & \F & \T & \T & \T & \F & \T & \F\\
  $-x$ & \F & \F & \F & \F & \F & \T & \F\\
  $0.0$ & \F& \T& \F& \F&\F & \T & \F\\
  $-0.0$ &\F &\T & \F& \F& \F& \T & \F\\
  ~Inf~  &\T &\T &\T &\T &\F &\T &\F \\
  ~-Inf~ &\F &\F &\F &\F &\F &\F &\F \\
  ~NaN~ &\F &\F &\F &\F &\F &\F &\F \\
\end{tabular}

Once again, notice that ~NaN~ always compares false.


\begin{tabular}{r|ccccccc}
  $+$ & $x$ & $-x$ & $0.0$ & $-0.0$ & ~Inf~ & ~-Inf~ & ~NaN~ \\\hline
  $x$ & $2x$ &$0.0$ & $x$ & $x$  & ~Inf~  & ~-Inf~ & ~NaN~  \\
  $-x$ & $0.0$ &$-2x$ & $-x$ & $-x$  & ~Inf~  & ~-Inf~ & ~NaN~ \\
  $0.0$ & $x$& $-x$ & $0.0$ & $0.0$ & ~Inf~ & ~-Inf~ & ~NaN~ \\
  $-0.0$ &$x$& $-x$ & $0.0$ & $-0.0$ & ~Inf~ & ~-Inf~ & ~NaN~ \\
  ~Inf~  &~Inf~ &~Inf~ &~Inf~ &~Inf~ & ~Inf~ & ~NaN~ & ~NaN~\\
  ~-Inf~ &~-Inf~ &~-Inf~ &~-Inf~ &~-Inf~ &~NaN~ &~-Inf~ & ~NaN~\\
  ~NaN~ &~NaN~ &~NaN~ & ~NaN~& ~NaN~& ~NaN~& ~NaN~& ~NaN~ \\
\end{tabular}

\begin{tabular}{r|ccccccc}
  $\times$ & $x$ & $-x$ & $0.0$ & $-0.0$ & ~Inf~ & ~-Inf~ & ~NaN~ \\\hline
  $x$ & $x^2$ &$-x^2$ & $0.0$ & $-0.0$  & ~Inf~  & ~-Inf~ & ~NaN~  \\
  $-x$ & $-x^2$ &$x^2$ & $-0.0$ & $0.0$  & ~-Inf~  & ~Inf~ & ~NaN~ \\
  $0.0$ & $0.0$& $-0.0$ & $0.0$ & $-0.0$ & ~NaN~ & ~NaN~ & ~NaN~ \\
  $-0.0$ &$-0.0$& $0.0$ & $-0.0$ & $0.0$ & ~NaN~ & ~NaN~ & ~NaN~ \\
  ~Inf~  &~Inf~ &~-Inf~ &~NaN~ &~NaN~ & ~Inf~ & ~-Inf~ & ~NaN~\\
  ~-Inf~ &~-Inf~ &~Inf~ &~NaN~ &~NaN~ &~-Inf~ &~Inf~ & ~NaN~\\
  ~NaN~ &~NaN~ &~NaN~ & ~NaN~& ~NaN~& ~NaN~& ~NaN~& ~NaN~ \\
\end{tabular}

\begin{tabular}{r|ccccccc}
  $\div$ & $x$ & $-x$ & $0.0$ & $-0.0$ & ~Inf~ & ~-Inf~ & ~NaN~ \\\hline
  $x$ & $1.0$ &$-1.0$ & ~Inf~ & ~-Inf~  & $0.0$  & $-0.0$ & ~NaN~  \\
  $-x$ & $-1.0$ &$1.0$ & ~-Inf~ & ~Inf~  & $-0.0$  & $0.0$ & ~NaN~ \\
  $0.0$ & $0.0$& $-0.0$ & ~NaN~ & ~NaN~ & $0.0$ & $-0.0$ & ~NaN~ \\
  $-0.0$ &$-0.0$& $0.0$ & ~NaN~ & ~NaN~ & $-0.0$ & $0.0$ & ~NaN~ \\
  ~Inf~  &~Inf~ &~-Inf~ &~Inf~ &~-Inf~ & ~NaN~ & ~NaN~ & ~NaN~\\
  ~-Inf~ &~-Inf~ &~Inf~ &~-Inf~ &~Inf~ &~NaN~ &~NaN~ & ~NaN~\\
  ~NaN~ &~NaN~ &~NaN~ & ~NaN~& ~NaN~& ~NaN~& ~NaN~& ~NaN~ \\
\end{tabular}

\begin{exercise}
  * Write a program in Python which produces each of the tables above. It
  will be easier to use $\pm1.0$ rather than $\pm x$.
\end{exercise}

\section{Python representation of integers and floats}

Python's very flexible typing system means that it is often not necessary to
pay attention to whether a number is an integer or a float. However it helps
to know what the rules are. 

\subsection{Literals}

Any number written without a decimal point is automatically stored as an
integer, any number with a decimal point is a float. So, for example, ~1~ is
an integer but ~1.~ or ~1.0~ is a float.

It is also possible to enter floats in scientific notation using the letter
~e~ to separate the mantissa from the exponent. So, for example, ~5.e2~
means $5\times10^2$ or in other words ~500.~. Note that the base of the
exponent is 10, not 2.

\subsection{Casting to type}

It is also possible to use a function to force a value to be a float or an
integer. For example ~float(10)~ is the floating point value ~10.0~. The
corresponding function ~int~ operates by truncating the fractional part
towards zero so, for example ~int(1.9)~ is the integer ~1~ and ~int(-1.9)~
is the integer ~-1~. Using a function to change the type of a value is known
as \emph{casting}.

\subsection{Special values}

A special case of casting occurs if for some reason we wish to produce the
special values ~Inf~, ~-Inf~ and ~NaN~. These are created with the commands
~float('Inf')~, ~float('-Inf')~ and ~float('NaN')~ respectively.

\section{Complex numbers}

Python also has inbuilt support for complex numbers. For this, see section
1.6 in Langtangen.

\section{Further exercises*}
You learnt in Maths Methods 1 that the exponential function can be
represented as the series:
\begin{equation}\label{ex:exp_series}
  e^x=\sum_{i=0}^{\infty} \frac{x^i}{i!}
\end{equation}

\begin{exercise}* Write a Python module ~series.py~ containing a function
  ~exp_series(x,n)~ which approximates $\exp{x}$ using the first $n$ terms
  of the sum in equation \ref{ex:exp_series}. Check your work by ensuring
  that ~exp_series(1.0,100)~ returns the same result as
  ~math.exp(1.0)~. \emph{Hint:}\ Cast ~x~ to ~float~ to ensure your function
  does the right thing if the user enters an integer.
\end{exercise}

\begin{exercise}*
  Evaluate $e^1$ using your function for values of ~n~ ranging from 1 to
  10. Now do the same for $e^{-5}$. What goes wrong? \emph{Hint:}\ look at
  section \ref{sec:catastrophic}
\end{exercise}

\begin{exercise}**
  Using the identity:
  \begin{equation*}
    e^{-x}=\frac{1}{e^x},
  \end{equation*}
  produce a modified function ~exp_safe(x,n)~ which behaves correctly for
  negative inputs. Include ~exp_safe~ in your ~series~ module.
\end{exercise}

\section{Further reading}

The seminal work on floating point arithmetic is Goldberg, D. 1991. \emph{What every computer scientist should know about floating-point arithmetic}. ACM Comput. Surv. 23, 1 (Mar. 1991), 5-48. DOI=http://doi.acm.org/10.1145/103162.103163
