\lecture{Introduction}

\section{Contact details}

\begin{tabular}{ll}
  \textbf{Lecturer:} & Dr David Ham\\
  \textbf{Email:} & David.Ham@imperial.ac.uk \\
  \textbf{Office:} & 3.55\\
  \textbf{Phone:} & 020 7594 6439 (x46439)\\
\end{tabular}

\section{Aims}

The course will cover the basic techniques which underlie the computational
mathematical techniques in common use in the Earth sciences. It will provide
an overview of the capabilities and limitations of scientific computation
including the concepts of error and conditioning. The course will cover
techniques applicable to both observational data and simulation and will
have an emphasis on the practical implementation of the methods studied in
the Python programming language.


\section{Objectives}
By the end of this course, you should be able to:
\begin{itemize}
\item write Python code for a number of important and useful mathematical algorithms
\item understand the sources of error in algorithms
\item be able to apply numerical techniques to solve problems which occur in
  geoscience such as curve fitting, solving systems of equations and root finding
\end{itemize}

\section{Assumed knowledge}

The prerequisites for this module are Maths Methods 1 and 2, and Programming
for Geoscientists. This module will use and build upon the material from those courses
extensively. Particular use will be made of:

\begin{description}
\item[Maths methods 1] Newton-Raphson iteration, matrices and vectors.
\item[Maths methods 2] Taylor series.
\item[Programming for Geoscientists] writing and debugging short programmes
  in Python.
\end{description}


\section{Lecture plan}

The module will cover the numerical implementation of many of the areas of
mathematics which will have been covered at school and in previous
university modules. 

We will start by looking at basic arithmetic: addition, subtraction and
multiplication. We will examine the impact of using binary arithmetic on a
computer with finite number lengths.

The following three weeks will focus on the numerical implementation of
calculus: first differentiation and then integration. Week 5, root finding,
is concerned with numerical methods for automatically solving equations on a
computer. In weeks 6 and 7 we will examine numerical linear algebra:
calculation and equation solving with matrices and vectors. This leads up to
the final week in which we will learn to solve overdetermined systems in a
least squares sense and apply this knowledge to the problem of fitting
curves to scientific data.

\begin{description}
\item[Lecture 1] Binary arithmetic
\item[Lecture 2] Floating point numbers, round-off error and Approximating functions
\item[Lecture 3] Taylor series and numerical integration
\item[Lecture 4] Numerical differentiation
\item[Lecture 5] Root finding
\item[Lecture 6] Numerical linear algebra
\item[Lecture 7] LU factorisation
\item[Lecture 8] Curve fitting and least squares solutions
\end{description}


\section{Course text}

The course will be primarily taught from lecture notes, which will be issued
each week. You will also find it useful to refer back to the course text for
Programming for Geoscientists:

Langtangen, Hans Petter \emph{A primer on scientific computing with Python},
Springer, 2009.

\section{Assessment}

\subsection{Exam}
There will be a question associated with this module on one of the combined
exams. As usual, a choice of two questions will be provided of which
students taking this module must answer at least one. The exam will focus on
the \emph{mathematical}\ parts of the module.

\subsection{Programming exercise}

The one piece of assessed coursework will be a programming exercise which
will take place at some point during the reading week at the end of the
term. You will be given some mathematical expressions or algorithms and
asked to write programmes in Python which implement them. The programming
exercise will focus on testing the \emph{programming}\ parts of the
module. 

\section{Coursework}

The lecture notes contain exercises and the last hour of each lecture will be devoted to working on
exercises. The lecturer and demonstrators will be available to provide help
and immediate feedback. In addition, every two weeks you should hand in the
solutions to those two weeks exercises in order to receive written
feedback. The submission dates for the exercises have already been set up on
ESESIS.

It is likely that you will need to devote additional time to completing the
exercises outside of the lectures.

\textbf{The coursework is not assessable}. The solutions you hand in will be
\emph{corrected}\ but no numerical marks will be given. The coursework is
therefore your opportunity to gain \emph{feedback}\ on your progress with no
penalty for failure. You are encouraged to hand in your best attempt at a
solution even if it is wrong in some way so that you receive feedback in the
form of corrections.

\subsection{The course directory}

There are various example programs, code and data to assist with the
exercises. These are to be found in the
~/numerical-methods-1~ directory on ~student.ese.ic.ac.uk~, which is the
machine you log in to from the department labs. The exercises will indicate
which files you need. You should copy the files to the directory you are
working in before using them. It is a good idea to create a new working
directory for each topic covered in the course so as to maintain some order
in your work and reduce possibilities for confusion.

\section{What to hand in}

There are various sorts of question in this module, each of which has
different outputs which need to be handed in. ESESIS has been configured to
expect \emph{both}\ a written part and an online part to each set of
exercises. Please follow the instructions in the subsequent paragraphs
carefully in determining what to hand in online and what to hand in on paper.

\subsection{Programs}

Some questions will ask you to write a program, or a module containing
particular functions. For these questions, you should submit the source code
\emph{online}\ but also submit \emph{printouts}. The demonstrator will run
your source code on a computer to check for any errors and will provide
written feedback on the printouts.

The easiest way to obtain printouts of your programs is using ~a2ps~. For
example, if you have a Python file named ~foo.py~ then:
\begin{bash}
   > a2ps foo.py
\end{bash}
will print a nicely formatted version of ~foo.py~ to the college print
system. 

ESESIS will only allow you to upload a single file for each assignment so
you need to bundle together the files you intend to upload in a single
archive file. The easiest way to do this is to place all the files you
intend to upload into a single directory. For example, suppose you had
placed all the files you wish to upload for weeks 1 and 2 in a directory
called ~week_1_2~. From the \emph{directory above}\ ~week_1_2~ you could
then type:
\begin{bash}
  > tar cfvz week_1_2.tgz week_1_2
\end{bash}
This will create ~week_1_2.tgz~ containing the whole ~week_1_2~
directory. You can list the contents of the ~tgz~ file to double check what
you are submitting using:
\begin{bash}
  > tar tfvz week_1_2.tgz
\end{bash}

See section \ref{sec:correct_program}\ below for guidance as to what
should be in the programs you hand in.

\subsection{Computer output}

Some questions require you to produce plots or graphs using Python. You
should print these out and submit them as hard copy. Always ensure that your
axes are labelled and that there is a legend showing what the lines
mean. The ~xlabel~, ~ylabel~ and ~legend~ Python commands are particularly
useful for this. You should also \emph{neatly}\ write a short caption
indicating what the plot shows and which exercise it answers. Do not leave
the poor demonstrator to guess why you are handing in this plot!

If you wish to embed your plots in a typed document in \LaTeX\ or a word
processor, you may do so but this is not required and it is perfectly
acceptable to neatly write on the printouts.

\subsection{Mathematical calculations and proofs}

Where a question asks you to make a mathematical calculation or a proof, you
should write out your answer showing your working. Students often have
particular difficulty working out how much detail to write down in proof
questions. You should take the proof of theorem
\ref{thm:Oadd}\ as an example of a good proof answer. 

\section{Components of a correct program}\label{sec:correct_program}

\subsection{Interfaces}

Software is comprised of components which have to work together. This is
achieved by strictly sticking to specified interfaces. Coursework questions
will specify module and function names and interfaces. They may also specify
particular exceptions which are to be raised in the event of erroneous input
or the failure of an algorithm.

\subsection{Comments and style}

A program is not just a series of instructions for a computer, it's also a
medium for communicating your ideas to the next programmer. You are expected
to provide a docstring for every function describing what that function
does. In addition, your code should be commented to a standard that it
would be immediately obvious to another student what each line of the
program does. Be careful, though, over-commenting can obscure the meaning
of a program just as much as too few comments.

A consistent and clear programming style is also essential to
readability. Ask yourself do the variable names make sense? Is the indentation
consistent? Does the way in which the program is split into modules and
functions aid or obscure understanding?

\subsection{Functionality}

Ensure that you carefully read the question and that your program does
exactly what is required of it. Use test data to ensure the right answer
and, where available, test your program against the ``official'' version
in Numpy or Scitools.

\section{Plagiarism}

You are encouraged to read widely online and to help each other in solving
coding problems, but directly copying code from other sources without
attribution is plagiarism just like copying text. It's also surprisingly
easy to tell when code has been copied: an experienced programmer will see
differences of coding style just as easily as differences of writing style in
text.

We may also ask you to explain what a particular line of your code does!

Remember, the coursework is not assessable so you gain precisely no marks by
cheating. The in-class programming exercise, however, \emph{is}\ assessed so
\emph{you}\ need to be able to write programs by yourself to pass this
module.

\section{Misuse of computers during lectures}

It is an unfair distraction to the students around you if you are web
surfing, checking email or browsing social media during the
lectures. Students caught engaging in this behaviour will be asked to leave
the lecture.

\section{Notation} 

These notes contain many Python programmes and snippets of Python code. In
common with the course text ``A Primer on Scientific Programming with
Python'' by Hans Petter Langtangen, Python code is placed on a pale blue
background with complete programmes distinguished by a coloured bar at the
left. For example, the famous one-line program with which all texts start is
rendered:
\begin{pythonprogram}
  #!/usr/bin/env python
  print 'Hello World!'
\end{pythonprogram}

To avoid endless repetition, the initial line ~#!/usr/bin/env python~ will
be omitted in the examples through the text. Occasionally, examples of input
on the Linux command line will be given. These are distinguished by a grey
rather than a blue background. For example:
\begin{bash}
   > a2ps foo.py
\end{bash}


\section{Extension material*}

Some parts of these notes extend the basic course material. Extension material
will be marked with an asterisk (*). These sections are present for the
benefit of those students who are interested in taking the module a little further, but
don't worry, they won't be on the exam! There are a few exercises marked
with a double asterisk (**). This is particularly challenging material to be
attempted by the very brave.
