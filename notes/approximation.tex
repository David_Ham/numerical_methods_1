\lecture{Function approximation and big \O notation.}

\section{Approximating functions}

Now that we have covered the basic arithmetic operations, we can move on to
consider functions. For the purposes of this module, we will only be
interested in functions of one variable. The multi-variable case is more
complex but the same principles apply. So, suppose we have a function
$y=f(x)$. This function contains infinite information, for it takes a value
$y$ for each of an infinite set of inputs $x$. On a finite computer, this is
a problem. Faced with the same problem for real numbers, we adopted the
floating point system in which real arithmetic is approximated by a finite
set of numbers. 

We can adopt a similar approach for functions. First, we assume that we are
only interested in the values of a function over some finite interval
$[a,b]$. For most scientific purposes this is a completely reasonable
assumption. Next we divide the interval into $n$ equal-sized intervals with
width $h=(b-a)/n$. As a first approximation, we could take the value of the
function over each interval to be the value which the function takes at the
left-hand end of that interval. Figure \ref{fig:constant1}\ illustrates this
approach for the function:
\begin{equation}
  f(x)=\sin(x)
\end{equation}
on the interval $[0,\pi]$. Remember that we wanted to approximate our
function at infinitely many points by a function which only requires finite
storage on a computer. This approximation achieves this: we only need to
record the five $x$ values corresponding to the ends of the intervals and
the four $f(x)$ values. 
\begin{figure}[ht]
  \centering
  \includegraphics{functions/piecewise_constant_4}
  \caption{Piecewise constant approximation to the function $\sin(x)$ using
    four equal intervals. The approximate function is shown in blue, the
    actual function in black. The blue dots show the left-hand end of each
    interval and the shaded area indicates the error in the approximation.}
  \label{fig:constant1}
\end{figure}

\subsection{Plotting piecewise constant approximations}

We can write a very short Python function which will plot a piecewise
approximation to given function over a given interval using a given number
of equal sub-intervals:

\begin{pythonsnippet}
  def plot_constant(f, a, b, n):
    '''plot_constant(f, a, b, n)

    Plot a piecewise constant approximation to f over the 
    interval [a,b] using n equal width intervals.'''
    # Import the plotting system.
    import pylab

    # Calculate the width of one interval. 
    # Cast (b-a) to float to prevent integer division.
    h=float(b-a)/n

    # Create a new figure.
    fig=pylab.figure()

    # Plot each interval.
    for i in range(n):
        x=(h*i, h*(i+1))
        y=(f(x[0]), f(x[0]))
        pylab.plot(x,y,'b')

    # Expand the margins by 10% in each direction so we can see 
    # the edge lines.
    pylab.margins(0.1)
\end{pythonsnippet}

The function is available in ~/numerical-methods-1/approximation.py~. Copy
this file to your working directory and run the function by executing
~ipython -pylab~ on the command line and then typing:
\begin{pythonsnippet}
  In [1]: from approximation import *

  In [2]: def f(x):
   ...:     return sin(x)
   ...: 

  In [3]: plot_constant(f, 0, pi, 4)
\end{pythonsnippet}

The ~approximation~ module also contains the function
~plot_constant_error~, which produces plots similar to figure \ref{fig:constant1}.

\begin{exercise}
  Write a short Python script named ~plot_x_squared.py~ which uses \linebreak~approximation.plot_constant_error~ to produce
  plots of approximations to the function $x^2$ on the interval $[0,1]$ with
  $n=2,3,4,5$. Your script should be run by running ~ipython -pylab~ and
  then typing:
  \begin{pythonsnippet}
    execfile("plot_x_squared.py")
  \end{pythonsnippet}
  \emph{Hint:} your script could have the following form:
  \begin{enumerate}
  \item import the approximation module.
  \item define a function which returns $x^2$.
  \item loop over the list [2,3,4,5] calling
    ~approximation.plot_constant_error~ to produce the actual plots.
  \end{enumerate}
  You should submit ~plot_x_squared.py~ online and in hard copy. There is no
  need to submit printouts of the plots.
\end{exercise}

\section{Better approximations}\label{sec:betterapprox}

Figure \ref{fig:constant1}\ is actually quite a bad approximation to
$\sin(x)$. We can see this both because our piecewise constant approximation
doesn't look much like a sine curve and also because the error, the area in
blue, is large compared with the area under the curve. So what can we do
about this? The first answer is that we can use more intervals: the smaller
the intervals, the smaller the error. The second answer is that we can
use a better approximation to the line than a series of horizontal
lines. The approximation which is one step up from a horizontal line is to
take the value at \emph{each}\ end of each interval and draw a sloped line
between them. Figure \ref{fig:approximations}\ shows approximations to
$\sin(x)$ using piecewise constant and piecewise linear functions with
various numbers of sub-intervals. 
\begin{figure}[htp]
  \centering
  \includegraphics{functions/approximations.pdf}
  \caption{Piecewise constant (left) and piecewise linear (right) approximations to the function $\sin(x)$. Notice that the error is far lower for a given number of intervals in the linear approximation.}
  \label{fig:approximations}
\end{figure}

Two things are apparent by observation. The first is that the greater the
number of intervals, the smaller the visible error. The second is that the
piecewise linear approximation has much lower error than the piecewise
constant approximation. We can make this a bit more formal by actually
writing down the error we have made. The error is the area between the
actual function and our approximate function\footnote{There are different
  sorts of error we could measure. For example we might be interested in the
  error in $f(x_0)$ for a given fixed $x_0$. The error we're using here measures
  the error over the whole interval $[a,b]$, we'll meet some other errors
  later.}. If we write $\fhat(x)$ for the
approximate function, then we know from secondary school calculus that:
\begin{equation}
  E=\int_a^b|f(x)-\fhat(x)|\dx
\end{equation}
To actually calculate this integral, we can split it into a sum of
integrals, one over each sub-interval:
\begin{equation}
  E=\sum_{i=0}^{n-1}\int_{a+hi}^{a+h(i+1)}|f(x)-\fhat(x)|\dx \qquad h=\frac{b-a}{n}
\end{equation}
Note that $x=a+ih$ is the left hand end of the $i$-th interval while
$x=a+(i+1)h$ is the right hand end of that interval.

This makes our life a little easier since we can write an explicit
expression for $\fhat$ on each sub-interval since it's just constant:
\begin{equation}
  \fhat(x)=f(ih), \qquad a+ih\leq x<a+(i+1)h
\end{equation}
We could now substitute in $f(x)=\sin(x)$ and attempt to work out this
integral by hand, but actually it's easier to just use a Python routine
provided the ~scipy.integrate~ module to integrate this expression numerically:
\begin{pythonsnippet}
  def constant_error(f, a, b, n):
    '''Return the integral of the error when f is approximated
    over the interval [a,b] by n equal width piecewise constant 
    functions.'''
    import scipy.integrate as si
   
    error=0
    h=float(b-a)/n

    for i in range(n):
        l=a+i*h
        r=a+(i+1)*h

        def f_err(x):
          return abs(f(x)-f(l))
          
        # si.quad is the numerical integration routine.
        error+=si.quad(f_err,l,r)[0]
    
    return error
\end{pythonsnippet}
 
We can do the same calculation for the error in the piecewise linear
case. This time we need to use the equation for a straight line
$y=y_0+m(x-x_0)$ where $m$ is the gradient of our line and $(x_0,y_0)$ is
our starting point. Our starting point is $(a+ih, f(a+ih))$ and we can calculate
the gradient of a straight line using its two end points:
\begin{equation}\label{eq:linearapprox}
  \fhat(x)=f(a+ih)+(x-(a+ih))\frac{f\left(a+(i+1)h\right)-f(a+ih)}{h}, \quad a+ih\leq x<a+(i+1)h
\end{equation}
This yields a very similar error function for the piecewise linear case:
\begin{pythonsnippet}
  def linear_error(f, a, b, n):
    '''Return the integral of the error when f is approximated 
    over the interval [a,b] by n equal width piecewise linear 
    functions.''' 
    import scipy.integrate as si   
    error=0
    h=float(b-a)/n

    for i in range(n):
        l=a+i*h
        r=a+(i+1)*h
        dy=f(r)-f(l)
        def f_err(x):
            x_bar=(x-l)
            return abs(f(x)-(f(l)+x_bar*dy/h))

        error+=si.quad(f_err,l,r)[0]
    
    return error
\end{pythonsnippet}

By evaluating those two functions for various numbers of sub-intervals, we
can calculate how the error in each approximation decreases as the interval
width falls. Figure \ref{fig:approx_error}\ shows this for our usual example
of $\sin(x)$.

\begin{figure}[ht]
  \centering
  \includegraphics{functions/approx_convergence.pdf}

  \caption{Error in piecewise constant and piecewise linear approximations of $\sin(x)$ on the interval $[0,\pi]$ as a function of sub-interval width $h$.}
  \label{fig:approx_error}
\end{figure}

Figure \ref{fig:approx_error}\ accords with our observations that error
falls with $h$ and that the piecewise linear approximation has much lower
error for a given $h$ than does the piecewise constant
approximation. However something even more interesting is apparent if we
look at the \emph{shape}\ of each of the two curves. As $h$ becomes small,
the error in the piecewise constant starts to look like a straight line. For
small $h$, the piecewise linear approximation is curved, perhaps more like a
quadratic function. 

Understanding this behaviour is very important, because in general we do
\emph{not}\ know an expression for the function we are approximating or the
value of the error. However if we know that the error is linear in $h$, then
if we double out number of sub-intervals then the error will halve. If we
know that the error is quadratic in $h$ then doubling the number of
sub-intervals will cause the error to reduce by a factor of 4, which is much better.

\section{Big \O notation}

Because the study of the scaling of functions is so important, there is a
special set of notation which has grown up to express it. 

\begin{define}\label{def:bigO}
  A function $f(x)$ is $\O(\alpha(x))$ as $x\rightarrow a$ if there exists
  some positive constant $c$ such that for any choice of $x$ sufficiently
  close to $a$:
  \begin{equation*}
    |f(x)|<c|\alpha(x)|
  \end{equation*}
\end{define}

Most often, $\alpha$ will be $x^n$ for some power $n$. 

Big $\O$ notation gives an \emph{upper bound}\ on the \emph{magnitude}\ of a function. This is useful
in two important contexts, we want to know that the error in a calculation
gets smaller at a particular rate as we use smaller steps. We also often
want to know that the amount of work, for example the number of floating
point operations, will not increase faster than a particular rate as we
increase the problem size. At the moment we're concerned with the former of
these: showing that the error is small.

We're interested in particular in the case were the error $E(h)$ is
$\O(h^n)$ as $h\rightarrow 0$. One critical result to notice is that $h^n$
approaches zero faster for higher values of $n$. The symbol \O is usually
pronounced ``order'', so if $f$ is $\O(h^2)$, we would say ``$f$ is order
$h$ squared''.

The following program plots $x^n$ for increasing $n$ demonstrating this point:

\begin{pythonprogram}
  import pylab 
  import time

  # 101 evenly spaced points in [0,1]
  x=pylab.linspace(0,1,101)

  for i in range(20):
    pylab.plot(x, x**(i+1))
    pylab.draw() # Force plot update.
    time.sleep(0.3)
\end{pythonprogram}

To actually see the animation, run ~ipython -pylab~ and run this program in
that context.

The consequence of this result is the following:

\begin{theorem}
  If $E(h)$ is $\O(h^n)$ as $h\rightarrow 0$ then $E(h)$ is $\O(h^m)$ as
  $h\rightarrow 0$ for all $m>0$ less than $n$.
\end{theorem}

When we talk about \emph{the}\ order of convergence of a numerical method,
we mean the maximum value of $n$ for which $E(h)$ is $\O(h^n)$ as
$h\rightarrow 0$. If we are talking about errors, we are always
interested in the case where $h\rightarrow 0$ so it is usual to leave this
out and just say $E(h)$ is $\O(h^n)$.

Another very important, if counterintuitive, result is the addition rule for
\O:
\begin{theorem}\label{thm:Oadd}
  If $E(h)$ is $\O(h^n)$ and $F(h)$ is $\O(h^n)$ then $E(h)+F(h)$ is
  $\O(h^n)$.
\end{theorem}

\textbf{Proof:} By the definition of \O, we have:
\begin{gather}
  |E(h)|<c_1|h^n|\\
  |F(h)|<c_2|h^n|
\end{gather}
for some $c_1>0$, some $c_2>0$ and for $h$ sufficiently close to
zero. Adding these two inequalities, we have:
\begin{equation}
  |E(h)|+|F(h)|<(c_1+c_2)|h^n|
\end{equation}
But we also know\footnote{This result is known as the triangle
  inequality. To see that it is true note that where $E(h)$ and $F(h)$ have the
  same sign, $|E(h)+F(h)|=|E(h)|+|F(h)|$. However where $E(h)$ and $F(h)$
  different sign, they partially cancel so $|E(h)+F(h)|<|E(h)|+|F(h)|$} that:
\begin{equation}
  |E(h)+F(h)|\leq|E(h)|+|F(h)|
\end{equation}
So if we write $c_3=c_1+c_2$ then we have:
\begin{equation}
  |E(h)+F(h)|<c_3|h^n|  
\end{equation}
with $c_3>0$ for $h$ sufficiently close to zero. Hence by the definition of
$\O$, $E(h)+F(h)$ is $\O(h^n)$. 

\hfill{}Q.E.D.

\begin{exercise}\label{ex:odivision}
  Suppose that $E(h)$ is $\O(h^n)$. If we define:
  \begin{equation}
    F(h)=E(h)/h
  \end{equation}
  prove that $F(h)$ is $\O(h^{n-1})$
\end{exercise}

\subsection{Numerically estimating the order of convergence}

Suppose our error $E(h)$ is actually converging at approximately the rate of
$h^n$. Then we could write:
\begin{equation}
  |E(h)|\approx c |h^n|
\end{equation}
For some unknown constant $c$. Suppose we know the error in our method for
two resolutions, $h_1$ and $h_2$, then we can write:
\begin{gather}
  |E(h_1)|\approx c |h_1^n|\\
  |E(h_2)|\approx c |h_2^n|.
\end{gather}
Equivalently:
\begin{gather}
  \left|\frac{E(h_1)}{E(h_2)}\right|\approx \left|\frac{h_1}{h_2}\right|^n
\end{gather}
By taking logarithms, we can find an expression for $n$:
\begin{gather}
  \ln\left(\left|\frac{E(h_1)}{E(h_2)}\right|\right)\approx n\ln\left(\left|\frac{h_1}{h_2}\right|\right)\\
  n\approx\frac{\ln\left(\left|\frac{E(h_1)}{E(h_2)}\right|\right)}
  {\ln\left(\left|\frac{h_1}{h_2}\right|\right)}\label{eq:bigOconv}
\end{gather}

Suppose now that we have a sequence of error values for decreasing $h$. If
the order of convergence of our method is $n$ then we would expect the right
hand side of equation \eqref{eq:bigOconv}\ to converge to $n$ as we move
along the sequence.

Turning this statement around, if we have a sequence of error values for
decreasing $h$ then we can estimate the order of convergence by evaluating
equation \eqref{eq:bigOconv}\ for each adjacent pair of $h$ and $E$ values.

\begin{exercise}\label{ex:convergence}
  Write a Python module ~convergence~ containing a function
  ~convergence(h,s)~ which takes a sequence of $h$ values and a matching
  sequence of error values $s$ and returns a sequence of estimates of
  convergence orders using equation \eqref{eq:bigOconv}.

  Copy ~/numerical-methods-1/test_convergence.py~ to your working
  directory. You can run the same test of your code that the demonstrators
  will use by running:
  \begin{bash}
  > python test_convergence.py
  \end{bash}
  You can also test your function by hand by running ~ipython~ and typing:
\begin{pythonsnippet}
  import convergence
  from test_convergence import S1,S2,h
  convergence.convergence(h,S1)
  convergence.convergence(h,S2)
\end{pythonsnippet}
In the first case, your ~convergence~ function should return a sequence
which converges towards 1.0, in the second case your sequence should
converge towards 2.0.
\end{exercise}

