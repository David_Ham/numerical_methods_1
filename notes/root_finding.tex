\lecture{Root finding}

One of the most common mathematical tasks we encounter is the need to solve
equations. That is to say, for some function $f(x)$ and a value $b$, we wish
to know for which $x$ it is true that $f(x)=b$. 

In fact, this problem can be reduced to the special case of finding the
values of $x$ for which a given function takes the value zero. Suppose we
wish to solve $f(x)=b$. We can simply define a new function $g(x)=f(x)-b$
with the result that our problem is now to solve $g(x)=0$.

Quite a lot of the algebra which you learned at school is directed to
solving this problem for particular sorts of functions. For example, if $g(x)$
is of the form $ax^2+bx+c$ then the quadratic formula can be used to solve
$g(x)=0$. However for some functions an algebraic solution may be difficult
to find or may not even exist, or we might not have an algebraic
representation of our function at all! In these cases, how do we solve the
equation?

\section{The bisection method}

The simplest root-finding algorithm is the bisection method. It relies on
the fact that, for a continuous function $f$, if $f(x_0)<0$ and $f(x_1)>0$
then $f(x')=0$ for some $x'$ in the interval $(x_1,x_2)$.
\begin{figure}[ht]
  \centering
  \includegraphics{root_finding/bolzano.pdf}
  \caption{The Bolzano theorem: if $f(x_0)<0$ and $f(x_1)>0$ then there must
  exist a point $x'$ such that $f(x')=0$}
  \label{fig:bolzano}
\end{figure}
This seemingly obvious result is known as the Bolzano theorem after the
18th-19th century Bohemian mathematician Bernhard Placidus Johann Nepomuk
Bolzano. It is a special case of the intermediate value theorem.

The basic idea of the algorithm is that we know that the root lies in the
interval $[x_l,x_r]$. We can now successively halve this interval and check in
which half the root now lies. We keep the half of the interval in which the
root is to be found and discard the other half. By repeating this operation,
we obtain successively smaller intervals which contain the root. Figure
\ref{fig:bisection} illustrates the application of the bisection method.
\begin{figure}[ht]
  \centering
  \includegraphics{root_finding/bisection.pdf}
  \caption{The bisection method finds the root by successively halving an
    interval known to contain that root. The process starts with the
    interval $[x_0,x_1]$; the subsequent $x_n$ are the results of the method.}
  \label{fig:bisection}
\end{figure}

\subsection{Implementing the bisection method}

We can write out the bisection algorithm using a notation known as
\emph{pseudocode}. Pseudocode is a mechanism for writing out sequences of
mathematical operations without writing the particular syntactic details of
a given programming language. Assume we have $x_l$ and $x_r$ such that
$f(x_l)f(x_r)<0$. That is to say, $f(x_l)$ and $f(x_r)$ have different
signs. The bisection method is then:
\begin{algorithmic}
  \STATE $x_c\gets \frac{x_l+x_r}{2}$
  \IF{$f(x_c)f(x_l)>0$}
  \STATE $x_l\gets x_c$
  \ELSE
  \STATE $x_r\gets x_c$
  \ENDIF
\end{algorithmic}
The only symbol here which we're not already familiar with is $\gets$ which
is pseudocode for assigment. In Python, that's the same as $=$. 

The algorithm above is just one step of the bisection method. How many steps
should we take? The usual answer is that we will accept an approximate root
$x'$ if $|f(x')|<\epsilon$ for some small $\epsilon$ which we specify. We
can therefore expand our algorithm to include multiple steps and a stopping
criterion:

\parbox{\textwidth}{
\begin{algorithmic}
  \REPEAT
  \STATE $x_c\gets \frac{x_l+x_r}{2}$
  \IF{$f(x_c)f(x_l)>0$}
  \STATE $x_l\gets x_c$
  \ELSE
  \STATE $x_r\gets x_c$
  \ENDIF
  \UNTIL{$|f(x_c)|<\epsilon$}
\end{algorithmic}}

Note that the pseudocode explains the steps to be taken by an algorithm but
doesn't necessarily line up one for one with statements in a particular
language. For example, the repeat\ldots until construct needs to be
translated to an appropriate Python loop.

The algorithm so far evaluates $f(x_l)$ on every step. This is fine if $f$
is a simple function which is cheap to evaluate, but $f$ might just as well
be a huge and complex piece of code with a massive computation cost. We
might therefore choose to introduce some temporary variables to prevent
unnecessary re-evaluation of $f$:

\begin{algorithmic}
  \STATE $f_l\gets f(x_l)$
  \REPEAT
  \STATE $x_c\gets \frac{x_l+x_r}{2}$
  \STATE $f_c\gets f(x_c)$
  \IF{$f_cf_l>0$}
  \STATE $x_l\gets x_c$
  \STATE $f_l\gets f_c$
  \ELSE
  \STATE $x_r\gets x_c$
  \ENDIF
  \UNTIL{$|f(x_c)|<\epsilon$}
\end{algorithmic}

\begin{exercise}
  Create a Python module ~root_finding~ containing a function\linebreak
  ~bisection(f,x_0,x_1,epsilon,record)~. This function should solve $f(x)=0$ starting
  with the interval $[x_0,x_1]$ and applying the bisection method using
  ~epsilon~ as the convergence threshold $\epsilon$. If ~record~ is ~False~,
  then the function should return the final $x_c$: the best estimate of the
  root. If ~record~ is ~True~ then the function should return a sequence of
  pairs $(x_c,f(x_c))$ constructed from each successive estimate of the
  root. The default value of ~record~ should be ~False~.

  If if $f(x_0)$ and $f(x_1)$ have the same sign, your function should
  raise ~ValueError~ with an appropriate error message. If $\epsilon\leq0$,
  your function should also raise ~ValueError~, but with a message
  appropriate to that error.

  You can test your function using ~test_root_finding.py~ from
  ~/numerical-methods-1~ by typing:
  \begin{bash}
  > python test_root_finding.py bisection
  \end{bash}
\end{exercise}

\section{Iterative algorithms}

The bisection method is an example of an \emph{iterative algorithm}. That
is to say, it is a process which produces a series of better and better
approximations to the correct answer which converge to the exact answer as
the sequence becomes infinitely long. We might question the utility of an
algorithm which might only produce the right answer if we run it for
ever. However in reality, almost no quantities in science are known exactly:
if there is already error in the input data and rounding error in the
calculations then any answer we give will by necessity be
approximate. Iterative methods can be a very powerful tool in very quickly
producing an approximate answer to a given accuracy.

\section{Newton's method}

Newton's method, also known as Newton-Raphson iteration is an iterative
root-finding algorithm which uses the derivative of the function to provide
more information about where to search for the root. We can derive Newton's
method using the first two terms of the Taylor series for a function $f$
starting at our initial guess $x_1$:
\begin{equation}
  f(x_1+h)=f(x_1)+hf'(x_1)+\O(h^2)
\end{equation}
We wish to know what we need to set $h$ to so that $f(x_1+h)=0$ so we impose
this condition and solve for $h$:
\begin{equation}
  h\approx-\frac{f(x_1)}{f'(x_1)}
\end{equation}
To form an iterative method, we use $h$ to update the last estimate of the
root:
\begin{align}\label{h_newton}
  x_{n+1}&=x_n+h\\
  &=x_n-\frac{f(x_n)}{f'(x_n)}
\end{align}
Figure \ref{fig:newton}\ illustrates the application of this method. It is
noticable that $x_2$ is far closer to the actual root than even $x_5$ for
the bisection method on the same problem.
\begin{figure}[t]
  \centering
  \includegraphics{root_finding/newton.pdf}
  \caption{Newton's method finds the root by approximating $f(x)$ by a
    straight line at each iteration.}
  \label{fig:newton}
\end{figure}

\subsection{Implementing Newton's method}

Newton's method may be written in pseudocode as:
\begin{algorithmic}
  \REPEAT
  \STATE $f_x\gets f(x)$
  \STATE $x\gets x - \frac{f_x}{f'(x)}$
  \UNTIL{$(|f_x|<\epsilon)$ or maximum iterations exceeded. }  
\end{algorithmic}
Once again we have used a temporary variable $f_x$ to avoid evaluating
$f(x)$ too many times. Note that we have included a maximum iteration count
since Newton's method is not guaranteed to converge.

\begin{exercise}
  Add a function ~newton(f, dfdx, x_0, epsilon, N, record)~ to your
  ~root_finding~ module. The arguments have the same meaning as those for
  the ~bisection~ function with the addition of ~dfdx~, which should be the
  function which is the first derivative of ~f~, and ~N~, which is the
  maximum permitted number of iterations. The default value of ~N~ should be
  $100$ and your function should raise ~ArithmeticError~ with an appropriate
  error message if this number of iterations is exceded. The output should
  depend on ~record~ in exactly the same manner as that of ~bisection~. 

  Test your implementation using the ~test_root_finding.py~ script, passing
  ~newton~ as the argument instead of ~bisection~
\end{exercise}

\begin{exercise}\label{ex:secant}
  Newton's method requires the derivative of the function to be
  known. Instead, we can use the last two iterations to
  approximate the derivative: 

  \begin{equation*}
    f'(x_{n})\approx\frac{f(x_{n})-f(x_{n-1})}{x_{n}-x_{n-1}}
  \end{equation*}
  This leads to the secant method:
  \begin{equation*}
    x_{n+1}=x_{n} - f(x_{n})\left(\frac{x_{n}-x_{n-1}}{f(x_{n})-f(x_{n-1})}\right)
  \end{equation*}
  Write a function ~secant(f, x0, x1, epsilon, N, record)~ which uses the
  secant method to solve $f(x)=0$. The function arguments should have the
  same meaning as in the ~newton~ function except that ~x0~ and ~x1~ are the
  two starting points and, of course, there is no need for ~dfdx~. 

  Your secant function can be tested using the ~test_root_finding.py~ script
  with an argument of ~secant~.
\end{exercise}

\section{Rates of convergence}

We now have three different root finding algorithms: the bisection method,
Newton's method and the secant method from exercise \ref{ex:secant}. Which
of these should we choose if we need to find a root? Part of the answer is
that we might wish to choose the method which converges in the smallest
number of iterations. In other words we want to study the rate of
convergence. 

The measure of convergence relevant to sequences of iterations is called
$Q$-convergence because it is calculated from the quotient of adjacent
sequence iterations.

\begin{define}
 A sequence ${x_k}$ converges to $x_*$ with Q-convergence order $p$
 if:
 \begin{equation}
   \lim_{k\rightarrow\infty} \frac{|x_{k+1}-x_*|}{|x_{k}-x_*|^p}<1
 \end{equation}
\end{define}

For example, if a sequence converges Q-quadratically then for sufficiently
large $k$, the error at least squares for each iteration.  The Q-convergence
order (Q-order) of a sequence is the largest $p$ for which the sequence
exhibits Q-convergence.

Note that the convergence rate is formally achieved in the infinite
limit. What this is in essence saying is that the first iterations of the
sequence may converge at other rates but they should settle down to the
expected rate of convergence.  

We are interested in numerically estimating the order of convergence. We
assume therefore that there is a $p$ and a $\mu$ such that:
\begin{equation}
  \frac{|x_{k+1}-x_*|}{|x_{k}-x_*|^p}\approx\mu
\end{equation}
In other words we assume that by setting $k$ high enough we have
approximately converged to the limit case. Rearranging, we have:
\begin{equation}
  |x_{k+1}-x_*|\approx\mu |x_{k}-x_*|^p
\end{equation}
We can also apply the same formula to the previous iteration in the
sequence:
\begin{equation}
  |x_{k}-x_*|\approx\mu |x_{k-1}-x_*|^p
\end{equation}
By dividing these two approximate equations, we can eliminate $\mu$:
\begin{equation}
  \frac{|x_{k+1}-x_*|}{|x_{k}-x_*|}\approx\left(\frac{|x_{k}-x_*|}{|x_{k-1}-x_*|}\right)^p
\end{equation}
Recall that we're looking for an expression for $p$ so we take the logarithm
of each side:
\begin{equation}
  \log\left(\frac{|x_{k+1}-x_*|}{|x_{k}-x_*|}\right)\approx p\log\left(\frac{|x_{k}-x_*|}{|x_{k-1}-x_*|}\right)
\end{equation}
Hence:
\begin{equation}
  p\approx\frac{\log\left(\frac{|x_{k+1}-x_*|}{|x_{k}-x_*|}\right)}{\log\left(\frac{|x_{k}-x_*|}{|x_{k-1}-x_*|}\right)}
\end{equation}
This expression still assumes that we know precisely the root to which the
sequence is converging. In practice, we usually only have the sequence of
iterations we have generated. Provided our sequence is well converged, we
might choose to use the last member of the sequence to approximate the
``true'' limit value. Using the Pythonic notation of -1 to indicate the last
index in a sequence, this gives the formula:
\begin{equation}\label{eq:sequence_conv}
  p\approx\frac{\log\left(\frac{|x_{k+1}-x_{-1}|}{|x_{k}-x_{-1}|}\right)}{\log\left(\frac{|x_{k}-x_{-1}|}{|x_{k-1}-x_{-1}|}\right)}
\end{equation}

\begin{exercise}
  Add a function ~q_convergence(s)~ to your ~convergence~ module.  ~s~  is
  sequence of values. The
  return value should be a list of $p$ values calculated from the input
  according to equation \eqref{eq:sequence_conv}.

  The module ~test_q_convergence~ in ~/numerical-methods-1~ contains two
  sequences ~S1~ and ~S2~ which you can use to check your work. ~S1~
  converges at a Q-order approaching 1 while ~S2~ converges at a Q-order
  approaching 2. You can automatically apply these tests by running
  ~test_q_convergence.py~ as a script.
\end{exercise}

\begin{exercise}\label{ex:applied_sequence_conv}
  Write a script ~find_sin_root.py~ which uses each of your three root
  finding methods to solve the equation:
  \begin{equation*}
    x^5 = 1
  \end{equation*}
  For the bisection method, use $[0,1.5]$ as the starting interval. Start
  Newton's method from $x_0=0.1$ and use $x_1=0.2$ for the secant method.

  For each method, print the sequence of approximate roots
  generated. Also print estimates the order of convergence of each method on each
  function. You may need to make $\epsilon$ very small to get really good
  convergence.
\end{exercise}

\section{Breakdown of Newton's method*}

In exercise \ref{ex:applied_sequence_conv}\ you will have observed that
Newton's method converges faster than the bisection method. This more rapid
convergence comes, however, at a cost. While the bisection method is
guaranteed to converge to the root and its accuracy is limited only by the
accuracy with which the function $f$ can be evaluated, there are a number of
circumstances in which Newton's method can converge only slowly or even not
at all. 

\subsection{Slow convergence*}
At a multiple root, such as the root $x=0$ for $f(x)=x^2$, the derivative
of the function $f'(x)$ is zero. This makes the calculation of $h$ less
accurate as both the numerator and the denominator in equation
\eqref{h_newton}\ are tending to zero. The result of this is that Newton's
method converges at first order instead of its usual second order.

\begin{exercise}*
  Use Newton's method to solve $x^2=0$ with an initial condition of
  $x=1$. Calculate the rate of convergence numerically.
\end{exercise}

\subsection{Finding the wrong root*}

If a function has more than one root, then an initial guess too far from the
root may result in a very large first step which places the next iterate
closer to another root. In this case, Newton's method may well converge to a
root other than the one sought.

\begin{exercise}*
  Use Newton's method to solve $\sin x=0$ with a starting
  guess of $x=1.7$. To which answer does the sequence converge?
\end{exercise}

\subsection{Divergence*}

If a function becomes very flat as its value becomes large, then Newton's
method may set off in the wrong direction and diverge to infinity. 

\begin{exercise}* 
  Use Newton's method to attempt to solve $\arctan x = 0$
  starting at $x=1.5$. The derivative of $\arctan x$ is $1./(1.+x^2)$.
\end{exercise}

\section{Summary of root finding algorithms}

We have met three root-finding algorithms here. They illustrate trade-offs
which are common in numerical computing.

\begin{description}
\item[The bisection method] requires minimal information about the function
  and always converges, but has a slow rate of convergence.
\item[Newton's method] often converges very fast but may converge slowly or
  not at all. It also requires us to know the gradient of the function we
  are solving.`
\item[The secant method] comes between the previous two methods in speed and
  does not require us to know the gradient of the function but can break
  down in the same ways as Newton's method.
\end{description}
