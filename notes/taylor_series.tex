\lecture{Taylor series}\label{app:taylor}
\renewcommand{\thepage}{\thechapter-\arabic{page}}
\setcounter{page}{1}

\section{Power series}

A power series is a function represented by an infinite sum:
\begin{equation}
  \begin{split}
      f(x)&=a_0+a_1(x-x_0)+a_2(x-x_0)^2+\ldots+a_n(x-x_0)^n+\ldots\\
      &=\sum_{k=0}^{\infty}a_k(x-x_0)^k
  \end{split}
\end{equation}
The point $x_0$, often referred to as the \emph{centre}\ of the power series,
is fixed: it does not depend on $x$. Similarly, the coefficients $a_k$ are
independent of $x$. 

For the purposes of numerical mathematics, it is most convenient to write
power series in another form. By making the substitution $x=x_0+h$, we can
write:
\begin{equation}
  f(x_0+h)=\sum_{k=0}^{\infty}a_0h^k
\end{equation}

\section{Radius of convergence}

A power series representation of $f$ is only valid for values of $h$ for which the series
converges. It is possible to test for the convergence of a series using the
ratio test, otherwise known as d'Alembert's test after the 18th century French
mathematician Jean de Rond d'Alembert who originally published it. The test
establishes the following sufficient test for convergence:
\begin{equation}
  \lim_{n\rightarrow\infty}\left|\frac{a_{n+1}h^{n+1}}{a_nh^n}\right|<1
\end{equation}
or equivalently:
\begin{equation}
  |h|\lim_{n\rightarrow\infty}\left|\frac{a_{n+1}}{a_n}\right|<1
\end{equation}
Hence the convergence of the power series depends not only on the
coefficients $a_n$, it is also linear in the magnitude of $h$. This enables
us to define a new parameter $R$,  the radius of convergence. The
convergence criteria now becomes:
\begin{equation}
  |h|<R\quad \Longleftrightarrow\quad x_0-R<x<x_0+R
\end{equation}
where:
\begin{equation}
  R\equiv \lim_{n\rightarrow\infty}\left|\frac{a_{n}}{a_{n+1}}\right|
\end{equation}
A power series \emph{centred}\ at $x_0$ is therefore valid for values $h$
less than the radius $R$. 

The radius of convergence of a power series is preserved under addition,
subtraction, multiplication and differentiation. That is to say, if two
power series with the same radius of convergence are added, subtracted or
multiplied then the resulting power series has the same radius of
convergence. Similarly, the derivative of a power series is a power series
with the same radius of convergence.

\section{Taylor and MacLaurin Series}

Suppose we have some function $f(x)$ whose derivatives we know at some point
$x_0$. We can write:
\begin{equation}
  f(x_0+h)=a_0+a_1h+a_2h^2+\ldots+a_nh^n+\ldots  
\end{equation}
If we set $h=0$ then we note that:
\begin{equation}
  f(x_0)=a_0
\end{equation}
Differentiating the series we have:
\begin{equation}
  f'(x)=a_1+2a_2h+3a_3h^2\ldots+na_nh^{n-1}+\ldots
\end{equation}
so that:
\begin{equation}
  f'(x_0)=a_1
\end{equation}
If we differentiate again, we have:
\begin{equation}
  f''(x)=2a_2+2\times3a_3h\ldots+(n-1)\times na_nh^{n-2}+\ldots
\end{equation}
yielding:
\begin{equation}
  f''(x_0)=2a_2
\end{equation}
similarly:
\begin{equation}
  \begin{split}
    f'''(x_0)&=3\times2a_3\\
    &=3!\,a_3
  \end{split}
\end{equation}
In general:
\begin{equation}
  f^{(n)}(x_0)=n!\,a_n
\end{equation}
In other words:
\begin{equation}
  a_n=\frac{f^{(n)}(x_0)}{n!}
\end{equation}

This gives us a new way of representing a function:
\begin{equation}
  \begin{split}
    f(x_0+h)&=f(x_0)+f'(x_0)h+\frac{f''(x_0)}{2!}h^2+\ldots
    +\frac{f^{(n)}(x_0)}{n!}h^n+\ldots\\
    &=\sum_{k=0}^\infty \frac{f^{(k)}(x_0)}{k!}h^k
  \end{split}
\end{equation}
provided, of course, that the series converges. In this final formula we
employ the conventions that the zeroth derivative of a function is the
function itself ($f^{(0)}(x)=f(x)$) and that $0!=1$.

This means we have an algorithm for expressing $f(x)$ as a power series
subject to the radius of convergence and assuming that the derivatives
$f^{(n)}(x)$ are continuous within that radius of convergence.

This form of power series is known as a Taylor series.
A special case of the Taylor series is achieved by setting $x_0=0$:
\begin{equation}
  f(x)=\sum_{k=0}^\infty \frac{f^{(k)}(0)}{k!}x^k
\end{equation}
This form of Taylor series is referred to as a MacLaurin series.

\subsection{Example: $e^x$}

Expand $f(x)=e^x$ around $x=0$.

$e^x$ is unique in that:
\begin{equation}
  f(x)=f'(x)=f''(x)=f^{(n)}(x)=e^x
\end{equation}
so at $x=0$ all the derivatives are $e^0=1$. This produces the series:
\begin{equation}
  \begin{split}
    e^x&=\sum_{k=0}^{\infty} \frac{x^k}{k!}\\
    &=1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\frac{x^4}{4!}+\ldots
  \end{split}
\end{equation}

The radius of convergence for this series is:
\begin{equation}
  \begin{split}
    R&=\lim_{n\rightarrow\infty}\left|\frac{a_{n}}{a_n+1}\right|\\
    &=\lim_{n\rightarrow\infty}\left|\frac{(n+1)!}{n!}\right|\\
    &=\lim_{n\rightarrow\infty} n+1\\
    &=\infty
  \end{split}
\end{equation}
That is to say, the expansion converges for any $x$.

\subsection{Example: $\cos x$}

Expand $f(x)=\cos x$ around $x=0$.

In this case we have:
\begin{align*}
  f'(x)&=-\sin x & f''(x)&=-\cos x\\
  f'''(x)&=\sin x & f''''(x)&=\cos x\\
  f^{(5)}(x)&=-\sin x & f^{(6)}(x)&=-\cos x\\
  f^{(7)}(x)&=\sin x & f^{(8)}(x)&=\cos x
\end{align*}
So at $x=0$ all the odd derivatives are $0$ while the even derivatives
alternate between $-1$ and $1$. This produces:
\begin{equation}
  \cos x = 1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\ldots
\end{equation}
once again, the radius of convergence is $\infty$. By a similar process:
\begin{equation}
  \sin x=x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\ldots
\end{equation}

\subsection{Example: $\ln x$}

Since $\ln x$ is not defined at $x=0$, there is no MacLaurin series for this
function. Instead, we produce a Taylor series for $\ln x$ around $x_0=1$.

In this case we have the following derivatives:
\begin{gather*}
  f'(x)=\frac{1}{x}\\
  f''(x)=-\frac{1}{x^2}\\
  f'''(x)=\frac{2}{x^3}\\
  f''''(x)=\frac{3\times2}{x^4}\\
  f^{(n)}(x)=-1^{n-1}\frac{(n-1)!}{x^n}
\end{gather*}
So that at $x=1$ we have:
\begin{gather*}
  f(1)=0\\
  f'(1)=1\\
  f''(1)=-1\\
  f'''(1)=2\\
  f''''(1)=-6\\
  \vdots
\end{gather*}
This leads us to:
\begin{equation}
  \begin{split}
    \ln (1+h) &= \sum_{k=1}^{\infty} -1^{k-1}\frac{(k-1)!}{k!}h^k\\
    &=\sum_{k=1}^{\infty} -1^{k-1}\frac{h^k}{k}\\
    &=h - \frac{h^2}{2} + \frac{h^3}{3}+\ldots
  \end{split}
\end{equation}

This has radius of convergence:
\begin{equation}
  \begin{split}
    R&=\lim_{n\rightarrow\infty}\left|\frac{a_n}{a_{n+1}}\right|\\
    &=\lim_{n\rightarrow\infty}\left|\frac{n+1}{n}\right|\\
    &=1
  \end{split}
\end{equation}
So the series converges for $-1<h<1$ or $0<x<2$.