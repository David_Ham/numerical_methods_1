\lecture{Binary arithmetic}

If we are to use computers to do our arithmetic, one of the most important
details of which we must be aware is how computers store numbers and do
arithmetic on them. As in pen-and-paper mathematics, there are different
sorts of number system. For example, in the mathematical world, we are
familiar with the integers (whole numbers), rational numbers (fractions) and
real numbers (the whole of the number line), just to name a few. We know that
the properties of arithmetic operations are different too, for example
$\sqrt{2}$ cannot be evaluated in the integers or the rationals, only in the
reals. Conversely, if we have the list [``cat'', ``dog'', ``goldfish'',
``giraffe''], it makes no sense to ask ``what is in
the 1.5th place in this list''. Another way of saying ``it makes no sense to
ask'' is to say that this operation is not defined. This is an important
concept in computer arithmetic, to which we will return later. However for
this lecture, we will concentrate on integers, \emph{the whole numbers}.

\section{Binary integer arithmetic}

In the binary system, instead of digits we have binary digits, or
\emph{bits}. Whereas a decimal digit can take any one of the ten values in
the set $\{0,1,2,3,4,5,6,7,8,9\}$, a binary digit can only take the values
$0$ or $1$. All computers in current use operate on the binary system for
one simple reason: an electrical circuit exists in two basic states, off and
on. Binary arithmetic is the natural result of representing numbers as sets
of electrical circuits switching off and on.

\subsection{Counting}

So if 0 and 1 are the only digits available, how do we write the number two in
base two? The analogous question in the decimal system is ``how do you write
down the number ten''. The answer, which you have known since primary
school,  is to use successive powers of the base in
a place value system. So ten is written as 10 in base ten, where this
notation is understood to mean ``one times ten plus zero times one''.  So the
number two is written in base two as $10_2$, which means ``one times
\emph{two}\ plus zero times one''. Notice the subscript $2$ which is used to
distinguish binary numbers from decimal ones. 

We can also write $11_2$ and understand that this means ``one times two plus
one times one'', in other words, 3. What about four? This is analogous to
the number one hundred in base ten which is written 100. Four is written as
$100_2$ which is understood as ``one times two squared plus zero times two
plus zero times one''. Following this pattern, we can write the numbers from
zero to, say, nineteen in base 2:
\begin{eqnarray*}
  0=0_2&\hspace{3cm}  10=1010_2 \hspace{3cm}\\  
  1=1_2 & 11=1011_2\\
  2=10_2 & 12=1100_2\\
  3=11_2 & 13=1101_2\\
  4=100_2 & 14=1110_2\\
  5=101_2 & 15=1111_2\\
  6=110_2 & 16=10000_2\\
  7=111_2 & 17=10001_2\\
  8=1000_2 & 18=10010_2\\
  9=1001_2 & 19=10011_2\\
\end{eqnarray*}

\subsection{Addition}\label{sect:binaryadd}

The addition rules for binary numbers are directly analogous to those for
decimal numbers: when any column overflows then an extra value is carried to
the next column. For example, if we calculate two plus three, we have:
\begin{equation*}
  \begin{split}
    10_2&\\
    11_2&+\\\hline
    101_2&
  \end{split}
\end{equation*}
Notice that adding the two ones in the twos column produces zero twos plus a
carried 1 in the fours column.

\begin{exercise}
  Compute the following by first converting to binary:
  \begin{enumerate}
  \item 7+3
  \item 15+1
  \end{enumerate}
\end{exercise}

\subsection{Multiplication}\label{sect:binarymult}

Multiplication is achieved in exactly the same manner as long multiplication
for decimal numbers. Of course in binary this is a particularly easy
operation since at each step, the first operand is multiplied by either 1 or
0. For example, five times five is written:
\begin{equation*}
  \begin{split}
    101_2&\\
    101_2&\times\\\hline
    101_2&\\
    000_2\phantom{1}&\\
    101_2\phantom{01}&+\\\hline
    11001_2
  \end{split}
\end{equation*}
Notice the carry in the eights place when calculating the final summation.

\begin{exercise}
  Calculate the following by first converting to binary:
  \begin{enumerate}
  \item $5\times3$
  \item $6\times6$
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Calculate $3\times2$ and $3\times4$ in binary. What is the rule for
  multiplying by a power of 2 in binary?
\end{exercise}

\section{Some notation}

There is a standard set of notation associated with the different
mathematical number systems which it is useful to be aware of:

\begin{tabular}{ll}
  $\Integer$ & integers: the whole numbers.\\
  $\Rational$ & rationals: $\frac{a}{b}$ for $a, b \in\Integer$.\\
  $\Real$ & reals: the whole number line.\\
  $\Complex$ & complex numbers: $a+bi$ for $a,b\in \Real$ where
  $i=\sqrt{-1}$.
\end{tabular}

\section{Unsigned integers}

A feature of all of the sets of numbers in the preceding section is that
they are infinite: there are an infinite number of possible integers, and
there are an infinite number of rationals and reals even between 0 and
1. However, computers have a fixed amount of storage and are built to work
on binary numbers with a fixed size. Let's first consider integers. A computer
integer is expressed as a fixed number of binary digits, or
\emph{bits}. Let's confine ourselves to the positive numbers and zero, a set
referred to in computing as the unsigned integers. An unsigned 3 bit integer
represents numbers as follows:
\[
\begin{array}{|>{\columncolor{paleblue}}c|>{\columncolor{paleblue}}c|>{\columncolor{paleblue}}c|r}
  2^2 & 2^1 & 2^0 \\
  4 & 2 & 1 & \textrm{decimal}\\\hline
  0 & 0 & 0 & 0\\
  0 & 0 & 1 & 1\\
  0 & 1 & 0 & 2\\
  0 & 1 & 1 & 3\\
  1 & 0 & 0 & 4\\
  1 & 0 & 1 & 5\\
  1 & 1 & 0 & 6\\
  1 & 1 & 1 & 7\\
\end{array}
\]
So a 3 bit unsigned integer is only capable of representing the numbers from
0 to 7. Equivalently, an $n$ bit unsigned integer can contain the numbers
from $0$ to $2^n-1$.

\section{Signed integers}

Of course we will want to represent negative numbers too. The most
straightforward way to do this, and the solution which was common in the
early days of computing, is to simply add one bit to the start of the number
and designate 0 for + and 1 for -. In this system, our 3 bit unsigned
integer becomes a 4 bit signed integer:
\[
\begin{array}{|>{\columncolor{paleblue}}c|>{\columncolor{paleblue}}c|>{\columncolor{paleblue}}c|>{\columncolor{paleblue}}c|r}
   & 2^2 & 2^1 & 2^0 \\
  \textrm{sign} &  4 & 2 & 1 & \textrm{signed decimal}\\\hline
  1 & 1 & 1 & 1 & -7\\
  1 & 1 & 1 & 0 & -6\\
  1 & 1 & 0 & 1 & -5\\
  1 & 1 & 0 & 0 & -4\\
  1 & 0 & 1 & 1 & -3\\
  1 & 0 & 1 & 0 & -2\\
  1 & 0 & 0 & 1 & -1\\
  1 & 0 & 0 & 0 & -0\\
  0 & 0 & 0 & 0 & 0\\
  0 & 0 & 0 & 1 & 1\\
  0 & 0 & 1 & 0 & 2\\
  0 & 0 & 1 & 1 & 3\\
  0 & 1 & 0 & 0 & 4\\
  0 & 1 & 0 & 1 & 5\\
  0 & 1 & 1 & 0 & 6\\
  0 & 1 & 1 & 1 & 7\\
\end{array}
\]
We can immediately observe two peculiarities with this system. First, there
are two zeros with different signs. Second, the binary rule for counting is
different for positive and negative numbers. For positive numbers, the rule
is the rule we would expect. However if $k$ is negative then $k+1$ has to be
evaluated as $-(-k-1)$. This way of storing integers is known as \emph{sign
  magnitude}.

The approach which is taken for integers on almost all modern computer
systems is known as \emph{two's compliment}. In this case, the number -1 is
defined as the bit pattern such that adding 1 produces 0. This has the
result that -1 is represented by a bit pattern of all 1s. We can illustrate
this for a 4 bit signed
integer, as follows:

\begin{equation*}
  \begin{split}
    1111_2&\\
    1_2&+\\\hline
    \not{1}0000_2&  
  \end{split}
\end{equation*}
Note that the carried 1 in the $2^5$ column is dropped because our number
format only supports 4 columns. The 4 bit two's compliment signed integers
are:
\[
\begin{array}{|>{\columncolor{paleblue}}c|>{\columncolor{paleblue}}c|>{\columncolor{paleblue}}c|>{\columncolor{paleblue}}c|r}
   & 2^2 & 2^1 & 2^0 \\
  \textrm{sign} &  4 & 2 & 1 & \textrm{signed decimal}\\\hline
  1 & 0 & 0 & 0 & -8  \\
  1 & 0 & 0 & 1 & -7\\
  1 & 0 & 1 & 0 & -6\\
  1 & 0 & 1 & 1 & -5\\
  1 & 1 & 0 & 0 & -4\\
  1 & 1 & 0 & 1 & -3\\
  1 & 1 & 1 & 0 & -2\\
  1 & 1 & 1 & 1 & -1\\
  0 & 0 & 0 & 0 & 0\\
  0 & 0 & 0 & 1 & 1\\
  0 & 0 & 1 & 0 & 2\\
  0 & 0 & 1 & 1 & 3\\
  0 & 1 & 0 & 0 & 4\\
  0 & 1 & 0 & 1 & 5\\
  0 & 1 & 1 & 0 & 6\\
  0 & 1 & 1 & 1 & 7\\
\end{array}
\]
We can see in this representation that there is a single zero and that the
rule for counting (and therefore for addition) is the same for positive and
negative numbers. It turns out that the rule for multiplication is also
exactly the same for signed and unsigned arithmetic, but we won't cover the
details here. In fact, the machine implementation of unsigned and two's
compliment signed $n$ bit integer addition and subtraction is exactly the
same: it's only the \emph{interpretation}\ of some of the numbers as
negative which differs.

\subsection{Calculating the negative of a number}

Two's complement arithmetic also gives us an easy mechanism for finding the
bit pattern corresponding to a negative number. The hint is in the word
``complement''. The complement of a binary number is the number which
results from changing all the zeros to ones and all the ones to zeros. So in
4 bit binary, the complement of 2 ($0010_2$) is $1101_2$. If we add these
two numbers then we have $1111_2$, which is -1. If we think about this for
just a second, it becomes obvious that the sum of a number and its binary
complement is always -1. If we write $\bar{x}$ for the complement of $x$,
then:
\begin{equation*}
  x+\bar x = -1
\end{equation*}
with a little rearrangement, this becomes:
\begin{equation*}
  -x =\bar x + 1
\end{equation*}
So we now have an algorithm for finding the negative of $x$:
\begin{enumerate}
\item Compute $\bar x$ by changing all the zeros in $x$ to one and all the
  ones to zero.
\item Add one to $\bar x$.
\end{enumerate}
To gain some intuition into this result, it is useful to note that $-x$ is
the number which, when added to $x$, yields $0$. In contrast, $\bar x$ when
added to $x$ gives -1. Consequently, $-x$ must be one more than $x$.

\begin{exercise}
  Calculate $5-6$ by rewriting the problem as $5+(-6)$, and rewriting both
  operands as signed 4 bit binary integers. Next, add using the rules for
  unsigned addition from section \ref{sect:binaryadd}. Verify that your
  answer, when interpreted as a signed integer, is $-1$
\end{exercise}

\begin{exercise}
  Calculate $2\times-2$ by first converting both operands to signed
  integers, then multiply using the rules for unsigned integers from section
  \ref{sect:binarymult}. Verify that your answer, when interpreted as a
  signed integer, is $-4$.
\end{exercise}

\subsection{Determining integer size}

How do we know what is the biggest integer available on our computer? In
Python there is a special variable for this in the ~sys~ package. The
largest ordinary integer in python can be found with:

\begin{pythonprogram}
  import sys
  print sys.maxint
\end{pythonprogram}

So how big is that in bytes? We can assume that ~sys.maxint~ must be $2^{n-1}-1$
where $n$ is the number of bits in the number. So:
\begin{align*} 
  b&=2^{n-1}-1\\
  \log_2(b)&=\log_2\left(2^{n-1}-1\right)
\end{align*}
Now for large $n$, $\log_2\left(2^{n-1}-1\right)\approx \log_2\left(2^{n-1}\right)$ so:
\begin{align*} 
  \log_2(b)&\approx \log_2\left(2^{n-1}\right)\\
    &=n-1
\end{align*}
Happily, the numeric Python module ~numpy~ contains a function for taking the
base 2 logarithm of a number so we can try:
\begin{pythonprogram}
  import sys
  import numpy
  print numpy.log2(sys.maxint)
\end{pythonprogram}
On my computer, this produces ~63.0~ so my integers are 64 bit, a sensible
answer. The other answer which you may well see on a modern computer is 32
bit. 32 and 64 are the only default integer bit lengths in current use.

\subsection{Integer overflow}

So what is ~sys.maxint+1~? The answer depends on the computer system. In
Python, data types are very flexible so the answer is a larger integer type:
\begin{pythonsnippet}
  import sys
  sys.maxint+1
  Out[1]: 9223372036854775808L
\end{pythonsnippet}
Note the ~L~ at the end of the answer. This indicates a long integer, which
is a Python data type with essentially no maximum size. Long integers are
very flexible but, because they are not directly handled by the hardware,
computations with them can be very slow.

With a little trickery, we can force Python to calculate ~sys.maxint+1~ in
hardware. To do this, we use an ~array~. We will encounter arrays again
later but for now, the only thing we need to know is that the data type of
an array is fixed when it is created so Python won't perform the promotion
to long integer:
\begin{pythonsnippet}
  import sys
  import numpy
  numpy.array(sys.maxint)+1
  Out[2]: -9223372036854775808
\end{pythonsnippet}
This is the negative of the answer we expect! What has happened? Remember
that for two's compliment signed integers, the rules for addition are the
same as for unsigned integers. Let's look back to our example of 4-bit
integers. The largest signed value is 7, or $0111_2$. Let's add 1:
\begin{equation*}
  \begin{split}
    0111_2&\\
    1_2&+\\\hline
    1000_2&  
  \end{split}
\end{equation*}
As an \emph{unsigned}\ integer, $1000_2$ is 8. However, in two's
compliment it's -8. In other words, integer addition ``wraps
around''. This process is known as overflow and accounting for it is
important in any algorithm which uses large integers.


\begin{exercise}
  What is ~numpy.array(sys.maxint)*2~? Why is this the case? 
\end{exercise}
