
\lecture{Numerical linear algebra}\label{lec:linalg}

We met matrices in Maths Methods I and learned about matrix multiplication
and how to solve linear systems by means of matrices. The linear systems
encountered in science can be huge: many millions of rows and columns. It is
therefore very important to have techniques for matrix operations on the
computer. 

\section{Arrays: Python for vectors and matrices}

In chapter 4 of Langtangen, which you have already covered in Programming for
Geoscientists, you met Python ~array~ objects. These can be used to work
with vectors and matrices. To do this we need to introduce the concept of
the \emph{rank}\ of an array. To put it simply, the rank is the number of
indices which you need to describe the position of the entries in an
array. A vector is an array with rank one while a matrix is an array with rank
two\footnote{Array is a computing term. Mathematicians tend to use the word
  \emph{tensor}, which you might encounter in a later module}. 

This brings us to one of the most confusing terms in the array nomenclature:
\emph{dimension}. To a physicist, this is usually the length of a vector. However in
computing it is usually used to refer to which of the indices of an array we
are referring to. So a matrix (that is, a rank 2 array) has two dimensions
and a vector (of any length) just the one. 

The physicist's dimension is known as \emph{extent}\ in computing. An array
has an extent in each (computing) dimension. At this stage, an example might
help:

\begin{pythonsnippet}
  In [133]: from numpy import *

  In [134]: A=array([[1., 2.],[3., 4.],[5., 6.]])

  In [135]: print(A)
  [[ 1.  2.]
  [ 3.  4.]
  [ 5.  6.]]

  In [136]: rank(a)
  Out[136]: 2
\end{pythonsnippet}

~A~ is a rank 2 array (i.e. a matrix). Its extent in dimension 0 is 3 and
its extent in dimension 1 is 2. The vector composed of the extent of an
array in each dimension is called its \emph{shape}. To return to our
example:

\begin{pythonsnippet}
  In [157]: A.shape
  Out[157]: (3, 2)
\end{pythonsnippet}

\subsection{Array notation and counting from zero}

It is a widely used convention in mathematics that matrices and vectors are
typeset with particular character types and weights. This makes formulae
easier to read as it is possible to see the type of a symbol from its
typography. We will use upright capital letters for matrices (for example
$\mat A$). Vectors will be given by bold lower case letters ($\vec
a$). There are two common uses of subscripts, and this can lead to some
confusion. A bold lower case letter with a subscript, such as $\vec a_i$
indicates the $i$-th member of of a set of vectors. We might write the whole
set as $\{\vec a_i\}$. A subscript can also be used to indicate the $i$-th
component of a vector. This is, naturally, a scalar so we write this in
italic: $a_i$ is the $i$-th component of the vector $\vec a$.

Python arrays (as well as lists, tuples and everything else in Python) are
enumerated from 0. This means that the first entry in a vector ~x~ is
~x[0]~. This is a cause of potential confusion as mathematicians often
number vectors and matrices from 1. To try to avoid this confusion, in this
course we will generally number from zero both in mathematical notation
($x_0$) and Python syntax (~x[0]~)


\subsection{Slicing and dicing}

List slices were introduced in Langtangen section 2.1.9. They were extended
to arrays in section 4.2.2 and to multidimensional arrays in section 4.6. 

As a brief reminder, the colon is used to indicate a range of
values. Suppose we have a matrix ~A~. The first column of
~A~ is given by ~A[:,0]~ while the last row of of ~A~ is ~A[-1,:]~. 

Slices can also extend over part of a dimension. The slice can be thought of
as starting \emph{before}\ the number given. So if ~x~ is a vector then
~x[0:]~ is the whole vector but ~x[:0]~ is a zero length vector containing
nothing at all!

\subsection{Creating arrays}

There are many ways to create new array objects in Python but two are
particularly important. One, which we met above, is to cast a sequence to an
array:
\begin{pythonsnippet}
  array([1.,2.,3.])
\end{pythonsnippet}
A rank 2 array can be created by casting a sequence of sequences:
\begin{pythonsnippet}
  array([[1.,2.],[3.,4.]])
\end{pythonsnippet}
In this case, the outermost sequence corresponds to the first dimension of
the array. This means that the inner sequences correspond to the rows of a
matrix, which is probably what you expect.

The second important mechanism is using the ~numpy.zeros~ function. This
function returns an array with value 0.0 in every element and with shape
given by the argument. For example, to create a 3-vector call:
\begin{pythonsnippet}
  zeros(3)
\end{pythonsnippet}
or to create a matrix with 3 rows and 4 columns, call:
\begin{pythonsnippet}
  zeros((3,4))
\end{pythonsnippet}
Note the second set of brackets: ~zeros~ takes exactly one argument but that
argument can be a scalar or a (rank 1) sequence.

Once an array of zeros has been created, the entries can be
populated by assigning singly or to slices.


\section{Dot products and matrix multiplication}

Let's think about the dot product and matrix multiplication operators which
we met in maths methods 1. Assume that $\mat A$ and $\mat B$ are matrices
and $\vec x$ and $\vec y$ are vectors and for simplicity lets assume that
the matrices are square and the extent of both the matrices and the vectors
is $n$. Then we can write down the following mathematical identities:
\begin{gather}
  \mat A\vec y = \sum_{i=0}^{n-1} \mat A_{:,i}  y_i\\
  \vec x\cdot \vec y = \sum_{i=0}^{n-1}  x_i  y_i\\
  \vec x\mat A = \sum_{i=0}^{n-1}  x_i \mat A_{i,:}\\
  \mat A\mat B = \sum_{i=0}^{n-1} \mat A_{:,i} \mat B_{i,:}
\end{gather}
The similarity between these statements is striking. In fact, the dot
product and matrix multiplication are really the same operator. If ~C~ and
~D~ are arrays then we multiply them by writing ~numpy.dot(C,D)~. The answer
is achieved by summing over the last dimension of ~C~ and the first
dimension of ~D~. The operation is only defined if the extents of ~C~ and
~D~ match along these dimensions and an exception will be raised if they do
not.

From this definition we can draw some other conclusions. The first is that:
\begin{pythonsnippet}
  rank(dot(C,D)) == rank(C) + rank(D) - 2
\end{pythonsnippet}
What does this mean if ~C~ and ~D~ are both vectors? Well vectors have rank
1 so this statement says that the rank of the dot product of two vectors is
zero. This is correct. You can think of a scalar value as being a rank zero
array. Python even agrees:
\begin{pythonsnippet}
  In [163]: rank(1.0)
  Out[163]: 0
\end{pythonsnippet}

\section{Linear systems}

One of the most important classes of problems to be able to solve is the
matrix equation:
\begin{equation}
  \mat{A}\vec{x}=\vec{b}
\end{equation}
in which $\mat{A}$ is known square matrix, $\vec{b}$ is a known vector and
$\vec{x}$ is an unknown vector. Before we move on to methods for solving
this sort of system, we'll spend some time on what this equation means and
trying to understand the question it poses.

\section{Span and linear independence}

Let's look at the matrix $\A$ in a slightly different way. Rather
than thinking of $\A$ as a set of row vectors, let's think of a as a
collection of columns:
\begin{equation}
  \A=
  \begin{bmatrix}
    \A_{:,0}&\rule[-4ex]{1pt}{9ex}&
    \A_{:,1}&\rule[-4ex]{1pt}{9ex}&
    \cdots&\rule[-4ex]{1pt}{9ex}&
    \A_{:,n}\\
  \end{bmatrix}
\end{equation}
the matrix product $\A\x$ can then be thought of as a weighted sum of
the columns of $\A$:
\begin{equation}
  \A\x=x_0\A_{:,0}+x_1\A_{:,1} +\cdots+x_n\A_{:,1}
\end{equation}
This is also described as a \emph{linear combination}\ of the columns of
$\A$.

What form to linear combinations of vectors take? If we have a single vector
$\vec a$ then the linear combinations of that vector ($x\vec a$) are simply
the line passing through that vector. Figure \ref{fig:1dspan}\ illustrates this.

\begin{figure}[ht]
  \centering
  \input{figures/linear_1_pdf.tex}  
  \caption{The span of a single vector, $\vec a$, is the set $x\vec a$ for all $x\in\Real$. }
  \label{fig:1dspan}
\end{figure}
\begin{center}

\end{center}
If we have a set of vectors $\{\vec a_i\}$ then the set of linear
combinations of those vectors is called the \emph{span}\ of that set. The
span of a single vector (other than the zero vector) is a line passing
through that vector. 

What about two vectors $\vec a_0$ and $\vec a_1$? Well now we have two
options. Either $\vec a_0=x\vec a_1$ for some $x\in\Real$, or there is no
such $x$. In the first case, the two vectors are colinear and their span is
just the line in which they both lie. In the second case, the two vectors
are not colinear and their span is the plane in which the two vectors
lie. If the vectors are two-dimensional ($\vec a_0,\vec a_1 \in\Real^2$) then
that plane is simply equal to $\Real^2$. On the other hand if the vectors
lie in $\Real^3$ or a higher dimensional space, then their span is the plane
in that space passing through those two vectors.

For a set of three vectors, there are now three possibilities. The first is
that all three vectors are scalar multiples of each other:
\begin{equation}
  \vec a_0=x_1\vec a_1 = x_2 \vec a_2
\end{equation} 
for some $x_1, x_2\in \Real$. In this case the span
of the set is one-dimensional. The next possibility is that the vectors are
not all colinear one of the
vectors in the set can be written as a weighted sum of the other two:
\begin{equation}
  \vec a_0=x_1\vec a_1 + x_2 \vec a_2
\end{equation}
for some $x_1, x_2\in \Real$. In this case the span is once again a plane
and all three vectors lie in that plane. The final option is that none of
the vectors can be written as a linear combination of the other two, in
which case the span is three dimensional: either all of $\Real^3$ if the
vectors lie in $\Real^3$ or a three-dimensional subspace of the space if
larger.

If none of a set of vectors can be written as a linear combination of the
others, then that set is said to be \emph{linearly independent}. 

\begin{define}
  A set of vectors $\{\vec a_0,\ldots,\vec a_n\}$ is \emph{linearly dependent} if and
  only if there exists a set of coefficents $\{x_0,\ldots,x_n\}$ such that:
  \begin{equation}
    \sum_{i=0}^n x_i\vec a_i = 0
  \end{equation}
  and at least one of the coefficients $\x_i$ is not zero.
\end{define}

\begin{define}
  A set of vectors is \emph{linearly independent}\ if and only if it is not
  \emph{linearly dependent}.
\end{define}

Let's look back at our matrix $\mat A$. We can rephrase the matrix equation:
\begin{equation}
  \mat A\x=\vec b
\end{equation}
as the question: which linear combination of the columns of $\mat A$ gives
$\vec b$ As we've just seen, and as you learned in Mathematical Methods 1, the
answer may be that \emph{no}\ linear combination of the columns of $\mat A$
gives $\vec b$ or it may be that there is more than one combination which
works. 

If $\mat A$ is $n\times n$ then we've seen above that the span of its
columns, which we write $\operatorname{span}(\mat A)$ is a subspace of
$\Real^n$ with dimension somewhere between 0 and $n$. Further, the
$\operatorname{span}(\mat A)$ is only $n$-dimensional if the columns of $A$
form a linearly independent set. This leads us to a whole list of properties
which are equivalent for a $n\times n$ square matrix $\mat A$:

\begin{enumerate}
\item The columns of $\mat{A}$ are linearly independent.
\item The span of the columns of $\A$ is $\Real^n$.
\item $\A$ is invertible. i.e. there exists a matrix $\mat A^{-1}$ such that
  $\A\A^{-1}=\mat{I}$
\item $\A\x=\vec b$ has a unique solution for every $\vec b\in\Real^n$.
\item The unique solution to $\A\x=\vec 0$ is the zero vector, $\vec 0$.
\end{enumerate}

Using the knowledge of eigenvalues and determinants gained in Maths Methods
I, we can add two more items to this list:

\begin{enumerate}\setcounter{enumi}{5}
\item $\det(\A)\neq 0$.
\item $0$ is not an eigenvalue of $\A$.
\end{enumerate}

As a reminder:
\begin{define}
  $\lambda$ is an \emph{eigenvalue}\ of a matrix $\mat{A}$ if there exists
  some non-zero vector $\vec{v}$ such that:
  \begin{equation}
    \mat{A}\vec{v}=\lambda\vec{v}
  \end{equation}
\end{define}

\begin{exercise}
  If $\A$ is a square matrix whose columns are \emph{not}\ linearly
  independent, show that the equation:
  \begin{equation}
    \A\x=\vec{0}
  \end{equation}
  has a non-zero solution.
\end{exercise}

\begin{exercise}
  Use the result from the previous exercise to argue that $0$ is an
  eigenvalue of $\A$.
\end{exercise}

\subsection{Matrix rank}

Unfortunately at this stage we encounter a very confusing clash of
terminology. The dimension of the span of a matrix is known as the
\emph{rank}\ of that matrix. This has nothing to do with the concept of the
rank of an array. All matrices are rank 2 arrays. We will therefore avoid
talking about the matrix rank (i.e. the dimension of the column span) as
much as possible. However, there is one important piece of terminology which
it is hard to avoid. A matrix whose column span has the maximum possible
dimension is said to have full rank. For a $n\times m$ matrix, this maximum
rank is $\min(n,m)$.

We can now add an additional equivalent statement to the list above:

\begin{enumerate}\setcounter{enumi}{7}
\item $\A$ has full rank.
\end{enumerate}


\section{Matrices as a basis for $\Real^n$}

Yet another way that we can think of the columns of a matrix $\mat A$ is as
the axes of a coordinate system. In this interpretation, the matrix equation
\begin{equation}
  \A\x=\vec b
\end{equation}
means: what are the coordinates of the vector $\vec b$ in the coordinate
system given by the columns of $\mat A$?

To illustrate this, think of the matrix
whose columns are the usual three-dimensional basis vectors $\vec i$, $\vec
j$ and $\vec k$:
\begin{equation}
  \begin{bmatrix}
    \vec i&\rule[-4ex]{1pt}{9ex}&
    \vec j&\rule[-4ex]{1pt}{9ex}&
    \vec k\\
  \end{bmatrix}
  =
  \begin{bmatrix}
    1 & 0 & 0\\
    0 & 1 & 0\\
    0 & 0 & 1
  \end{bmatrix}
  \equiv 
  \mat I
\end{equation}
If we pose the equation:
\begin{equation}
  \mat I \x =\vec b
\end{equation}
then we are asking: what are the coordinates of $\vec b$ with respect to the
usual basis? The answer is, of course, $\x=\vec b$. 

So we can think of an $n\times n$ square matrix of full rank as an
alternative set of basis vectors for $\Real^n$. 

\section{Solving triangular systems}

A \emph{lower triangular matrix}\ is one in which all of the entries above
the diagonal are zero. 

\begin{equation}
  \mat{L}=\begin{bmatrix}
    \mat L_{0,0} & 0 & \cdots & 0\\
    \mat L_{1,0} & \mat L_{1,1} &\cdots & 0\\
    \vdots & \vdots & \ddots & 0 \\
    \mat L_{n,0} & \mat L_{n,1} & \cdots & \mat L_{n,n}
  \end{bmatrix}
\end{equation}

Solving a matrix equation involving a lower triangular matrix is easy. Let's
write:
\begin{equation}
    \mat{L}\x=\vec{b}
\end{equation}

We just have to remember the rule for matrix-vector multiplication. We first
multiply the first row of $\mat{L}$ by $\x$ and set it equal to the first
entry in $\vec{b}$:
\begin{equation}
  \mat{L}_{0,0}x_{0} + \vec0\cdot \vec{x}_{1:} = b_{0}
\end{equation}
Here we're writing $\vec0$ to mean a vector of all zeros and copying
Python's array slice notation so $\vec{x}_{1:}$ means the second and
subsequent entries in $\vec{x}$.

Hence:
\begin{equation}
  x_{0} = b_{0}/\mat{L}_{0,0}
\end{equation}

In the second row, we get:
\begin{equation}
  \mat{L}_{1,0} x_{0} + \mat{L}_{1,1} x_{1} + \vec0\cdot\vec x_{2:} =
   b_1
\end{equation}
Since we already know $ x_0$, we can write this as:
\begin{equation}
  x_{1} = \left(b_1 - \mat{L}_{1,0} x_{0}\right)/\mat{L}_{1,1}
\end{equation}
This pattern continues: for each row we can use the fact that we already
know all the preceding $\vec x$ values. The expression for the $n$-th row of
$\mat L$ is therefore:
\begin{equation}
   x_n = \left( b_n - \mat{L}_{n,:n}\cdot\vec x_{:n}\right)/\mat{L}_{n,n}
\end{equation}

\begin{exercise}
  Create a module called ~linear_algebra~. Write a function\linebreak ~solve_lower(L,b)~
  which solves the matrix problem $\mat{L}\vec{x}=\vec{b}$ if $\mat{L}$ is
  lower triangular and returns the
  answer $\vec{x}$. If $\mat{L}$ is not lower triangular, you should raise a
  ~ValueError~ exception. \emph{Hint:}\ you can check that you've got the
  right answer by calculating ~dot(L,x)-b~.
\end{exercise}

\begin{exercise}
  The functions in ~/numerical-methods-1/random_matrices.py~ are useful for
  generating matrices and vectors to test linear algebra problems. By using
  the test:
  \begin{pythonsnippet}
    if __name__=='__main__':
       # Test code goes here.
  \end{pythonsnippet}
  it is possible to include code in your ~linear_algebra.py~ file which is
  only executed when the file is used as a script rather than as a
  module. Add test code in this form to ~linear_algebra.py~ which solves $\mat{L}\vec{x}=\vec{b}$
  for random lower triangular matrices $\mat L$ of various sizes and
  corresponding random vectors $\vec b$. You should check that the answer is
  correct and print appropriate messages for success or failure.
\end{exercise}

\begin{exercise}
  An \emph{upper triangular matrix}\ is, naturally, a matrix in which the
  entries below the diagonal are all zero. Write another function for your
  module, this time ~solve_upper(U,b)~ to solve an upper triangular problem.
  You should raise ~ValueError~ if the matrix $\mat U$ is not actually upper
  triangular. \emph{Hint:}\ work backwards from the last row of the matrix.

  Extend your test code to also test ~solve_upper~.
\end{exercise}

\section{The LU factorisation}

We now know how to solve problems of the form $\mat L\vec x = \vec b$ and
$\mat U\vec x = \vec b$ where $\mat L$ and $\mat U$ are lower and upper
triangular respectively. This might not seem like a very useful piece of
information since most matrices we are likely to encounter in practice are
unlikely to be lower or upper triangular. Suppose, though, that we were able
to write a general matrix $\mat A$ in the form:
\begin{equation}
  \mat A = \mat L \mat U
\end{equation}
Then we could rewrite the problem
\begin{equation}
  \mat A\vec x = \vec b
\end{equation}
as:
\begin{equation}\label{eq:LU1}
  \mat L\mat U \vec x = \vec b
\end{equation}
Now if we create a new unknown vector $\vec c$ which we define by:
\begin{equation}\label{eq:U_c}
  \mat U\vec x = \vec c
\end{equation}
then we can substitute into equation \eqref{eq:LU1}\ to create:
\begin{equation}\label{eq:L_b}
  \mat L\vec c = \vec b
\end{equation}
We know how to solve equation \eqref{eq:L_b}\ for $\vec c$. Once we have
$\vec c$ then we know how to solve equation \eqref{eq:U_c} for $\vec x$.

The challenge, then is to find a way to rewrite $\mat A$ as $\mat L\mat U$. 

\subsection{Gaussian elimination revisited}

In Maths Methods 1 you learned how to solve the matrix equation $Ax=b$ using
Gaussian elimination. We'll revisit this algorithm now from the perspective
of matrix operations as a way of forming our $\mat L\mat U$ factorisation.

Let's take the following matrix as our example:
\begin{equation}
  A=\begin{bmatrix}
    {\color{black}5} & {\color{black}7} & {\color{black}5} & {\color{black}9}\\
{\color{black}5} & {\color{black}14} & {\color{black}7} & {\color{black}10}\\
{\color{black}20} & {\color{black}77} & {\color{black}41} & {\color{black}48}\\
{\color{black}25} & {\color{black}91} & {\color{black}55} & {\color{black}67}\\
    \end{bmatrix}
\end{equation}

Remember that the first step of Gaussian elimination is to set the
sub-diagonal rows in the first column to zero by subtracting multiples of
the first row from each of the subsequent rows. We can write this as a
matrix multiplication:
\begin{equation}
  \mat{L}_0\cdot\mat{A}
\end{equation}

\begin{equation*}
  \begin{bmatrix}
    {\color{black}1} & {\color{black}0} & {\color{black}0} & {\color{black}0}\\
    {\color{Orange}-1} & {\color{black}1} & {\color{black}0} & {\color{black}0}\\
    {\color{Orange}-4} & {\color{black}0} & {\color{black}1} & {\color{black}0}\\
    {\color{Orange}-5} & {\color{black}0} & {\color{black}0} & {\color{black}1}\\
    
  \end{bmatrix}\qquad\times\qquad\begin{bmatrix}
    {\color{black}5} & {\color{black}7} & {\color{black}5} & {\color{black}9}\\
    {\color{black}5} & {\color{black}14} & {\color{black}7} & {\color{black}10}\\
    {\color{black}20} & {\color{black}77} & {\color{black}41} & {\color{black}48}\\
    {\color{black}25} & {\color{black}91} & {\color{black}55} & {\color{black}67}\\
    
  \end{bmatrix}\qquad=\qquad\begin{bmatrix}
    {\color{black}5} & {\color{black}7} & {\color{black}5} & {\color{black}9}\\
    {\color{blue}0} & {\color{blue}7} & {\color{blue}2} & {\color{blue}1}\\
    {\color{blue}0} & {\color{blue}49} & {\color{blue}21} & {\color{blue}12}\\
    {\color{blue}0} & {\color{blue}56} & {\color{blue}30} & {\color{blue}22}\\
    
  \end{bmatrix}
\end{equation*}
To see how this works, let's remember the algorithm for matrix
multiplication:
\begin{equation}
  \mat L_0\mat A = \sum_{i=1}^{n} \mat L_0[:,i]\mat A[i,:].
\end{equation}
If we restrict this to just the second row of the right hand side (row
number 1), this becomes:
\begin{equation}
  \sum_{i=1}^{n} \mat L_0[1,i]\mat A[i,:]=-1\times \mat A[0,:]+1\times \mat A[1,:]
\end{equation}
In other words, the second line of $\mat L_0$ has the effect summing -1
times the first row of $\mat A$ and $1$ times the second row. The resulting
vector is the second row of the product $\mat L_0\cdot\mat A$. 

Similarly, the operation of the third row of $\mat L_0$ is to form the third
row of $\mat L_0\cdot\mat A$ by adding -4 times the first row of
$\mat A$ to 1 times the third row of $\mat A$.

So what's the rule for generating $\mat L_0$? We start with the identity
matrix, because we always have one times the current row. We then need to
subtract the correct multiple of the first row so that the first entry of
the second and subsequent row of $\mat L_0\cdot\mat A$ is zero. An algorithm
for this is:
\begin{algorithmic}
  \STATE $\mat  L_0\gets\mat I$
  \FOR {$i\gets 1\to\len(\A)-1$}
  \STATE $\L_0[i,0]\gets-\A[i,0]/\A[0,0]$ 
  \ENDFOR
\end{algorithmic}

Now let's procede to the second row of A. This time we use Gaussian
elimination to produce zeros below the diagonal in the \emph{second}\ column:

\begin{equation}
  \mat{L}_1\cdot\left(\mat{L}_0\cdot\mat{A}\right)
\end{equation}

\begin{equation*}
  \begin{bmatrix}
    {\color{black}1} & {\color{black}0} & {\color{black}0} & {\color{black}0}\\
    {\color{black}0} & {\color{black}1} & {\color{black}0} & {\color{black}0}\\
    {\color{black}0} & {\color{Orange}-7} & {\color{black}1} & {\color{black}0}\\
    {\color{black}0} & {\color{Orange}-8} & {\color{black}0} & {\color{black}1}\\

  \end{bmatrix}\qquad\times\qquad\begin{bmatrix}
    {\color{black}5} & {\color{black}7} & {\color{black}5} & {\color{black}9}\\
    {\color{black}0} & {\color{black}7} & {\color{black}2} & {\color{black}1}\\
    {\color{black}0} & {\color{black}49} & {\color{black}21} & {\color{black}12}\\
    {\color{black}0} & {\color{black}56} & {\color{black}30} & {\color{black}22}\\

  \end{bmatrix}\qquad=\qquad\begin{bmatrix}
    {\color{black}5} & {\color{black}7} & {\color{black}5} & {\color{black}9}\\
    {\color{black}0} & {\color{black}7} & {\color{black}2} & {\color{black}1}\\
    {\color{black}0} & {\color{blue}0} & {\color{blue}7} & {\color{blue}5}\\
    {\color{black}0} & {\color{blue}0} & {\color{blue}14} & {\color{blue}14}\\

  \end{bmatrix}
\end{equation*}

This time, Gaussian elimination was applied not to $\mat{A}$ but to the
answer from the first step: $\mat{L}_0\cdot\mat{A}$. So to write an
algorithm for Gaussian elimination, we need to keep track of the previous
steps applied to $\A$. For reasons which will become obvious, let's write
$\U=\L_0\cdot\mat{A}$. We could calculate $\U$ directly using a dot product,
but that would involve a lot of useless multiplications by zero since $\L_0$
is mostly made up of zeros. Instead, we can modify our algorithm for finding
$\L_0$ to also calculate $U$:

\begin{algorithmic}
  \STATE $\mat  L_0\gets\mat I$
  \STATE $\mat  U\gets\A$
  \FOR {$i\gets 1 \to\len(\A)-1$}
  \STATE$\L_0[i,0]\gets-\U[i,0]/\U[0,0]$
  \STATE$\U[i,:]\gets\U[i,:]+\L_0[i,0]\times\U[0,:]$
  \ENDFOR
\end{algorithmic}
Now we can use that $\U$ and the corresponding algorithm to calculate
$\L_1$:

\begin{algorithmic}
  \STATE$\mat  L_1=\mat I$
  \FOR{$i\gets 2\to\len(\A)-1$}
  \STATE$\L_1[i,1]\gets-\U[i,1]/\U[1,1]$
  \STATE$\U[i,:]\gets\U[i,:]+\L_0[i,1]\times\U[1,:]$
  \ENDFOR
\end{algorithmic}

Notice that the loop over $i$ now starts from 2 because we're setting the
subdiagonal entries of the second row. 

Of course we don't really want to write out a special case of the algorithm
for each row of the matrix, and we can easily generalise this to the full
algorithm for all the $\mat{L}_j$:

\begin{algorithmic}
  \STATE $\mat U\gets\A$
  \FOR{$j\gets 0 \to\len(\A)-2$}
  \STATE$\mat  L_j\gets\mat I$
  \FOR{$i\gets j+1\to\len(\A)-1$}
  \STATE$\L_j[i,j]\gets-\U[i,j]/\U[j,j]$
  \STATE$\U[i,:]\gets\U[i,:]+\L_j[i,j]\times\U[j,:]$
  \ENDFOR
  \ENDFOR
\end{algorithmic}

We can apply this algorithm to get the final step:

\begin{equation}
  \mat{L}_2\cdot\left(\mat{L}_1\cdot\left(\mat{L}_0\cdot\mat{A}\right)\right)
\end{equation}

\begin{equation*}
  \begin{bmatrix}
    {\color{black}1} & {\color{black}0} & {\color{black}0} & {\color{black}0}\\
    {\color{black}0} & {\color{black}1} & {\color{black}0} & {\color{black}0}\\
    {\color{black}0} & {\color{black}0} & {\color{black}1} & {\color{black}0}\\
    {\color{black}0} & {\color{black}0} & {\color{Orange}-2} & {\color{black}1}\\

  \end{bmatrix}\qquad\times\qquad\begin{bmatrix}
    {\color{black}5} & {\color{black}7} & {\color{black}5} & {\color{black}9}\\
    {\color{black}0} & {\color{black}7} & {\color{black}2} & {\color{black}1}\\
    {\color{black}0} & {\color{black}0} & {\color{black}7} & {\color{black}5}\\
    {\color{black}0} & {\color{black}0} & {\color{black}14} & {\color{black}14}\\

  \end{bmatrix}\qquad=\qquad\begin{bmatrix}
    {\color{black}5} & {\color{black}7} & {\color{black}5} & {\color{black}9}\\
    {\color{black}0} & {\color{black}7} & {\color{black}2} & {\color{black}1}\\
    {\color{black}0} & {\color{black}0} & {\color{black}7} & {\color{black}5}\\
    {\color{black}0} & {\color{black}0} & {\color{blue}0} & {\color{blue}4}\\

  \end{bmatrix}
\end{equation*}

We can now see why we called the product matrix $\U$. So we've now managed
to write:
\begin{equation}
  \mat{L}_2\cdot\left(\mat{L}_1\cdot\left(\mat{L}_0\cdot\mat{A}\right)\right)=\U
\end{equation}
This is sort of like our goal of $\A=\L\cdot\U$ but not quite. To get from
one to the other we first need to move the $\L_i$ from the left to the right
of the equation. We can move $\L_2$ by multiplying both sides by its inverse:
\begin{gather}
  \mat{L}_2^{-1}\cdot\mat{L}_2\cdot\left(\mat{L}_1\cdot\left(\mat{L}_0\cdot\mat{A}\right)\right)=\mat{L}_2^{-1}\cdot\U\\
  \mat I\cdot\left(\mat{L}_1\cdot\left(\mat{L}_0\cdot\mat{A}\right)\right)=\mat{L}_2^{-1}\cdot\U\\
  \left(\mat{L}_1\cdot\left(\mat{L}_0\cdot\mat{A}\right)\right)=\mat{L}_2^{-1}\cdot\U
\end{gather}
By successively multiplying by the inverses if $\L_i$ in reverse order, we
come to:
\begin{equation}
  \mat{A}=\mat{L}_0^{-1}\cdot\mat{L}_1^{-1}\cdot\mat{L}_2^{-1}\cdot\U
\end{equation}
This looks a bit closer, but it depends on us knowing $\L_i^{-1}$. This is
the first piece of magic in this algorithm. Let's take the example of
$\L_0$. It so turns out that:
\begin{equation}
  \begin{bmatrix}
    {1} & {0} & {0} & {0}\\
    {-1} & {1} & {0} & {0}\\
    {-4} & {0} & {1} & {0}\\
    {-5} & {0} & {0} & {1}\\
  \end{bmatrix}^{-1}
  =
  \begin{bmatrix}
    {1} & {0} & {0} & {0}\\
    {1} & {1} & {0} & {0}\\
    {4} & {0} & {1} & {0}\\
    {5} & {0} & {0} & {1}\\
  \end{bmatrix}.
\end{equation}
To see why this is true, consider the $2,0$ entry in $\L_0\cdot\L_0^{-1}$:
\begin{align}
  \L_0[2,0]&=
  \begin{bmatrix}
    {-4} & {0} & {1} & {0}\\
  \end{bmatrix}
  \begin{bmatrix}
    {1} \\
    {1} \\
    {4} \\
    {5} \\
  \end{bmatrix}\\
  &=-4\times1+0\times1+1\times4+0\times5\\
  &=0
\end{align}
This pattern of cancellations repeats throughout. 
We therefore have a nice easy way of forming $\L_i^{-1}$.

The final stage of our quest is to combine
$\mat{L}_0^{-1}\cdot\mat{L}_1^{-1}\cdot\mat{L}_2^{-1}$ into a single matrix
$\L$, which we hope will be lower diagonal. At this point, a second piece of
magic comes to our aid. It turns out that:
\begin{equation}
  \begin{bmatrix}
    {1} & {0} & {0} & {0}\\
    {1} & {1} & {0} & {0}\\
    {4} & {0} & {1} & {0}\\
    {5} & {0} & {0} & {1}\\
  \end{bmatrix}\cdot
  \begin{bmatrix}
    {1} & {0} & {0} & {0}\\
    {0} & {1} & {0} & {0}\\
    {0} & {7} & {1} & {0}\\
    {0} & {8} & {0} & {1}\\
  \end{bmatrix}\cdot
  \begin{bmatrix}
    {1} & {0} & {0} & {0}\\
    {0} & {1} & {0} & {0}\\
    {0} & {0} & {1} & {0}\\
    {0} & {0} & {2} & {1}\\
  \end{bmatrix}=
  \begin{bmatrix}
    {1} & {0} & {0} & {0}\\
    {1} & {1} & {0} & {0}\\
    {4} & {7} & {1} & {0}\\
    {5} & {8} & {2} & {1}\\
  \end{bmatrix}
\end{equation}
That is, the sub-diagonal entries are merged. Note that this is not a
general matrix property, it only applies to matrices of the form $\L_i$ and
only when multiplied in this order. 

We can use these inverse and multiplication properties to modify our
algorithm. Notice that the sign changes on the entries of $\L$ and this
causes a sign change on the construction of $\U$ to ensure we get the same
$\U$ as before:
\begin{algorithmic}
  \STATE$\mat  U\gets \A$
  \STATE$\mat  L\gets \mat I$
  \FOR{$j\gets 0\to \len(\A)-2$}
  \FOR{$i\gets j+1\to \len(\A)-1$}
  \STATE$\L[i,j]\gets \U[i,j]/\U[j,j]$
  \STATE$\U[i,:]\gets \U[i,:]-\L[i,j]\times\U[j,:]$  
  \ENDFOR
  \ENDFOR
\end{algorithmic}

\begin{exercise}
  Add a function ~factorise_lu(A)~ to your ~linear_algebra~ module. This
  function should take a matrix ~A~ and return its LU factors ~L~ and
  ~U~. Your function should raise ~ValueError~ if ~A~ is not square.

  Extend the test code in ~linear_algebra.py~ to test ~factorise_lu~ for matrices of
  various sizes.  In particular,
  ~random_matrices.random_lu~ is guaranteed to return a matrix for which LU
  factorisation will work.
\end{exercise}


\begin{exercise}
  Add a function ~solve_lu(A,b)~ to your ~linear_algebra~ module. This
  function should take a matrix ~A~ and a vector ~b~ and return ~x~, the
  solution to $\A\x=\vec b$. Calculate the solution by first calling
  ~factorise_lu~ and then solving the two resulting diagonal systems.

  Extend the test code in ~linear_algebra.py~ to test ~solve_lu~ for matrices of various
  sizes.
\end{exercise}

\section{Breakdown of the LU factorisation}

Let's take the example\footnote{This example is from Trefethen and Bau (1997)} of the matrix:
\begin{equation}\label{eq:breakdown}
  \mat{A}=
  \begin{bmatrix}
    0 & 1\\
    1 & 1
  \end{bmatrix}
\end{equation}

If we apply LU factorisation to this matrix then $\U[0,0]=0$ and as soon as
we start calculating the second row, we have to divide by $\U[0,0]$ so the
algorithm fails.

There is an even nastier version of this problem. Consider: 
\begin{equation}
  \mat{B}=
  \begin{bmatrix}
    10^{-20} & 1\\
    1 & 1
  \end{bmatrix}
\end{equation}

If we apply LU factorisation to this matrix using exact arithmetic, we have:
\begin{equation}
  \mat{L}=
  \begin{bmatrix}
    1 & 0 \\
    10^{20} & 1
  \end{bmatrix},
  \qquad
  \mat{U}=
  \begin{bmatrix}
    10^{-20} & 1 \\
    0 & 1-10^{20}
  \end{bmatrix}
\end{equation}
The factorisation has numbers with huge and tiny magnitudes in it. This
might not be a problem in exact arithmetic, but in floating point arithmetic
the last entry in $\mat{U}$ is likely to be rounded to $-10^{20}$. Is this OK?
After all $1$ in $10^{20}$ error is pretty tiny. However:
\begin{equation}
    \begin{bmatrix}
    1 & 0 \\
    10^{20} & 1
  \end{bmatrix}
  \times
  \begin{bmatrix}
    10^{-20} & 1 \\
    0 & -10^{20}
  \end{bmatrix}=
  \begin{bmatrix}
    10^{-20} & 1\\
    1 & 0
  \end{bmatrix}
\end{equation}
Look at the last entry in the product. That's a huge error. 

The lesson we draw from this is that LU factorisation is
\emph{unstable}. It is succeptable to dividing by zero or by tiny numbers
when these occur in the algorithm and the effects of very small rounding
errors result in huge errors in the factorisation.

\section{Pivoting*}

If we were to factor matrix $\mat{A}$ from equation \eqref{eq:breakdown}\
using Gaussian elimination, we would immediately know what to do: switch the
first two rows and the division by zero vanishes. How do we introduce row
interchange operations in an automatic way?

\subsection{Permutation matrices*}

It turns out that we can swap the rows or columns of a matrix by multiplying
by a special matrix. The mathematical term for switching the order of a list
is \emph{permutation}.  A permutation matrix is one in which every row and
every column contains exactly one entry with the value 1 and all the other
entries zero. If we left multiply by a permutation matrix (i.e. evaluate
$\mat{P}\mat{A}$ then the matrix will rearrange the \emph{rows}\ of A.  In
particular, if $\mat{P}_{i,j}=1$ then the $i$-th row of $\mat{PA}$ will be
equal to the $j$-th row of $A$. Some examples might help. The first is a
$3\times3$ matrix which swaps the first two rows:
\begin{equation}
  \begin{bmatrix}
    0 & 1 & 0\\
    1 & 0 & 0\\
    0 & 0 & 1
  \end{bmatrix}
\end{equation}
This next one is known as a cyclic permutation, it moves each row to the
next one down and the last row to the first:
\begin{equation}
  \begin{bmatrix}
    0 & 0 & 0 & 1\\
    1 & 0 & 0 & 0\\
    0 & 1 & 0 & 0\\
    0 & 0 & 1 & 0
  \end{bmatrix}
\end{equation}
Note that the permutation matrix is just the identity matrix with its rows
permuted. The identity matrix is itself a permutation: the identity
permutation which maps each row to itself.

Another way of writing down a permutation is as a vector. The $i$-th entry
of the vector is the number to which $i$ is mapped under the
permutation. For this last example, the permutation vectors corresponding to
the permutation matrices above are:
\begin{gather}
  \begin{bmatrix}
    1,0,2
  \end{bmatrix}\\\intertext{and}
  \begin{bmatrix}
    3,0,1,2
  \end{bmatrix}
\end{gather}

Python makes it trivial to generate permutation matrices from permutation
vectors. First, create an identity matrix using the ~numpy.eye~ function and
then select the rows in the order of the permutation by providing the
permutation vector as a subscript. For example, we can produce the first
permutation matrix above by writing:
\begin{pythonsnippet}
  from numpy import array,eye
  p_vector=array([1,0,2])
  p_array=eye(3)
  p_array=p_array[p_vector]
\end{pythonsnippet}
Actually, since the result of ~eye~ is an array, we can write:
\begin{pythonsnippet}
  from numpy import array,eye
  p_vector=array([1,0,2])
  p_array=eye(3)[p_vector]
\end{pythonsnippet}

\subsection{Partial pivoting*}

We can now apply permutation matrices to swap the rows of $\A$ so we never
end up dividing by a value which is close to zero. The way we do this is by
replacing our equation:
\begin{equation}
  \A\x=\vec b
\end{equation}
with 
\begin{equation}
  \P\A\x=\P\vec b
\end{equation}
where $\P$ is a permutation matrix which interchanges the rows of $\A$ to
avoid unfortunate divisions. So far, so good. We know how to construct $\P$
if we know which row to choose. Let's look back at our LU algorithm:

\begin{algorithmic}
  \STATE$\mat  U\gets \A$
  \STATE$\mat  L\gets \mat I$
  \FOR{$j\gets 0\to \len(\A)-2$}
  \FOR{$i\gets j+1\to \len(\A)-1$}
  \STATE$\L[i,j]\gets \U[i,j]/\U[j,j]$
  \STATE$\U[i,:]\gets \U[i,:]-\L[i,j]\times\U[j,:]$  
  \ENDFOR
  \ENDFOR
\end{algorithmic}

The only division, and therefore the problem term, is the division by
$\U[j,j]$. If we swap rows, this means we can instead choose to divide by
any value in the column under $\U[j,j]$. In other words, $\U[k,j]$ for any
$k\geq j$.

There is a catch, however. $\U$ changes at each step of the algorithm so
it's only at the start of the step that we can decide which row to use
next. To minimise the rounding errors from the division, we always choose
the row for which $|\U[k,j]|$ is largest.

But wait, we've already started! We can't just go messing with the row order
in $\U$ because we've already started building $\L$. The answer is that it's
all fine if we also switch the rows in the part of $\L$ we've already
built. This modifies $\L$ as if we had always assembled it in permuted order. The
new algorithm looks like this:

\begin{algorithmic}
  \STATE$\mat  U\gets \A$
  \STATE$\mat  L\gets \mat I$
  \STATE  $\P\gets \mat I$
  \FOR{$j\gets 0\to \len(\A)-2$}
  \STATE choose $k\geq j$ such that $|\U[k,j]|$ is maximised.
  \STATE$\U[j,j:]\leftrightarrow\U[k,j:]$
  \STATE$\L[j,:j]\leftrightarrow\L[k,:j]$
  \STATE$\P[j,:]\leftrightarrow\P[k,:]$
  \FOR{$i\gets j+1\to \len(\A)-1$}
  \STATE$\L[i,j]\gets \U[i,j]/\U[j,j]$
  \STATE$\U[i,:]\gets \U[i,:]-\L[i,j]\times\U[j,:]$  
  \ENDFOR
  \ENDFOR
\end{algorithmic}

This algorithm is known as partial pivotting in contrast with full pivotting
in which both rows and columns are interchanged. It should also be noted
that this algorithm is mathematically correct but there are more efficient
versions in which less copying about takes place.

\begin{exercise}*
  Modify your ~factorise_lu(A)~ function to include partial pivoting and
  return the permutation matrix $\P$ as well as $\L$ and $\U$. Modify
  ~solve_lu(A,b)~ to employ your new function.
\end{exercise}

\section{The official version}

It's important to understand how matrix equations work, but efficient
implementation of solution strategies is a black art all of its own. Numpy
comes with efficient and stable implementations of many important linear
algebra algorithms. In particular, matrix solution is implemented by
~numpy.linalg.solve~. The eigenvalue routine ~numpy.linalg.eig~ is also
particularly useful for practical work. 

There is an even more complete set of linear algebra routines in the module
~scipy.linalg~ including ~scipy.linalg.lu_factor~ and
~scipy.linalg.lu_solve~ which form an implementation of the algorithm
studied here.

\section{Further reading}

An excellent reference on a wide range of numerical linear algebra is
Trefethen and Bau ``Numerical Linear Algebra'', SIAM, 1997. Lecture 20
covers Gaussian elimination and the LU factorisation.
