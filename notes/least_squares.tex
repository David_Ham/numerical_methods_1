
\lecture{Curve fitting and least squares}

\section{Orthogonality}

In order to understand the main subject of this lecture, we first need to
revisit the concept of orthogonality. Orthogonality can be thought of as the
property of being at right angles. \emph{Perpendicular} and \emph{normal} are other words
which are used to describe orthogonality. From Maths Methods 1, you already
know the relationship between the orthogonality of vectors and the dot
product:

\begin{define}
  Two vectors $\vec a$ and $\vec b$ are \emph{orthogonal}\ if and only if:
  \begin{equation}
    \vec{a}\cdot\vec{b}=0
  \end{equation}
\end{define}

Using what we know about the relationship between the dot product and matrix
multiplication, we can draw some immediate conclusions from this definition:

\begin{theorem}
  A vector $\vec b$ is \emph{orthogonal} to all of the rows of a matrix $\A$
  if and only if:
  \begin{equation}
    \A\vec b = \vec 0
  \end{equation}
\end{theorem}

\begin{theorem}
  A vector $\vec b$ is \emph{orthogonal} to all of the columns of a matrix $\A$
  if and only if:
  \begin{equation}
    \vec b\A = \vec 0
  \end{equation}
\end{theorem}


\section{Least squares solutions in two dimensions}

In chapter \ref{lec:linalg}\ we learned that the span of one vector is the
line passing through that vector. We also learned that if $\mat A$ has just
one column there is no solution to the problem:
\begin{equation}\label{eq:least}
  \mat A\vec x = \vec b
\end{equation}
unless $\vec b$ is a multiple of the one column of $\mat A$. Note that if
$\mat A$ has only one column, $\vec x$ only has one entry. However we will
continue to write it as a vector because we will later generalise this
concept to more dimensions. 

The situation described by equation \eqref{eq:least}\ is shown in figure
\ref{fig:least_square_1}
\begin{figure}
  \begin{center}
    \input{figures/least_square_1_pdf.tex}

  \end{center}

  \caption{$\A\x=\vec b$ has no solution if $\A$ has just one column and
    $\vec b$ is not a scalar multiple of that column.}
  \label{fig:least_square_1}
\end{figure}
Clearly there is no multiple of $\mat A$ ($=\mat A_{:,0}$) which will give
$\vec b$. However it's also clear that some multiples of $\mat A$ are closer
to $\vec b$ than others. To explore this concept we need to introduce the
concept of a \emph{residual}.
\begin{define}
  For a matrix equation:
  \begin{equation}
    \mat A\vec x = \vec b
  \end{equation}
  then for any choice of vector $\vec x$, the $residual$ $\vec r$ is given
  by:
  \begin{equation}
    \vec r = \vec b - \mat A\vec x
  \end{equation}
\end{define}
Note that if $\vec x$ is a solution to the equation, then the residual will
be the zero vector, $\vec 0$.  The case where $\vec x$ is not the
solution is illustrated in figure \ref{fig:residual}.
\begin{figure}
  \begin{center}
    \input{figures/residual_pdf.tex}

  \end{center}
  \caption{The residual $\vec r=\vec b - \A\x$}
\label{fig:residual}
\end{figure}
The exciting thing about this is that it gives us a sense in which we can
solve the problem $\A\vec x =\vec b$. There is no exact solution, but
we can find the solution for which the residual is the smallest possible. 

What does it mean for the residual to be as small as possible? This is just
saying that the length of $\vec r$ should be as small as possible. Remember
that the length of $\vec r$ is given by $\sqrt{\vec r\cdot\vec r}$. So we
want to minimise $\sqrt{\vec r\cdot\vec r}$. This is actually the same
problem as minimising $\vec r\cdot\vec r$ or in other words $\vec r^2$, which is what gives
this form of solution its name: \emph{least squares}.

So, which vector $\mat A\vec x$ will minimise the residual? It's possible to
mathematically derive the answer, but actually it's obvious from thinking
about the problem geometrically: $\vec r\cdot\vec r$ is minimised when $\vec
r$ is \emph{orthogonal}\ to $\A\vec x$. Figure \ref{fig:least_square_2}\
illustrates this.
\begin{figure}
  \begin{center}
    \input{figures/least_square_2_pdf.tex}

  \end{center}
  
  \caption{The residual is minimised when it is \emph{orthogonal}\ to $\A\x$}
\label{fig:least_square_2}
\end{figure}
So, when is $\vec r$ orthogonal to $\A\vec x$? As we already know, $\A\vec
x$ always points in the same direction as $\mat A_{:,0}$. So $\vec r$ is
orthogonal to $\A\vec x$ when it is orthogonal to $\mat A_{:,0}$. From the
definition of orthogonality, we know this occurs when:
\begin{equation}
  \mat A_{:,0}\cdot \vec r = 0
\end{equation}
If we now substitute in the definition of $\vec r$, we get:
\begin{equation}
  \mat A_{:,0}\cdot(\vec b - \mat A\x) = 0
\end{equation}
Now we can rearrange this equation:
\begin{gather}
  \mat A_{:,0}\cdot\vec b - \mat A_{:,0}\cdot\mat A\x = 0\\
  \mat A_{:,0}\cdot\mat A\x = \mat A_{:,0}\cdot\vec b
\end{gather}
Remember that $\A$ only has one column, so we can rewrite this as:
\begin{gather}
  \mat A_{:,0}\cdot\mat A_{:,0}\x = \mat A_{:,0}\cdot\vec b\\
  |\mat A_{:,0}|^2\x = \mat A_{:,0}\cdot\vec b\\
  \x=\frac{\mat A_{:,0}\cdot\vec b}{|\mat A_{:,0}|^2}\label{eq:ls_2d}
\end{gather}
So that's it: we've solved this equation for $\x$. 

\subsection{2D least squares example}

That was a bit complex so let's try a practical example. Suppose:
\begin{gather}
  \mat{A}=
  \begin{bmatrix}
    1 \\
    1
  \end{bmatrix}\\
  \vec b=
  \begin{bmatrix}
    1 \\
    2
  \end{bmatrix}
\end{gather}
and we want to solve:
\begin{equation}
  A\vec{x}=\vec b
\end{equation}
in a least squares sense.

Remember that since $\vec{A}$ has shape $(2,1)$ and $\vec{b}$ has shape
$(2,)$, $\x$ only has one entry and so has shape $(1,)$.

Using equation \eqref{eq:ls_2d}, we have:
\begin{align}
  \x&=\frac{\begin{bmatrix}
      1 \\
      1
    \end{bmatrix}\cdot\begin{bmatrix}
      1 \\
      2
    \end{bmatrix}}{\begin{bmatrix}
      1 \\
      1
    \end{bmatrix}\cdot\begin{bmatrix}
      1 \\
      1
    \end{bmatrix}}\\
  &=\frac{3}{2}
\end{align}
To see if this is correct, let's calculate $\mat A\vec x$ and $\vec r$:
\begin{gather}
  \mat A\vec x=
  \begin{bmatrix}
    \frac{3}{2} \\
    \frac{3}{2}
  \end{bmatrix}
\end{gather}
\begin{align}
  \vec r&=\vec b-\mat A\vec x\\
  &=
  \begin{bmatrix}
    1 \\
    2
  \end{bmatrix}-
  \begin{bmatrix}
    \frac{3}{2} \\
    \frac{3}{2}
  \end{bmatrix}\\
  &=\begin{bmatrix}
    -\frac{1}{2} \\
    \frac{1}{2}
  \end{bmatrix}
\end{align}
Note that $\vec r$ is orthogonal to $\mat A_{:,0}$:
\begin{align}
  \begin{bmatrix}
      1 \\
      1
    \end{bmatrix}\cdot
    \begin{bmatrix}
    -\frac{1}{2} \\
    \frac{1}{2}
  \end{bmatrix}=0
\end{align}

\section{Least squares in more dimensions}
\begin{figure}[tb]
  \centering
  \includegraphics{least_squares/least_squares_2d.pdf}
  \caption{A three dimensional linear least squares problem in which $\A$
    has two columns. The columns of $\A$ are shown in magenta, $\vec b$ is
    black, $\A\x$ is blue and the residual, $\vec r$, is red.}
  \label{fig:least_3d}
\end{figure}

In more dimensions, $\A$ might have more columns but still not enough to
span the space. For example in three dimensions, $\A$ might have two columns
which are linearly independent and the span of its columns would be a
plane. In this case, $\A$ has shape $(3,2)$, $\x$ has shape $(2,)$ and $\vec b$
has shape $(3,)$. 

Geometrically, $\A\x=\vec b$ only has a solution if $\vec b$ lies in the plane
spanned by the columns of $\A$. There is always a least squares solution,
however, and it is given by finding $\x$ such that the line from $\vec b$ to
$\A\x$ is orthogonal to the plane spanned by $\A$. That line is, of course,
the residual which is still given by:
\begin{align}
  \vec r=\vec b -\A\x
\end{align}
This time, the least squares solution is achieved when $\vec r$ is
orthogonal to \emph{all}\ the columns of $\A$. Figure \ref{fig:least_3d}\
illustrates this situation.

When $\A$ had a single column, we could look for a residual orthogonal to
$\A$ by taking the dot product between the single column of $\A$ and $\vec
r$ and setting this to $0$. With more columns in $\A$, we need to take the
dot product between each of them and $\vec r$ and set all the results to
zero. In other words, we need to multiply the matrix whose \emph{rows}\ are
the \emph{columns}\ of $\A$ by $\vec r$. This is achieved by taking the
\emph{transpose}\ of $\A$. The condition for the least squares solution is
therefore:
\begin{equation}
  \AT\vec r=\vec 0
\end{equation}
Substituting the definition of $\vec r$ gives:
\begin{gather}
  \AT(\vec b-\A\x)=\vec 0\\
  \AT\vec b-\AT\A\x=\vec 0\\
  \AT\A\x=\AT\vec b
\end{gather}
As a side note, the Python syntax for the transpose of an array ~A~ is ~A.T~.

To attempt to understand what we've just done, let's consider the size of
$\A$, $\x$ and $\vec b$. We assumed that $\A$ has more rows than columns so
$\shape(\A)$ is $(m,n)$ with $m>n$. $\shape(\x)$ is therefore $(n)$ and
$\shape(\vec b)$ is $(m)$. Figure \ref{fig:matmul_least}\  illustrates the
shapes of the arrays in the problem.
\begin{figure}[p]
  \begin{center}
    \input{figures/matmul_least_pdf}
  \end{center}
  
  \caption{Matrix shapes in the overdetermined problem $\A\x=\vec b$}
\label{fig:matmul_least}
\end{figure}
Now, $\AT$ has shape $(n,m)$, so $\AT\A$ has shape $(n,n)$, as shown in
figure \ref{fig:matmul_transpose}.
\begin{figure}[p]
  \begin{center}
    \input{figures/matmul_transpose_pdf}
  \end{center}

  \caption{$\AT\A$ is a square $n\times n$ matrix.}
\label{fig:matmul_transpose}
\end{figure}

Similarly, $\AT\vec b$ is a vector of length $n$. Our least squares problem is
therefore a square $n\times n$ matrix problem:
\begin{equation}
  \mat (\AT\A)\x=\AT\vec b
\end{equation}
So long as the columns of $\AT\A$ are linearly independent, we know how to
solve this problem.

We will state without proof (to avoid additional complication), the
condition which ensures that we can solve this least squares problem:
\begin{theorem}
  The matrix $\AT\A$ has linearly independent columns if and only if the
  matrix $\A$ has linearly independent columns.
\end{theorem}

\subsection{3D Least squares example}

To make this technique more concrete, let's try an example. Suppose
\begin{gather}
  \A=
  \begin{bmatrix}
    1 & 0\\
    0 & 2\\
    0.5 & 1
  \end{bmatrix}\\
  \vec b=
  \begin{bmatrix}
    2 \\
    2 \\
    8 \\
  \end{bmatrix}
\end{gather}
Then:
\begin{gather}
  \AT\A=
  \begin{bmatrix}
    1.25 &  0.5\\
    0.5 &  5
  \end{bmatrix}\\
  \vec \AT b=
  \begin{bmatrix}
    6\\
    12
  \end{bmatrix}
\end{gather}
Solving $\AT\A\x=\AT\vec b$ gives:
\begin{equation}
  \begin{bmatrix}
    4\\
    2
  \end{bmatrix}
\end{equation}
To check if this is correct, we can calculate the residual, $\vec r$:
\begin{align}
  \vec r &= \vec b-\A\x\\
  &=  
  \begin{bmatrix}
    2 \\
    2 \\
    8 \\
  \end{bmatrix}-
  \begin{bmatrix}
    1 & 0\\
    0 & 2\\
    0.5 & 1
  \end{bmatrix}
  \begin{bmatrix}
    4\\
    2
  \end{bmatrix}\\
  &=
  \begin{bmatrix}
    2 \\
    2 \\
    8
  \end{bmatrix}
  -
  \begin{bmatrix}
    4\\
    4\\
    4
  \end{bmatrix}\\
  &=
  \begin{bmatrix}
    -2\\
    -2\\
    4
  \end{bmatrix}
\end{align}
Is this right? Well we can check by computing $\AT\vec r$:
\begin{align}
  \AT\vec r&=
  \begin{bmatrix}
    1 & 0 & 0.5\\
    0 & 2 & 1
  \end{bmatrix}
  \begin{bmatrix}
    -2\\
    -2\\
    4    
  \end{bmatrix}\\
  &=
  \begin{bmatrix}
    0\\
    0
  \end{bmatrix}
\end{align}

We can complete all of these calculations easily in Python:

\begin{pythonsnippet}
  from numpy import *
  A=array([[1.,0.],[0.,2.],[0.5,1.]])
  b=array([2.,2.,8.])
  
  x=numpy.linalg.solve(dot(A.T,A),dot(A.T,b))
  
  r=b-dot(A,x)
  
  # Least squares solution
  print x 
  # Residual          
  print r 
  # Should be zero!
  print dot(A.T,r)
\end{pythonsnippet}

\begin{exercise}
  Given:
  \begin{equation}
    \mat A=
    \begin{bmatrix}
      2\\
      5
    \end{bmatrix}
    \qquad
    \vec b=
    \begin{bmatrix}
      -2\\
      9.5
    \end{bmatrix}
  \end{equation}
  Solve $\A\x=\vec b$ by hand using equation \eqref{eq:ls_2d}. Draw by hand a plot
  similar to figure \ref{fig:least_square_2}\ showing $\A_{:,0}$, $\A\x$,
  $\vec b$ and the residual $\vec r$.
\end{exercise}

\begin{exercise}
  Write a Python script ~test_least_squares.py~. The script
  should use ~random_matrices.random_nonsquare~ from ~/numerical-methods-1~,
  generate random $3\times2$ and $3\times 4$ matrices. Use
  ~random_matrices.random_vec~ to generate random right hand sides. Solve
  the resulting least squares systems and verify that your answer is correct
  by checking that the residual is orthogonal to the columns of $\A$.
\end{exercise}

\section{Curve fitting}

\subsection{Fitting a line through two points}

Let's now consider one of the most common tasks in science. We have some
experimental or observational data which we expect to fit some theoretical
curve, but we don't know the right parameters. For example, suppose we have
two input values (experimental machine settings, for example):
\begin{equation}
  \vec x=
  \begin{bmatrix}
    1\\
    2
  \end{bmatrix}
\end{equation}
As a result of our experiment, we find two outcomes:
\begin{equation}
    \vec y =
  \begin{bmatrix}
    2\\
    6
  \end{bmatrix} 
\end{equation}
We want to fit a straight line of the form $y=c_0x+c_1$ to these points. Using
our two points, we can write two such equations:
\begin{gather}
  c_0x_0+c_1=y_0\\
  c_0x_1+c_1=y_1
\end{gather}
Of course we can also write this system of equations as a single matrix
equation:
\begin{equation}
  \begin{bmatrix}
    x_0 & 1 \\
    x_1 & 1
  \end{bmatrix}
  \vec c=
  \begin{bmatrix}
    y_0\\
    y_1
  \end{bmatrix}
\end{equation}
By substituting our two points in, we achieve:
\begin{equation}
  \begin{bmatrix}
    1 & 1 \\
    2 & 1
  \end{bmatrix}\vec c=
  \begin{bmatrix}
    2\\
    6
  \end{bmatrix}
\end{equation}
By solving this, we find:
\begin{equation}
  \vec c=
  \begin{bmatrix}
    4\\
    -2
  \end{bmatrix}
\end{equation}

We can write a very easy Python code for this. The output of this program is figure \ref{fig:linear_fit}.

\begin{figure}
  \centering
  \includegraphics{least_squares/linear_fit}
  
  \caption{The line passing through the points $(1,2)$ and $(2,6)$}
  \label{fig:linear_fit}
\end{figure}

\newpage

\begin{pythonprogram}
  from numpy import *
  from pylab import *

  def fit_linear(x,y):
      '''Construct and solve a linear system for the line 
      passing through two points in the plane. x is the 
      two x values, y is the two y values.
  
      The result is a vector c where c[0]x+c[1]=y is the line. 
      '''
 
      if(x.shape!=(2,)): 
          raise ValueError("x must be a 2-vector")
      if(y.shape!=(2,)): 
          raise ValueError("y must be a 2-vector")

      A=zeros((2,2))

      A[:,0]=x
      A[:,1]=1.

      c=linalg.solve(A,y)

      return c

  # x values to fit to.
  x=array([1,2])
  # y values to fit to.
  y=array([2,6])

  # solve for gradient and y intercept.
  c=fit_linear(x,y)

  # plot the original data.
  plot(x,y,'o')

  # plot the line we have fit.
  X=linspace(-1,3)
  Y=c[0]*X+c[1]

  # plot x and y axes:
  plot(X,0*X,'k')
  plot(0*Y,Y,'k')

  plot(X,Y)
  axis([min(X),max(X),min(Y),max(Y)])

  show()
\end{pythonprogram}

\subsection{Fitting higer degree polynomials}

Suppose now we have \emph{three}\ points through which we wish to fit a
quadratic curve (a parabola). The process is essentially identical except
now the general quadratic curve is written as $c_0x^2+c_1x+c_2=0$. Suppose
I have a vector of $x$ values and a vector of corresponding $y$ values:
\begin{equation}
  \x=
  \begin{bmatrix}
    1\\
    3\\
    4
  \end{bmatrix}
  \qquad \vec y=
  \begin{bmatrix}
    6\\
    18\\
    27
  \end{bmatrix}
\end{equation}
This time, our matrix system is:
\begin{align}
  \begin{bmatrix}
    \x^2&\rule[-4ex]{1pt}{9ex}&
    \x&\rule[-4ex]{1pt}{9ex}&
    1.\\
  \end{bmatrix}
  \vec c=
  \vec y
\end{align}
Substituting $\x$ and $\vec y$, we have:
\begin{align}
  \begin{bmatrix}
    1. & 1. & 1. \\
    9. & 3. & 1. \\
    16. & 4. & 1.
  \end{bmatrix}
  \vec c=
  \begin{bmatrix}
    6\\
    18\\
    27
  \end{bmatrix}
\end{align}
For which the solution is:
\begin{equation}
  \vec c=
  \begin{bmatrix}
    1.\\
    2.\\
    3.
  \end{bmatrix}
\end{equation}
The quadratic which fits this data is therefore $y=x^2+2x+3$.

It is easy to see that we could extend this to fit a degree $n$ polynomial
through $n+1$ points. A Python function which does this is:

\label{fit_polynomial}
\begin{pythonsnippet}
  def fit_polynomial(x,y, n):
    '''Construct and solve a linear system for the degree n 
    polynomial passing through n+1 points in the plane.
    x is the n+1 x values, y is the n+1 y values.

    The result is a vector c where 
             c[0]x^n+c[1]x^(n-1)+...+c[n]=y 
    is the line. 
    '''

    if(x.shape!=(n+1,)): 
        raise ValueError("x must be a n+1-vector")
    if(y.shape!=(n+1,)): 
        raise ValueError("y must be a n+1-vector")

    A=zeros((n+1,n+1))

    for i in range(n+1):
        A[:,i]=x**(n-i)

    return linalg.solve(A,y)
\end{pythonsnippet}
\enlargethispage{\baselineskip}

\subsection{Overdetermined curve fitting}

The scenario in which we have exactly $n+1$ points to which we wish to fit
to a degree $n$ polynomial is not typical in science. Instead, we usually
have a large data set for which we have good theoretical reasons to believe
that the data should lie on some low degree polynomial curve such as a
straight line or a parabola.

Of course experimental data is usually noisy so the points won't line up
exactly on the line. What we want is the line which best fits the set of
points, in some sense. Figure \ref{fig:linear_lst}\ shows this situation.

\begin{figure}
  \centering
  \includegraphics{least_squares/linear_lst}
  
  \caption{The linear fit to a set of data.}
  \label{fig:linear_lst}
\end{figure}

So, how do we fit this line? Well we can first construct our linear system
as before. Suppose we have a set of independent variable values $\x$ and a
set of data $\vec y$. Then if they all lie on the same line, the equation is:
\begin{align}
  \begin{bmatrix}
    \x&\rule[-4ex]{1pt}{9ex}&
    1.\\
  \end{bmatrix}
  \vec c=
  \vec y
\end{align}
Of course this equation has many more equations than unknowns so unless we
are absolutely, stupidly fortunate, there will be no solution. However we
have just learned that a system like this can be solved in the least squares
sense by multiplying on the left by the transpose of the matrix. The least
squares system is:
\begin{gather}
  \AT\A\vec c = \AT\vec y\\
  \A= 
  \begin{bmatrix}
    \x&\rule[-4ex]{1pt}{9ex}&
    1.\\
  \end{bmatrix}
\end{gather}
This system is $2\times 2$ and gives the best line through the points in
some least squares sense. 

Exactly the same argument for fitting a polynomial of higher degree to a
dataset applies, only this time the matrix $\A$ will have columns
corresponding to the higher powers of $x$.

\begin{figure}
  \centering
  \includegraphics{least_squares/quad_lst}
  
  \caption{A data set with fitted parabola. The red vertical lines show the
    magnitude of the residual component associated with each data point.}
  \label{fig:quad_lst}
\end{figure}

\subsection{Interpretation of the least squares solution}

So what does the least squares solution mean for curve fitting? Remember
that it is the solution which minimises the residual $\vec r$ given by:
\begin{equation}
  \vec r = \vec y - \A\x
\end{equation}
In the curve fitting case, the points $\A\x$ lie on the curve so the
residual is the distance from the curve to the data points $\vec y$ in the
vertical direction. Figure \ref{fig:quad_lst}\ shows this distance for each
point in a data set with its least squares quadratic fit.

It would be convenient to have a single number which indicates how well the
curve fits the data. The obvious candidate is the magnitude of the residual:
\begin{align}
  |\vec r|&=\sqrt{\vec r \cdot \vec r}\\
  &=\sqrt{\sum_i r_i^2}\label{eq:rms}
\end{align}
The issue with this quantity is that it always increases as more data is
added, which is not a welcome feature of an error indicator.  To take into
account the size of the data set, we can use the \emph{root mean square
  error}. This is given by:
\begin{equation}
  \mathrm{RMS}(\vec r)=\sqrt{\frac{\sum_i r_i^2}{n}}
\end{equation}
where $n$ is the size of the data set and therefore the length of $\vec r$.

If you think back to secondary school statistics, this formula looks very
familiar. In fact it's almost the same as the standard deviation
formula. If we consider the curve as some sort of average for the data, then
the RMS error is the standard deviation of the difference between the data
value and the curve value for a given $x$.

\begin{exercise}
  Produce a modified version of ~fit_polynomial~ from page
  \pageref{fit_polynomial} which performs a least squares fit for any number
  of data points greater than the polynomial degree. The original
  ~fit_polynomial~ is available from\\
  ~/numerical-methods-1/fit_polynomial.py~.  Use
  ~fit_polynomial.random_data~ to test your routine and produce plots
  similar to figure \ref{fig:linear_lst}\ for at least linear and quadratic
  input data. \emph{Hint:} the checks for incorrect input to ~fit_polynomial~ will need to
  change. What checks apply to the least squares version?
\end{exercise}

\section{Official versions}
%\enlargethispage{2\baselineskip}

As with matrix solution, ~numpy~ comes with least squares solvers and a
special purpose routine for curve fitting which are likely to be faster and
more robust than anything you write for yourself. The ~numpy~ function for
least squares systems is ~numpy.linalg.lstsq~ while the one for polynomial
curve fitting is ~numpy.polyfit~. Another useful function in this regard is
~numpy.poly1d~ which takes a sequence of coefficients and returns the
corresponding polynomial function. This function can then be called on $x$
values to evaluate the polynomial.

