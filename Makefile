XFIG_IMAGES=figures/intermediate \
	figures/triangle \
	figures/linear_1 \
	figures/least_square_1 \
	figures/least_square_2 \
	figures/residual \
	figures/matmul_least \
	figures/matmul_transpose

TEX=notes/introduction.tex\
	notes/arithmetic.tex\
	notes/floating_point.tex\
	notes/approximation.tex\
	notes/taylor_diff.tex\
	notes/differentiation.tex\
	notes/root_finding.tex\
	notes/linear_algebra.tex\
	notes/least_squares.tex\
	notes/taylor_series.tex


TARGETS=notes.pdf

notes.pdf: $(addsuffix _pdf.tex, $(XFIG_IMAGES)) \
	$(addsuffix _tex.pdf, $(XFIG_IMAGES))\
	$(TEX)\
	functions/approx_convergence.pdf\
	functions/trapezoidal_error.pdf\
	functions/central_diff.pdf\
	root_finding/bolzano.pdf\
	least_squares/least_squares_2d.pdf\
	least_squares/linear_fit.pdf

functions/approx_convergence.pdf: functions/approx_func.py
	cd functions; python approx_func.py

functions/trapezoidal_error.pdf: functions/integration.py
	cd functions; python integration.py

functions/central_diff.pdf: functions/plot_derivative.py
	cd functions; python plot_derivative.py

root_finding/bolzano.pdf: root_finding/plot_root_algorithms.py
	cd root_finding; python plot_root_algorithms.py

least_squares/least_squares_2d.pdf:least_squares/least_squares_2d.py
	cd least_squares; python least_squares_2d.py

least_squares/linear_fit.pdf: least_squares/plot_curve_fitting.py
	cd least_squares; python plot_curve_fitting.py

least_squares/random_matrices.py: lu/random_matrices.py
	cd least_squares; ln -s ../lu/random_matrices.py .

.PHONY:materials

materials: notes.pdf
	install -d numerical-methods-1
	install functions/approximation.py functions/newton_cotes.py \
functions/test*.py numerical-methods-1
	install root_finding/test*.py numerical-methods-1
	install lu/random_matrices.py numerical-methods-1
	install least_squares/fit_polynomial.py numerical-methods-1
	install notes.pdf numerical-methods-1


%.pdf: %.tex 
	pdflatex $<
	if fgrep "Rerun to" $*.log; then $(MAKE) --assume-new $^ $@;fi
ifndef NOBIBTEX
	if fgrep "There were undefined"  $*.log;\
	then bibtex $*; \
	$(MAKE) NOBIBTEX=true --assume-new $^ $@;fi
endif

%_tex.pdf: %.fig
	fig2dev -L pdftex $^ $@

%_pdf.tex: %.fig
	fig2dev -L pdftex_t -p $*_tex.pdf $^ $@

%.tex: %.pstex_t %.pstex
	./wrap_pstex $<

%.dvi: %.tex 
	pdflatex -output-directory $(dir $@) -shell-escape -output-format dvi $<

%.png: %.dvi
	dvipng -T tight  -D 100 -bg Transparent $<  -o $@

clean: 
	rm $(TARGETS) $(PSFILES) *.dvi *.aux *.log *.bbl *.blg *.toc *.out \
*.pdftex *_tex.pdf *_pdf.tex *.pstex_t *.pstex *.pdf \
figures/*_tex.pdf figures/*_pdf.tex functions/*.pdf \
root_finding/*.pdf least_squares/*.pdf least_squares/random_matrices.py \
notes/*.aux 2>/dev/null||true
	rm -r numerical-methods-1 2>/dev/null||true