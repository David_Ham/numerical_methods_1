from numpy import *
from la_vis import *
from random_matrices import *


def solve_lower(L,b):

    L=asarray(L)
    b=asarray(b)

    shape=L.shape[0]
    
    x=zeros(shape)

    for i in range(shape):
        x[i]=(b[i]-dot(L[i,:i],x[:i]))/L[i,i]
    
    return x

def solve_visualise_lower(L,b):
    L=logarray(asarray(L))
    b=logarray(asarray(b))

    shape=L.shape[0]
    
    x=logarray(zeros(shape)*float('NaN'))

    yield x,L,b,"\mathrm{L}\mathbf{x}=\mathbf{b}"
    clear_log((x,L,b))

    for i in range(shape):
        code="{\color{orange}x[i]}=({\color{purple}b[i]}-{\color{blue}dot(L[i,:i],x[:i])})/{\color{green}L[i,i]}"
        exec strip_color(code)
        yield x,L,b,code.replace("i",`i`)
        clear_log((x,L,b))

slide=r'''
\begin{slide}{Solving lower triangular systems}
  \tiny

  \begin{gather*}
  <eqn>
  \end{gather*}
  
  \begin{equation*}
    <A>\qquad\times\qquad<x>\qquad=\qquad<b>
  \end{equation*}

\end{slide}
'''


def make_slides(L,b):

    ans=header

    for x,A,b,equation in solve_visualise_lower(L,b):
        ans+=slide.replace("<A>",latex_matrix(asarray(A),A.log)
                           ).replace("<x>",latex_vector(asarray(x),x.log)
                                     ).replace("<b>",latex_vector(asarray(b),b.log)
                                               ).replace("<eqn>",equation)
        
    ans+=footer
    return(ans)

def example(size, filename):

    L=random_lower(size)
    b=random_vec(size)

    file(filename+".tex","w").write(make_slides(L,b))
