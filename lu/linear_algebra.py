from numpy import *

def factorise_lu(A):

    if(A.shape[0]!=A.shape[1]):
        raise ValueError("Matrix A is not square")

    L=zeros(A.shape)
    U=zeros(A.shape)

    n=A.shape[0]

    for i in range(n):
        L[i,i]=1.
        for j in range(i):
            L[i,j]=(A[i,j]-dot(L[i,:j],U[:j,j]))/U[j,j]

        for j in range(i,n):
            U[i,j]=A[i,j]-dot(L[i,:i],U[:i,j])

    return L,U
