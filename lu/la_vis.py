from numpy import *

class logarray(ndarray):
    def __new__(cls, input_array, info=None):
        # Input array is an already formed ndarray instance
        # We first cast to be our class type
        obj = asarray(input_array).view(cls)

        obj.log=zeros(obj.shape)

        obj.logging=True

        # Finally, we must return the newly created object:
        return obj
    
    def clear_log(self):
        self.log=zeros(self.shape)

    def __getslice__(self,min,max):
        if hasattr(self,'log'):
            self.log[min:max]=3

        return asarray(self).__getslice__(min,max)

    def __getitem__(self, key):

        if not iterable(key):
            if hasattr(self,'log'):
                self.log[key]=4
        elif any(map(lambda x:isinstance(x,slice),key)):
            self.log[key]=3
        else:
            self.log[key]=2

        return asarray(self).__getitem__(key)

    def __setitem__(self, key, val):

        if not iterable(key):
            if hasattr(self,'log'):
                self.log[key]=1
        elif any(map(lambda x:isinstance(x,slice),key)):
            self.log[key]=1
        else:
            self.log[key]=1
        
        return asarray(self).__setitem__(key, val)

    def __setslice__(self, min,max,val):

        if hasattr(self,'log'):
            self.log.__setslice__(min,max,1)        

        return asarray(self).__setslice__(min,max,val)

def clear_log(arraylist):
    """clear_log(arraylist)

    Clear the logs for each ndarray in arraylist.
    """

    for a in arraylist:
        a.clear_log()

header=r'''\documentclass[imperial,pdf]{prosper}
\usepackage{color, listings, amsmath, amssymb,wrapfig, xspace,ulem, array}
\usepackage{pstricks, pst-node, psfrag}

\newcommand{\mat}[1]{\mathrm{#1}}

\begin{document}

'''

footer=r'''
\end{document}
'''

color=(
    'black',
    'orange',
    'green',
    'blue',
    'purple')


def latex_matrix(A,Am=None):

    ans=r'''\begin{bmatrix}
    '''
    if (Am==None):
        A_m=zeros(A.shape)
    else:
        A_m=Am

    for i in range(A.shape[0]):
        ans+=r' & '.join(map(lambda x:'{\color{%s}%g}'%(color[int(x[1])],x[0]), zip(A[i,:],A_m[i,:])))
        ans+=r'\\'+'\n'

    ans+=r'''
    \end{bmatrix}'''
    return ans

def latex_vector(A,Am):

    ans=r'''\begin{bmatrix}
    '''

    for i in range(A.shape[0]):
        ans+='{\color{%s}%g}'%(color[int(Am[i])],A[i])
        ans+=r'\\'+'\n'

    ans+=r'''\end{bmatrix}'''
    return ans

def strip_color(code):
    import re
    return re.sub(r"\{\\color\{\w+\}(.+?)\}",lambda x:x.groups()[0],code)


    
    

