from la_vis import *
from random_matrices import *

def row_mult(A,b):

    A=logarray(A)
    b=logarray(b)

    size=A.shape[0]

    x=logarray(zeros(size))

    yield x,A,b,"\mathbf{x}=\mathrm{A}\mathbf{b}"
    clear_log((x,A,b))

    for i in range(size):

        code="{\color{orange}x[i]}={\color{blue}dot(A[i,:],b[:])}"
        exec strip_color(code)
        yield x,A,b,code
        clear_log((x,A,b))

def row_matmul(A,B):

    A=logarray(A)
    B=logarray(B)

    size=A.shape

    X=logarray(zeros(size))

    yield X,A,B,"\mathbf{X}=\mathrm{A}\mathbf{B}"
    clear_log((X,A,B))

    for i in range(size[0]):
        for j in range(size[1]):
            code="{\color{orange}X[i,j]}={\color{blue}dot(A[i,:],B[:,j])}"
            exec strip_color(code)
            yield X,A,B,code
            clear_log((X,A,B))


def col_mult(A,b):

    A=logarray(A)
    b=logarray(b)

    size=A.shape[0]

    x=logarray(zeros(size))

    yield x,A,b,"\mathbf{x}=\mathrm{A}\mathbf{b}"
    clear_log((x,A,b))

    for i in range(size):

        code="{\color{orange}x[:]}+={\color{purple}b[i]}*{\color{blue}A[:,i]}"
        exec strip_color(code)
        yield x,A,b,code
        clear_log((x,A,b))



row_slide=r'''
\begin{slide}{Matrix multiplication, row-wise}
  \tiny

  \begin{gather*}
  <eqn>
  \end{gather*}
  
  \begin{equation*}
    <x>\qquad=\qquad<A>\qquad\times\qquad<b>
  \end{equation*}

\end{slide}
'''

def make_row_slides(A,b):

    ans=header
    slide=row_slide

    for x,A,b,equation in row_mult(A,b):
        ans+=slide.replace("<A>",latex_matrix(asarray(A),A.log)
                           ).replace("<x>",latex_vector(asarray(x),x.log)
                                     ).replace("<b>",latex_vector(asarray(b),b.log)
                                               ).replace("<eqn>",equation)
        
    ans+=footer
    return(ans)

def make_matmul_slides(A,b):

    ans=header
    slide=row_slide

    for x,A,b,equation in row_matmul(A,b):
        ans+=slide.replace("<A>",latex_matrix(asarray(A),A.log)
                           ).replace("<x>",latex_matrix(asarray(x),x.log)
                                     ).replace("<b>",latex_matrix(asarray(b),b.log)
                                               ).replace("<eqn>",equation)
        
    ans+=footer
    return(ans)

def row_example(size, filename):

    A=random_mat(size)
    b=random_vec(size)

    file(filename+".tex","w").write(make_row_slides(A,b))


col_slide=r'''
\begin{slide}{Matrix multiplication, column-wise}
  \tiny

  \begin{gather*}
  <eqn>
  \end{gather*}
  
  \begin{equation*}
    <x>\qquad=\qquad<A>\qquad\times\qquad<b>
  \end{equation*}

\end{slide}
'''

def make_col_slides(A,b):

    ans=header
    slide=col_slide

    for x,A,b,equation in col_mult(A,b):
        ans+=slide.replace("<A>",latex_matrix(asarray(A),A.log)
                           ).replace("<x>",latex_vector(asarray(x),x.log)
                                     ).replace("<b>",latex_vector(asarray(b),b.log)
                                               ).replace("<eqn>",equation)
        
    ans+=footer
    return(ans)

def col_example(size, filename):

    A=random_mat(size)
    b=random_vec(size)

    file(filename+".tex","w").write(make_col_slides(A,b))

