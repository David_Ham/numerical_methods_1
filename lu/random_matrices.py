import numpy

def random_lower(size):
    """random_lower(size)
    Return a random size*size lower triangular matrix
    with entries between 1 and 10."""
    
    M= numpy.random.randint(1,10,(size,size)).astype(numpy.double)

    for i in range(size):
        M[i,i+1:]=0.

    return M

def random_upper(size):
    """random_upper(size)
    Return a random size*size upper triangular
    matrix with entries between 1 and 10."""
    
    M= numpy.random.randint(1,10,(size,size)).astype(numpy.double)

    for i in range(size):
        M[i,:i]=0.

    return M

def random_mat(size):
    """random_upper(size)
    Return a random size*size matrix with entries between 1 and 10."""
    
    M= numpy.random.randint(1,10,(size,size)).astype(numpy.double)
    
    return M

def random_nonsquare(size):
    """random_upper(size)
    Return a random size[0]*size[1] matrix with entries between 1 and 10."""

    assert(numpy.shape(size)==(2,))
    
    M= numpy.random.randint(1,10,(size[0],size[1])).astype(numpy.double)
    
    return M

def random_lu(size):
    """random_lu(size)
    Return a random size*size matrix with entries between 1 and 10 and which
    is guaranteed to have a nice LU factorisation."""
    L=random_lower(size)
    for i in range(size):
        L[i,i]=1.0
    
    U=random_upper(size)

    M=numpy.dot(L,U)
    
    return M


def random_vec(size):
    """random_vec(size)
    Return a random size vector with entries between 1 and 10."""

    return numpy.random.randint(1,10,size).astype(numpy.double)
