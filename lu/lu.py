from la_vis import *

class logarray(ndarray):
    def __new__(cls, input_array, info=None):
        # Input array is an already formed ndarray instance
        # We first cast to be our class type
        obj = asarray(input_array).view(cls)

        obj.log=zeros(obj.shape)

        # Finally, we must return the newly created object:
        return obj
    
    def clear_log(self):
        self.log=zeros(self.shape)

    def __getitem__(self, key):

        if not iterable(key):
            # Single indices are typically internal calls not made by us.
            pass
        elif any(map(lambda x:isinstance(x,slice),key)):
            self.log[key]=3
        else:
            self.log[key]=2
        
        return asarray(self).__getitem__(key)

    def __setitem__(self, key, val):

        if not iterable(key):
            # Single indices are typically internal calls not made by us.
            pass
        elif any(map(lambda x:isinstance(x,slice),key)):
            self.log[key]=1
        else:
            self.log[key]=1
        
        return asarray(self).__setitem__(key, val)


def clear_log(arraylist):
    """clear_log(arraylist)

    Clear the logs for each ndarray in arraylist.
    """

    for a in arraylist:
        a.clear_log()

def factorise(A, pivot=True):

    A=asarray(A)
    n=A.shape[0]

    L=zeros(A.shape)
    U=zeros(A.shape)
    P=zeros(A.shape)

    # J is the set of numbers from which indices will be taken.
    J=range(n)

    for i in range(n):
        if pivot:
            ii=J[A[J,i].argmax()]
        else:
            ii=i

        L[i,i]=1.
                
        for j in range(i):
            
            L[i,j]=(A[ii,j]-dot(L[i,:i],U[:i,j]))/U[j,j]

        U[i,i]=A[ii,i]-dot(L[i,:i],U[:i,i])
        
        for j in range(i+1, n):
            
            U[i,j]=A[ii,j]-dot(L[i,:i],U[:i,j])
            
        J.remove(ii)
        P[i,ii]=1.

    return(P,L,U)

def factorise_visualise(A, pivot=True):

    A=logarray(asarray(A))
    n=A.shape[0]

    P=logarray(zeros(A.shape))
    L=logarray(zeros(A.shape))
    U=logarray(zeros(A.shape))

    # J is the set of numbers from which indices will be taken.
    J=range(n)

    for i in range(n):
        if pivot:
            p_i=J[A[J,i].argmax()]
            A.clear_log()
        else:
            p_i=i
            

        L[i,i]=1

        yield(A,P,L,U,"L[i,i]=1");clear_log((A,P,L,U))
                
        for j in range(i):
            
            L[i,j]=(A[p_i,j]-dot(L[i,:j],U[:j,j]))/U[j,j]

            yield(A,P,L,U,"L[i,j]=(A[p_i,j]-dot(L[i,:j],U[:j,j]))/U[j,j]")
            clear_log((A,P,L,U))

        U[i,i]=A[p_i,i]-dot(L[i,:i],U[:i,i])

        yield(A,P,L,U,"U[i,i]=A[p_i,i]-dot(L[i,:i],U[:i,i])")
        clear_log((A,P,L,U))
        
        for j in range(i+1, n):
            
            U[i,j]=A[p_i,j]-dot(L[i,:i],U[:i,j])

            yield(A,P,L,U,"U[i,j]=A[p_i,j]-dot(L[i,:i],U[:i,j])")
            clear_log((A,P,L,U))
            
        J.remove(p_i)
        P[i,p_i]=1
        
        if pivot:
            yield(A,P,L,U,"P[i,p_i]=1");clear_log((A,P,L,U))


def factorise_visualise_no_p(A):

    A=asmatrix(A)
    
    n=A.shape[0]

    L=zeros(A.shape)
    U=zeros(A.shape)

    for i in range(n):
        L[i,i]=1.
        
        Lm=zeros(A.shape)
        Um=zeros(A.shape)
        Am=zeros(A.shape)
        Lm[i,i]=1
        
        yield(L,Lm,U,Um,Am,0)
            
        U[i,i]=A[i,i]-dot(L[i,:i],U[:i,i])

        Lm=zeros(A.shape)
        Um=zeros(A.shape)
        Am=zeros(A.shape)
        Um[i,i]=1
        Am[i,i]=2
        Lm[i,:i]=3
        Um[:i,i]=3

        yield(L,Lm,U,Um,Am,1)

        for j in range(i+1, n):
            L[j,i]=(A[j,i]-dot(L[j,:i],U[:i,i]))/U[i,i]

            Lm=zeros(A.shape)
            Um=zeros(A.shape)
            Am=zeros(A.shape)
            Lm[j,i]=1
            Am[j,i]=2
            Lm[j,:i]=3
            Um[:i,i]=3
            Um[i,i]=4
            
            yield(L,Lm,U,Um,Am,2)


            U[i,j]=A[i,j]-dot(L[i,:i],U[:i,j])

            Lm=zeros(A.shape)
            Um=zeros(A.shape)
            Am=zeros(A.shape)
            Um[i,j]=1
            Am[i,j]=2
            Lm[i,:i]=3
            Um[:i,j]=3
            
            yield(L,Lm,U,Um,Am,3)


slide=r'''
\begin{slide}{LU Factorisation}
  \tiny

  \begin{gather*}
  <eqn>
  \end{gather*}
  
  \begin{equation*}
    <L>\qquad\times\qquad<U>\qquad=\qquad<A>
  \end{equation*}

\end{slide}
'''

equations=(
    '{\color{red}L[i,i]}=1.',
    '{\color{red}U[i,i]}={\color{green}A[i,i]}-{\color{blue}dot(L[i,:i],U[:i,i])}',
    '{\color{red}L[j,i]}=({\color{green}A[j,i]}-{\color{blue}dot(L[j,:i],U[:i,i]))}/{\color{purple}U[i,i]}',
    '{\color{red}U[i,j]}={\color{green}A[i,j]}-{\color{blue}dot(L[i,:i],U[:i,j])}',
    )


def make_slides(A, pivot=True):

    ans=header

    for A,P,L,U,equation in factorise_visualise(A,pivot):
        ans+=slide.replace("<L>",latex_matrix(asarray(L),L.log)
                           ).replace("<U>",latex_matrix(asarray(U),U.log)
                                     ).replace("<A>",latex_matrix(asarray(A),A.log)
                                               ).replace("<eqn>",equation)
        
    ans+=footer
    return(ans)
